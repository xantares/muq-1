
#ifndef _PowerKernel_h
#define _PowerKernel_h

#include "MUQ/Geostatistics/IsotropicCovKernel.h"

namespace muq {
namespace Geostatistics {
/**
 *  @class PowerKernel
 *
 *  @brief This is a basic class for building covariance matrices from a power law covariance kernel.
 *
 *  This class implements covariance kernels of the form:
 *  /f[
 *  cov(x,y) = \sigma\exp\left[-\frac{1}{P}\left(\frac{(\|x-y\|}{L}\right)^P\right]
 *  /f]
 *
 *  When the Power is set to 2, this gives a standard Gaussian covariance kernel.  A Power of 1, gives an Exponential
 *     kernel.
 *
 *  The length scale and variance are stored with their log values so they are unconstrained parameters. Only these two
 *     are
 *  treated as tunable parameters, as the exponent is typically a structural choice.
 *
 *  @author Matthew Parno, Patrick Conrad
 *
 */
class PowerKernel : public IsotropicCovKernel {
public:

  REGISTER_KERNEL_CONSTRUCTOR(PowerKernel)
  /** Construct the kernel from the characteristic length scale, power, and variance.
   *  @param[in] LengthIn characteristic length scale
   *  @param[in] PowerIn power in exponential decay
   *  @param[in] VarIn variance - i.e. diagonal of covariance matrix
   */
  PowerKernel(boost::property_tree::ptree& ptree);

  PowerKernel(double LengthIn = 1, double PowerIn = 2, double VarIn = 1);

  virtual ~PowerKernel() = default;

  /** This function evaluates the power kernel.
   *  @param[in] d Distance between two points
   *  @return covariance of random field between two points at a distance d
   */
  virtual double          DistKernel(double d) const override;

  /** Set the covariance kernel parameters (Length, power, variance) from a vector of parameters.
   *  @param[in] theta MuqVector or parameter values for the covariance kernel.
   */
  virtual void            SetParms(const Eigen::VectorXd& theta) override;

  virtual void            GetGrad(double d, Eigen::VectorXd& grad) const override;

  virtual Eigen::VectorXd GetParms() override
  {
    Eigen::VectorXd Out(2); Out(0) = logLength; Out(1) = logVar; return Out;
  }

  virtual double KernelRadialDerivative(double const r) override;

protected:

  double logLength;
  double Power;
  double logVar;
};

// class CovKernel
} // namespace Geostatistics
} // namespace muq

#endif // ifndef _PowerKernel_h
