
#ifndef IsotropicCovKernel_h_
#define IsotropicCovKernel_h_

#include "CovKernel.h"

namespace muq {
namespace utilities {
template<unsigned int dim>
class Mesh;
}
}
namespace muq {
namespace Geostatistics {
class IsotropicCovKernel;
typedef std::shared_ptr<IsotropicCovKernel> IsoCovKernelPtr;


/**
 *  @class IsotropicCovKernel
 *
 *  @brief This is a basic class for building covariance matrices from isotropic covariance kernels.
 *
 *  N dimensional Gaussian random fields can be describes just like any other Gaussian distribution, with a mean and
 *     covariance.  However, for random fields, the covariance matrix usually has additional structure.  This class
 *     serves as a base for describing covariance matrices through a covariance kernel.  This isotropic class provides
 * an
 *     interface for other covariance kernels that is general for N dimensions, because only the Euclidean distance
 *     between two points is needed to evaluate the kernel.
 *
 *
 *  All child classes must provide a DistKernel function, that takes the distance as input and returns the covariance
 *     between the field at those points.
 *
 *  @author Matthew Parno
 *
 *  \todo Add a handle class around the CovKernel class to enable tensor products of different kernels, addition of
 *     kernels, and scaling of kernels.
 *
 */
class IsotropicCovKernel : public CovKernel {
public:

  IsotropicCovKernel(unsigned int NparamsIn) : Nparams(NparamsIn) {}

  virtual ~IsotropicCovKernel() = default;

  virtual void   BuildGradMat(const Eigen::MatrixXd& xs, std::vector<Eigen::MatrixXd>& GradMat) const override;

  /** Evaluate the base kernel by computing the distance between x and y and then calling DistKernel
   *  @param[in] x Position 1
   *  @param[in] y Position 2
   *  @return covariance kernel evaluated at ||x-y||
   */
  virtual double Kernel(const Eigen::Ref<const Eigen::VectorXd>& x, const Eigen::Ref<const Eigen::VectorXd>& y) const;


  /** This is the interface all children of IsotropicCovKernel must follow.
   *  @param[in] d The distance between two points
   *  @return covariance between points separated by a distance d
   */
  virtual double       DistKernel(double d) const = 0;

  /** Get the number of parameters in this kernel.*/
  virtual unsigned int NumParams() const
  {
    return Nparams;
  }

  /** Set the hyperparameters from a vector. */
  virtual void            SetParms(const Eigen::VectorXd& theta) override = 0;

  /** Get the current set of parameters in vector form */
  virtual Eigen::VectorXd GetParms() override = 0;

  /** Regardless of what type of kernel this is, we can try to estimate a "length scale" or something related to the
   *  length.  This function uses a slightly modified bisection sort to find the isotropic distance where the covariance
   *  kernel value reaches 1e-8 of the variance.
   *  @return An approximate "lengthscale"
   */
  virtual double          EstimateLength() const;

  virtual Eigen::VectorXd KernelSpatialDerivative(Eigen::VectorXd const& x, Eigen::VectorXd const& xWrt) override;

  virtual double          KernelRadialDerivative(double const r)
  {
    std::cerr << " no derivative implemented" << std::endl; assert(false); return 0;
  }

private:

  /** Get the sensitivity of the covariance at distance d
   *  @param[in] d Distance
   *  @return The gradient of the kernel wrt the hyperparameters
   *
   *  As above, returns the grad by ref (not value) because it caused performance issues here
   */
  virtual void GetGrad(double d, Eigen::VectorXd& grad) const = 0;

protected:

  unsigned int Nparams;
  unsigned int mutable dimIn; //allow it to be set during evaluations that are otherwise const
};

// class CovKernel
} //namespace Geostatistics
} //namespace muq

#endif // ifndef IsotropicCovKernel_h_
