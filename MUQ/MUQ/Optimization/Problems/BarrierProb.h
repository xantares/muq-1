
#ifndef _BarrierProb_h
#define _BarrierProb_h

#include "MUQ/Optimization/Problems/OptProbBase.h"

namespace muq {
namespace Optimization {
/** @class BarrierProb
 *  @ingroup Optimization
 *  @brief An unconstrained penalized subproblem using a logarigthmic barrier function.
 *  @details This class provides the definition of an unconstrained penalized objective that is used inside the
 * muq::Optimization::Barrier solver to solve inequality constrained optimization problems using an arbitrary solver.
 */
class BarrierProb : public OptProbBase {
public:

  /** Construct the penalized subproblem using the original objective and constraints defined in prob, as well as the
   * penalty.
   *  @param[in] prob The original constrained optimization problem.
   *  @param[in] barrierCoeffIn penalty parameter set in the Barrier method's outer iteration
   */
  BarrierProb(std::shared_ptr<OptProbBase> prob, double barrierCoeffIn);

  /** Evaluate the penalized objective.
   *  @param[in] xc The location to evaluate the objective.
   *  @return The value of the penalized objective at xc.
   */
  virtual double eval(const Eigen::VectorXd& xc);

  /** Evaluate the penalized objective gradient.
   *  @param[in] xc The location to compute the objective and gradient.
   *  @param[out] gradient A vector that will be filled with the gradient.
   *  @return xc The value of the penalized objective at xc.
   */
  virtual double grad(const Eigen::VectorXd& xc, Eigen::VectorXd& gradient);

private:

  double barrierCoeff;

  // the underlying objective function and constraints
  std::shared_ptr<OptProbBase> baseProb;
};

// class BarrierProb
} //namespace Optimization
} // namespace muq


#endif // ifndef _BarrierProb_h
