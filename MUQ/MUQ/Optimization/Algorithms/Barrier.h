
#ifndef _Barrier_h
#define _Barrier_h

#include "MUQ/Optimization/Algorithms/OptAlgBase.h"

namespace muq {
namespace Optimization {
/** @class Barrier
 *  @ingroup Optimization
 *  @brief Logarithmic barrier function approach for solving nonlinear inequality constrained optimization problems
 *  @details This class uses a logarithmic barrier method to transform a constrained optimization problem into an
 * unconstrained problem. The idea is to create a series of unconstrained subproblems that are solved by any
 * unconstrained optimizer.  These uncsontrained problems contain a barrier function that prevents the optimizer from
 * stepping outside the feasible domain and violating the inequality constraints.  The magnitude of this barrier is
 * controlled by a penalty parameter that is slowly reduced to allow the solution of the inner optimization problem to
 * converge to the true constrained optimum. Note that if the optimizer requires derivatives, then derivatives of both
 * the constraints and objective will be required.  Similarly, if the optimizer is a derivative free algorithm, then no
 * derivative information about the constraints or the objective will be used.  Unlike Penalty of Augmented Lagrangian
 * approaches, this Barrier approach ensures the objective is never evaluated at an unfeasible point.
 */
class Barrier : public OptAlgBase {
public:

  REGISTER_OPT_CONSTRUCTOR(Barrier)
  /** Construct a the Barrier solver from the settings in a ptree.  The ptree should use some base optimization
   * algorithm as Opt.Method and should have an Opt.ConstraintHandler set to Barrier.
   *  @param[in] ProbPtr A shared pointer to the optimization problem object
   *  @param[in] properties Parameter list containing optimization method and parameters
   */
  Barrier(std::shared_ptr<OptProbBase> ProbPtr, boost::property_tree::ptree& properties);


  /** Solve the optimization problem using x0 as a starting point for the iteration.
   *  @param[in] x0 The initial point for the optimizer.
   *  @return A vector holding the best point found by the optimizer.  Note that the optimizer may not have converged
   * and the GetStatus() function should be called to ensure that the algorithm converged and this vector is meaningful.
   */
  virtual Eigen::VectorXd solve(const Eigen::VectorXd& x0) override;

protected:

  /// the current penalty weight
  double barrierCoeff;

  /// how multiplicative factor increasing penaltyCoeff at each outer iteration
  double barrierScale;

  // the maximum number of
  int maxOuterIts;

  // property list for unconstrained solver
  boost::property_tree::ptree uncProperties;

private:

  REGISTER_OPT_DEC_TYPE(Barrier)
};

//class Barrier
} // namespace Optimization
} // namespace muq

#endif // ifndef _Barrier_h
