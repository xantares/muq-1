
#ifndef _Newton_h
#define _Newton_h

#include "MUQ/Optimization/Algorithms/GradLineSearch.h"

namespace muq {
namespace Optimization {
/** @class Newton
 *  @ingroup Optimization
 *  @brief Implementation of Newton-like methods
 *  @details This class implements the simple Hessian based Newton's method.  However, if this algorithm is given a
 *     problem that uses approximate Hessians (i.e. Gauss-Newton Hessian), a quasi-Newton approach will effectively be
 *     implemented due to the approximate Hessian.
 */
class Newton : public GradLineSearch {
public:

  REGISTER_OPT_CONSTRUCTOR(Newton)
  /** parameter list constructor
   *  @param[in] ProbPtr A shared_ptr to the optimization problem we want to solve.
   *  @param[in] properties A parameter list containing any parameters that were set by the user.  If the parameters
   * were
   *     not set, default values will be used.
   */
  Newton(std::shared_ptr<OptProbBase> ProbPtr, boost::property_tree::ptree& properties);

  /** step function, return the Newton direction
   *  @return A vector holding the Newton direction scaled by the step size.
   */
  virtual Eigen::VectorXd step();

private:

  REGISTER_OPT_DEC_TYPE(Newton)
};
} // namespace Optimization
} // namespace muq


#endif // ifndef _Newton_h
