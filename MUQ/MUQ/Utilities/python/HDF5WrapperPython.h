
#ifndef HDF5WRAPPERPYTHON_H
#define HDF5WRAPPERPYTHON_H

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/HDF5Wrapper.h"
#include "MUQ/Utilities/python/PythonTranslater.h"

namespace muq {
namespace Utilities {
class HDF5WrapperPython : public HDF5Wrapper, public boost::python::wrapper<HDF5Wrapper> {
public:

  HDF5WrapperPython();

  static void OpenFileDict(boost::python::dict const& dict);

  static void OpenFileStr(std::string const& filename);

  static void WriteMatrixPy(std::string const datasetName, boost::python::list const& pyMatrix);

  template<typename scalarType>
  static void WriteVectorAttributePy(std::string const& datasetName, boost::python::list const& vector)
  {
    WriteMatrix(datasetName, GetEigenVector<Eigen::Matrix<scalarType, Eigen::Dynamic, 1> >(vector));
  }

  template<typename scalarType>
  static boost::python::list GetVectorAttributePy(std::string const& datasetName,
                                                  std::string const& attributeName)
  {
    return GetPythonVector<Eigen::Matrix<scalarType, Eigen::Dynamic, 1> >(GetVectorAttribute<scalarType>(datasetName,
                                                                                                         attributeName));
  }

  static void                WriteVectorPy(std::string const          datasetName,
                                           boost::python::list const& vector);

  static boost::python::list ReadMatrixPy(std::string const datasetName);

  static boost::python::list ReadVectorPy(std::string const datasetName);

  static void                WritePartialMatrixPy(std::string const          datasetName,
                                                  boost::python::list const& matrix,
                                                  int                        row,
                                                  int                        col);
};

void ExportHDF5Wrapper();
}
}

#endif // ifndef HDF5WRAPPERPYTHON_H
