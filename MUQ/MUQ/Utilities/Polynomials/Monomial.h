#ifndef MONOMIAL_H_
#define MONOMIAL_H_


#include <boost/serialization/access.hpp>

#include "MUQ/Utilities/Polynomials/RecursivePolynomialFamily1D.h"

namespace muq {
namespace Utilities {
/**
 * @class Monomial
 * @ingroup Polynomials1D
 * @brief Implements monomials.
 * @details Subclasses the numerically stable RecursivePolynomialFamily1D with the three-term
 * recurrence necessary to generate the Monomials.  This class also implements a 
 * static root finding algorithm based on Sturm sequences and a bisection solver.
 **/
class Monomial : public Static1DPolynomial<Monomial> {
public:

  friend class Static1DPolynomial<Monomial>;
  
  Monomial() = default;
  virtual ~Monomial() = default;
  
  /// Throws an assert because monomials are not orthogonal
  static double GetNormalization_static(unsigned int n){assert(false);};
  
  static Eigen::VectorXd GetMonomialCoeffs(const int order);
  
  static double gradient_static(unsigned int order, double x);
  static double secondDerivative_static(unsigned int order, double x);
  
private:
  
  ///Grant access to serialization
  friend class boost::serialization::access;

  ///Serialization method
  template<class Archive>
  void   serialize(Archive& ar, const unsigned int version);

  //implement methods defining recurrence
  static double alpha(double x, unsigned int k);
  static double beta(double x, unsigned int k);
  static double phi0(double x);
  static double phi1(double x);
  
};
}
}

BOOST_CLASS_EXPORT_KEY(muq::Utilities::Static1DPolynomial<muq::Utilities::Monomial>)
BOOST_CLASS_EXPORT_KEY(muq::Utilities::Monomial)

#endif /* HERMITEPOLYNOMIALS1DRECURSIVE_H_ */
