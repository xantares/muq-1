#ifndef REGISTERCLASSNAMEHELPER_H_
#define REGISTERCLASSNAMEHELPER_H_

#include <boost/functional/factory.hpp>

//this file is based on a snippet: http://pastebin.com/dSTLt7vW

/**
 * This file contains a basic prescription for performing string registration. It allows you to go from a string and get 
 * a function pointer something that you can call like make_shared<T>.
 * 
 * If you're trying to register a class, and were directed here, then add:
 * REGISTER_YOURCLASSNAME(NAME) 
 * where YOURCLASSNAME is the type of registration, and NAME is the unqualified name of the class to register.
 * 
 * If you're creating a new type of registered class, follow the directions below.
 */
 



namespace boost{
	template <typename T>
	struct shared_factory : public factory<std::shared_ptr<T>>
	{};
}

//Put this boilerplate code in the header, replacing RESULTING_BASE_CLASS and CONSTRUCTOR_ARG_TYPES

//start of registration boilerplate from MUQ/Utilities/RegisterClassNameHelper.h, should be public ************************************
// typedef boost::function<std::shared_ptr<RESULTING_BASE_CLASS> (CONSTRUCTOR_ARG_TYPES)> FactorySignature;
// 
// typedef std::map<std::string, FactorySignature> FactoryMapType;
// 
// 				   
// ///Boilerplate code to have a safe, static copy of a map from names to constructors. 
// static std::shared_ptr<FactoryMapType> GetFactoryMap()
//   {
//     static std::shared_ptr<FactoryMapType> map;
// 
//     // if the map doesn't exist yet, create it
//     if (!map) {
//       map = std::make_shared<FactoryMapType>();
//     }
// 
//     return map;
//   }
//end of registration boilerplate ************************************

//also define this, and call it within the cpp for child classes
//#define REGISTER_YOURCLASSNAME(NAME) static auto reg##NAME = FULLY_QUALIFIED_BASE_NAME::GetFactoryMap()->insert(std::make_pair(#NAME,boost::shared_factory<NAME>()));

//a create method might look like this:
//   std::shared_ptr<YOURCLASSNAME> CLASS::Create(CONSTRUCTOR_ARG_TYPES)
//   {
	   //assert that it's actually present
// 	   assert(GetFactoryMap()->find(CHILD_NAME_AS_STRING) != GetFactoryMap()->end());
// 	  //lookup the one we want and construct it
// 	  return GetFactoryMap()->at(CHILD_NAME_AS_STRING)(CONSTRUCTOR_ARG_TYPES);
//   }
#endif //REGISTERCLASSNAMEHELPER_H_