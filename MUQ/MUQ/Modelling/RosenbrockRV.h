#ifndef ROSENBROCKRV_H_
#define ROSENBROCKRV_H_

#include "MUQ/config.h"

#if MUQ_PYTHON == 1
#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"
#endif // MUQ_PYTHON == 1

#include "MUQ/Modelling/GaussianRV.h"
#include "MUQ/Modelling/RosenbrockSpecification.h"

namespace muq { 
  namespace Modelling { 
    class RosenbrockRV : public RandVar {
    public:

      RosenbrockRV(std::shared_ptr<RosenbrockSpecification> const& specification);

      template<typename ... Args>
	RosenbrockRV(Args ... args) : RosenbrockRV(std::make_shared<RosenbrockSpecification>(args ...)) {}

      virtual Eigen::MatrixXd Sample(std::vector<Eigen::VectorXd> const& input, int numSamps = 1) override;

      virtual Eigen::MatrixXd Sample(int numSamps = 1) override;

#if MUQ_PYTHON == 1
      boost::python::list PySample();
      boost::python::list PySampleInputs(boost::python::list const& inputs);
      boost::python::list PySamples(int numSamps);
      boost::python::list PySamplesInputs(boost::python::list const& inputs, int numSamps);
#endif // MUQ_PYTHON == 1

    private:
      
      /**
	 @param[in] input Inputs to compute parameters
       */
      virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& input) override;

      /// Specification to determine parameters 
      std::shared_ptr<RosenbrockSpecification> specification;

      /// A 2D standard Gaussian 
      /**
	 Samples from this will be transformed into samples from the Rosenbrock distribution
       */
      std::shared_ptr<GaussianRV> gaussian = std::make_shared<GaussianRV>(2);

    };
  } // namespace Modelling
} // namespace muq

#endif
