
#ifndef RandVarBase_h_
#define RandVarBase_h_

#include <memory>

#include <Eigen/Core>

#include "MUQ/Utilities/RandomGenerator.h"

namespace muq {
namespace Modelling {
/** Define a smart pointer type for smart pointers to Random Variables. */
class RandVarBase;


// use boost smart pointers
typedef std::shared_ptr<RandVarBase> RVptr;

/** @ingroup Modelling
 *   @author Matthew Parno
 *   @brief Abstract base for Random Variables
 *  @details Define an abstract base class for Random variables.  Random variable classes will have a sample() function
 *  that fills in a MuqVectorBase with a random realization of the variable.
 */
class RandVarBase {
public:

  /** Purely virtual function that will fill in a MuqVectorBase with a sample of this random variable. */
  virtual void sample(Eigen::VectorXd& samp) = 0;

  /** return the dimension of the random variable.  i.e. how long is the sample vector? */
  virtual int  GetDimOut() const
  {
    return DimOut;
  }

protected:

  /** The dimension of the random variable. */
  int DimOut;
};

// RandVarBase class definition
} // namespace Modelling
} // namespace muq

#endif // ifndef RandVarBase_h_
