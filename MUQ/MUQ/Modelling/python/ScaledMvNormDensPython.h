
#ifndef SCALEDMVNORMDENSPYTHON_H_
#define SCALEDMVNORMDENSPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Modelling/ScaledMvNormDens.h"

#include "MUQ/Utilities/python/PythonTranslater.h"

namespace muq {
namespace Modelling {
/// Python wrapper around muq::Modelling::ScaledMvNormDens
class ScaledMvNormDensPython : public ScaledMvNormDens, public boost::python::wrapper<ScaledMvNormDens> {
public:

  /// Constructor using a specification
  ScaledMvNormDensPython(std::shared_ptr<ConstantMVNormSpecification> const& specification);

  /// Gaussian with identity covariance and zero mean of dimension dimIn
  ScaledMvNormDensPython(int dimIn);

  /// Gaussian with mean and scaled identity covariance
  ScaledMvNormDensPython(boost::python::list const& meanIn, double scalarCovariance);

  ScaledMvNormDensPython(Eigen::VectorXd const                           & meanIn,
                         Eigen::VectorXd const                           & covDiag,
                         ConstantMVNormSpecification::SpecificationMode    mode =
                           ConstantMVNormSpecification::SpecificationMode::DiagonalCovariance);

  ScaledMvNormDensPython(Eigen::VectorXd const                           & meanIn,
                         Eigen::MatrixXd const                           & cov,
                         ConstantMVNormSpecification::SpecificationMode    mode =
                           ConstantMVNormSpecification::SpecificationMode::CovarianceMatrix);

  static std::shared_ptr<ScaledMvNormDensPython> Create(boost::python::list const& meanIn,
                                                        boost::python::list const& covIn,
                                                        bool                       isDiag);

  static std::shared_ptr<ScaledMvNormDensPython> CreateMode(boost::python::list const                    & meanIn,
                                                            boost::python::list const                    & covIn,
                                                            bool                                           isDiag,
                                                            ConstantMVNormSpecification::SpecificationMode mode);
};

void ExportScaledMvNormDens();
} // namespace modelling
} // namespace muq

#endif // ifndef MVNORMDENSPYTHON_H_
