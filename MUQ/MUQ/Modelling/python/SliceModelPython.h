#ifndef SLICEMODELPYTHON_H_
#define SLICEMODELPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"

#include "MUQ/Modelling/SliceModel.h"

namespace muq {
namespace Modelling {
class SliceModelPython : public SliceModel, public boost::python::wrapper<SliceModel> {
public:

  /** Construct from a start index, end index, and skip interval.  Both indices are inclusive */
  SliceModelPython(int inputDim, int startIndIn, int endIndIn, int skipIn = 1);

  /** Construct from a vector of the the indices to keep. */
  SliceModelPython(int inputDim, boost::python::list const& indicesIn);

private:

  static std::vector<unsigned int> GetIndicesIn(boost::python::list const& indicesIn);
};

void                               ExportSliceModel();
}
}

#endif // ifndef SLICEMODELPYTHON_H_
