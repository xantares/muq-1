
#ifndef LinearModel_h_
#define LinearModel_h_

#include "MUQ/Modelling/ModPieceTemplates.h"
#include <MUQ/Modelling/ModGraph.h>

namespace muq {
namespace Modelling {

/** \author Matthew Parno
 *  \brief Linear operator forward model.
 *  @ingroup Modelling
 *  @class LinearModel
 *  \details This class represents the action of a matrix on the input.  So, when Update() is called, the input vector
 *is multiplied by the matrix stored in this class and the resulting matvec is stored as the state vector.
 */
class LinearModel : public OneInputJacobianModPiece {
public:

  friend std::shared_ptr<ModGraph> operator+(const Eigen::VectorXd& b, std::shared_ptr<ModGraph> x);


  /** Construct the LinearModel from and Eigen::MatrixXd.  This class returns a vector of the form Ax+b.  In general,
   * this constructor will take A as an input and set b to zero.  However, there are times when we may wish to set A to
   * the identity and use Ain to set b.  The useIdentityMatrix allows the user to enforce this behavior.  When
   * useIdentityMatrix is false, the input matrix can be rectangular and will be used to evaluate A*x.  On the other
   * hand, when useIdentityMatrix is true, this input matrix must be a column vector, and x+b will be returned.
   */
  LinearModel(const Eigen::MatrixXd& Ain, bool useIdentityMatrix = false);

  /**
   *   Construct the LinearModel from an Eigen::MatrixXd and an additive Eigen::VectorXd.
   */
  LinearModel(const Eigen::VectorXd& bin, const Eigen::MatrixXd& Ain, bool useAInverse = false);

  virtual ~LinearModel() = default;

  /// Compute the null space and its complement of A
  /**
   *  Compute the eigendecomposition of \f$\mathbf{A}^T \mathbf{A}\f$; the null space is spanned by the vectors of the
   *zero eigenvalues.
   *  @param[out] nullSpace Columns of this matrix span the null space
   *  @param[out] nullComplement Columns of this matrix span the null space complement (defaults to empty)
   */
  void NullSpace(Eigen::MatrixXd& nullSpace, Eigen::MatrixXd& nullComplement) const;

  /// Get Cholesky decomposition 
  Eigen::MatrixXd GetCholesky() const;
  
protected:

  Eigen::MatrixXd A; // the linear operator
  Eigen::VectorXd b; // offset

  /// Cholesky of Linear::Model::A (stored only if we want to apply A inverse)
  Eigen::MatrixXd L; 

  virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const& input) override;
  virtual Eigen::MatrixXd JacobianImpl(Eigen::VectorXd const& input) override;

  const bool isAffine, isId, useAInverse;
};
} // namespace Modelling
} // namespace muq

#endif // ifndef LinearModel_h_
