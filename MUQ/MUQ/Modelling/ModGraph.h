
#ifndef ModGraph_h_
#define ModGraph_h_

// standard library includes
#include <map>
#include <vector>
#include <string>
#include <memory>

// other external library includes
#include <boost/graph/adjacency_list.hpp>

#include <Eigen/Core>

namespace muq {
namespace Modelling {

// forward declarations of the modpiece classes
class ModPiece;
class ModPieceDensity;



class ModGraphNode {
public:

  ModGraphNode(const std::string& nameIn, std::shared_ptr<ModPiece> pieceIn) : name(nameIn), piece(pieceIn) {}

  // store a name for this node
  std::string name;

  // store a pointer to a ModPiece.
  std::shared_ptr<ModPiece> piece;
};


class ModGraphEdge {
public:

  ModGraphEdge(int dimIn) : dim(dimIn) {}

  int GetDim() const
  {
    return dim;
  }

private:

  int dim;
};


/** Here we define a few shortcuts to the type of Vertices and Edges we are using. */
typedef boost::adjacency_list < boost::vecS, boost::vecS, boost::bidirectionalS, std::shared_ptr<ModGraphNode>,
  std::shared_ptr < ModGraphEdge >> Graph;


class ModGraphPiece;

/** @class ModGraph
 *  @ingroup Modelling
 *  @brief A graph allowing for easily combining individual ModPieces into a more complicated model.
 *  @details This class wraps around the boost graph library to provide an efficient way of keeping track of
 * dependencies
 *     between
 *  models, performing chain rule differentiation, and extracting model structure.  Conceptually, this class keeps
 * pieces
 *     of a model
 *  (represented by a ModPiece) at each node in a graph and represents connections between model pieces with graph
 * edges.
 *      The strong
 *  relationship between model pieces, as implemented in the muq::Modelling::ModPiece class and this class, means that
 *     users interested
 *  in this class should understand the basics of muq::Modelling::ModPiece first.  Each node in this class is
 * corresponds
 *     to a ModPiece
 *  shared_ptr and a name.  The node names are used to query the graph, and to add/remove edges.
 */
class ModGraph {
public:

  // friend classes
  friend ModGraphPiece;

  ModGraph() = default;

  ///Create a graph with only the input node
  ModGraph(const std::shared_ptr<ModPiece>& node);

  virtual ~ModGraph() = default;

  /** Print the nodes and edges of this graph to std::cout. */
  void print() const;

  /** List the inputs to this graph (i.e. ModPiece inputs that do not correspond to any edge in the graph). */
  void printInputs() const;

  enum colorOptionsType {DEFAULT_COLOR, DERIV_COLOR, CALLS_COLOR, TIME_COLOR};
  
  /** Write a graphViz file or image of this graph.  The extension on the filename is used to determine the output
   *  format.
   * For example, if a supported image extension is used, then this function will call dot to draw the graph as an
   * image.
   * However, if any unsupported extension is used, this function will simply write a graphViz text file using the given
   * filename.
   * Typical usage will follow this example:
   *  @code myGraph.writeGraphViz("/path/to/outputFile.pdf"); @endcode
   *  @param[in] filename The file to create and write into.  Supported image extensions are: .png, .jpg, .tif, .eps, .pdf, and .svg
   *  @param[in] colorOption This is one of the values in the colorOptionsType enum.  
                 If ModGraph::DEFAULT_COLOR is passed, all interior nodes will be the same color.
                 If ModGraph::DERIV_COLOR is passed, then the nodes will be colored by the type of direct derivative information that is avaiable (i.e. hasDirectGradient, etc...)
                 If ModGraph::CALLS_COLOR is passed, the nodes will be colored by the number of times EvaluateImpl has been called.
                 If ModGraph::TIME_COLOR is passed, the nodes will be color by the average run time of the EvaluateImpl function. 
   *  @param[in] addDerivLabel If true, a four booleen variables will be added to each node based on available derivative information.
   *             The string is (hasDirectGradient, hasDirectJacobian, hasDirectJacobianAction,hasDirectHessian)
   */
  void writeGraphViz(const std::string& filename, colorOptionsType colorOption=DEFAULT_COLOR, bool addDerivLabel=false) const;


  /** Add an edge from the node with name "nameFrom" to the inputDim input of the node with name "nameTo"
   *  @param[in] nameFrom The name of the upstream node.
   *  @param[in] nameTo The name of the downstream node.
   *  @param[in] inputDim The input dimension of "nameTo" that will be given the output of "nameFrom"
   */
  void AddEdge(const std::string& nameFrom, const std::string& nameTo, int inputDim);

  /** Remove an edge from the graph.
   *  @param[in] nameFrom The name of the upstream node.
   *  @param[in] nameTo The name of the downstream node.
   *  @param[in] inputDim The input dimension of "nameTo" that will be given the output of "nameFrom"
   */
  void RemoveEdge(const std::string& nameFrom, const std::string& nameTo, int inputDim);


  /** Add a new node to the graph.
   *  @param[in] Input A ModPiece shared_ptr that will be called when this node is evaluated.
   *  @param[in] name A string representing a <b>unique</b> name for this node.
   */
  void AddNode(std::shared_ptr<ModPiece> Input, const std::string& name);

  /** Get the ModPiece used at a particular node in the graph.
   *  @param[in] name The name of the node of interest
   *  @return a copy of the ModPiece shared_ptr used at "name"
   */
  std::shared_ptr<ModPiece> GetNodeModel(const std::string& name);

  /** Swap the ModPiece at a given node with a new ModPiece and optionally update the name of the node.
   *  @param[in] currName The name of the node we wish to swap before the swap occurs
   *  @param[in] Input The new ModPiece shared_ptr
   *  @param[in] newName A string holding the new name for the node previously called currName.
   */
  void                      SwapNode(const std::string       & currName,
                                     std::shared_ptr<ModPiece> Input,
                                     const std::string       & newName = "");
  ///Move all the inputs from node called oldNode to a node called newNode, preserving ordering.
  void     MoveInputEdges(const std::string& oldNode, const std::string& newNode);
  ///Move all the outputs from node called oldNode to a node called newNode, preserving ordering.
  void     MoveOutputEdges(const std::string& oldNode, const std::string& newNode);

  /** Remove a node and all related edges from the graph.
   *  @param[in] nodeName A string defining the node we want to remove.
   */
  void     RemoveNode(const std::string& nodeName);

  /** Fix the value of a particular node to a constant value and remove any input edges from that node.  After binding a
   *  node or edge,
   * if DependentCut is called, any group of constant nodes will be grouped into a single node.  This is a useful
   * feature when offline
   * computations are required for a model.
   *  @param[in] nodeName The node to bind
   *  @param[in] x The value that "nodeName" will be fixed at.  Note that x must be the same size of the existing output
   * of "nodeName"
   */
  void     BindNode(const std::string& nodeName, const Eigen::VectorXd& x);

  /** Like BindNode, but only for an edge.  This function actually creates a new node in the system and fixes the value
   *  of that node.
   * The new node name is constructed using @code nodeName+"_FixedInput"+std::to_string(inputDim) @endcode  As a naming
   * example,
   * if nodeName is "x" and inputDim is 2, this function will create a new node with name "x_FixedInput2"
   *  @param[in] nodeName The name of the existing node whose input we wish to set
   *  @param[in] inputDim The input of "nodeName" to fix
   *  @param[in] x The value the input should be set to
   */
  void     BindEdge(const std::string& nodeName, int inputDim, const Eigen::VectorXd& x);

  /** Create a modpiece that includes all dependencies of the output, furthermore, lump any bound pieces into a single
   *  Constant Modpiece
   */
  std::shared_ptr<ModGraph> DependentCut(const std::string& nameOut) const;

  ///Construct a new graph that provides the output of the provided node as an input. Remove unnecessary edges and
  // nodes. Does not modify initial graph.
  std::shared_ptr<ModGraph> ReplaceNodeWithInput(const std::string& inputName, const std::string& outputName);

  /** Get the number of inputs (i.e. unset ModPiece inputs) in this graph. */
  int      NumInputs() const;

  /** Get the number of outputs (i.e. unused ModPiece outputs) in this graph. */
  int                       NumOutputs() const;

  /** Get a vector with the names of outputs (i.e. unused ModPiece outputs) in this graph.  */
  std::vector<std::string>  GetOutputNames() const;

  /**Get the names of the input nodes.**/
  std::vector<std::string> GetInputNames() const;
  
// Return the names of the input nodes for a specified output
  /**
   *  @param[in] outputName The name of the output node, if it is empty (defualt) return all of the input node names
   */
  std::vector<std::string> InputNames(std::string const& outputName = "") const;
  
  
  /** Return the number of edges in this graph. */
  int      NumEdges() const
  {
    return boost::num_edges(ModelGraph);
  }

  /** Return the number of nodes in this graph. */
  int NumNodes() const
  {
    return boost::num_vertices(ModelGraph);
  }

  /** Are all the ModPieces in this graph capable of efficiently computing a gradient? */
  bool            allDirectGradient() const;

  /** Are all the ModPieces in this graph capable of efficiently constructing the Jacobian matrix? */
  bool            allDirectJacobian() const;

  /** Are all the ModPieces in this graph capable of efficiently computing the action of the Jacobian on a vector? */
  bool            allDirectJacobianAction() const;

  /** Are all the ModPieces in this graph capable of efficiently constructing the Hessian? */
  bool            allDirectHessian() const;

  /** Do any of the ModPieces used in this graph produce random output? */
  bool            anyRandom() const;

  /** Get the sizes of all the inputs in this graph.  This is similar to the muq::Modelling::ModPiece::inputSizes
   *  variable. */
  Eigen::VectorXi inputSizes() const;

  /** If a node name is passed to this function, the output size of that node is returned.  Alternatively, when
   *  NumOutputs() returns 1, and no name is given, the size of the only unused ModPiece output is used.*/
  int             outputSize(const std::string& nodeName = "") const;

  /**
   * Get the name of the output node of the graph. Asserts that NumOutputs()==1.
   */
  std::string     GetOutputNodeName() const;

  /**
   * A convenience method, set the name of the output node of the graph. Asserts that NumOutputs()==1.
   */
  void            SetOutputNodeName(std::string const& newName);

  /** Does the node "name" depend on any of the unset inputs in this graph?
   *  @param[in] name The name of the node of interest
   *  @return True is "name" will remain constant regardless of the values at the remaining ModPiece inputs, and false
   *     otherwise.
   */
  bool            isConstant(const std::string& name);

  /** If a node is constant (as defined in muq::Modelling::ModGraph::isConstant), this function will return the constant
   *  value.
   *  @param[in] name The name of the node of interest
   *  @return The value of the output from "name"
   */
  Eigen::VectorXd GetConstantVal(const std::string& name) const;

  /** Is node "name2" dependent on the output of "name1"  i.e. will information from v1 flow to v2?
   *  @param[in] name1 The name of the potential upstream node
   *  @param[in] name2 The name of the potential downstream node
   */
  bool            NodesConnected(const std::string& name1, const std::string& name2) const;


  /** Use the graph to create a ModPiece that can be evaluated. */
  static std::shared_ptr<ModGraphPiece>   ConstructModPiece(std::shared_ptr<ModGraph> graph,
                                                            const std::string       & outNode);
  static std::shared_ptr<ModPieceDensity> ConstructDensity(std::shared_ptr<ModGraph> graph, const std::string& outNode);
  static std::shared_ptr<ModGraphPiece>   ConstructModPiece(std::shared_ptr<ModGraph>       graph,
                                                            const std::string             & outNode,
                                                            const std::vector<std::string>& inputOrder);
  static std::shared_ptr<ModPieceDensity> ConstructDensity(std::shared_ptr<ModGraph>       graph,
                                                           const std::string             & outNode,
                                                           const std::vector<std::string>& inputOrder);

  /// check to see if a particular node exists
  bool         NodeExists(const std::string& name) const;

  /// Return the model graph
  inline Graph GetModelGraph()
  {
    return ModelGraph;
  }

  std::shared_ptr<ModGraph> DeepCopy();

  // boost directed graph of models
  Graph ModelGraph;

  /**
   * @brief Return a new graph that is the union of two others. Enforce uniqueness of the node names.
   *
   * In essence, this method just creates a new graph that is the union of the nodes and edges in both graphs.
   * However, for that to make sense, we have to enforce some rules to make sure it does something sensible.
   * # Each node and edge from both graphs are copied into the result, without changing names.
   * # A node's name in the resulting graph must be unique
   * # If two nodes in different graphs have the same name, they must point to the same object. This means
   * they can be merged in the resulting graph. If not, it will fail an assert.
   *
   * @param a A graph to merge.
   * @param b A graph to merge.
   * @return std::shared_ptr< muq::Modelling::ModGraph >
   */

  static std::shared_ptr<ModGraph> FormUnion(std::shared_ptr<ModGraph> a, std::shared_ptr<ModGraph> b);

protected:

  // get a vertex_iterator to the node with name "name"
  boost::graph_traits<Graph>::vertex_iterator GetNodeIterator(const std::string& name) const;

  // recursively go upstream from a node, copying nodes and lumping any contiguous constant nodes together
  void                                        RecursiveCopyAndCut(
    const boost::graph_traits<Graph>::vertex_descriptor& vOld,
    const boost::graph_traits<Graph>::vertex_descriptor& vNew,
    std::shared_ptr<ModGraph>                            gNew) const;

private:

  // are two nodes in the graph connected?  i.e. will information from v1 flow to v2?
  bool NodesConnected(const boost::graph_traits<Graph>::vertex_descriptor& v1,
                      const boost::graph_traits<Graph>::vertex_descriptor& v2) const;


  // get the outputsize of the node with vertex_descriptor v
  int outputSize(const boost::graph_traits<Graph>::vertex_descriptor& v) const;

  // find the intputs to the graph
  std::vector < std::pair < boost::graph_traits<Graph>::vertex_descriptor, int >> GraphInputs() const;

  // check to see if a node is constant, i.e. only depends (directly or indirectly) on ConstantPiece nodes
  bool                                        isConstant(boost::graph_traits<Graph>::vertex_descriptor v) const;

  // get the constant value of the node with vertex_descriptor v
  Eigen::VectorXd                             GetConstantVal(boost::graph_traits<Graph>::vertex_descriptor v) const;

  // get a vertex_iterator to the node holding a copy of ptr
  boost::graph_traits<Graph>::vertex_iterator GetNodeIterator(std::shared_ptr<ModPiece> ptr) const;
};
}
}


#endif // ifndef ModGraph_h_
