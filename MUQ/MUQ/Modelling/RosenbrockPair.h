#ifndef ROSENBROCKPAIR_H_
#define ROSENBROCKPAIR_H_

#include "MUQ/Modelling/RosenbrockRV.h"
#include "MUQ/Modelling/RosenbrockDensity.h"
#include "MUQ/Modelling/DensityRVPair.h"

namespace muq {
namespace Modelling {
typedef DensityRVPair<RosenbrockRV, RosenbrockDensity, RosenbrockSpecification> RosenbrockPair;
}
}

#endif
