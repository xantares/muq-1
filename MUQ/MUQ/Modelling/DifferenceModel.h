
#ifndef DIFFERENCEMODEL_H_
#define DIFFERENCEMODEL_H_

#include "MUQ/Modelling/ModPiece.h"

namespace muq {
namespace Modelling {
/// A model that returns the difference of twoinputs
/**
 *  Takes two inputs (of the same length) and returns their componentwise difference.
 */
class DifferenceModel : public ModPiece {
public:

  // Basic constructor
  /**

   *  @param[in] outputSize The output size (and the input size for ALL input vectors)
   */
  DifferenceModel(int const outputSize);

  virtual ~DifferenceModel() = default;
private:

  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs) override;

  virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& inputs, int const inputDimWrt) override;

  virtual Eigen::VectorXd JacobianActionImpl(std::vector<Eigen::VectorXd> const& inputs,
                                             Eigen::VectorXd const             & target,
                                             int const                           inputDimWrt) override;

  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& inputs,
                                       Eigen::VectorXd const             & sens,
                                       int const                           inputDimWrt) override;

  virtual Eigen::MatrixXd HessianImpl(std::vector<Eigen::VectorXd> const& inputs,
                                       Eigen::VectorXd const             & sens,
                                       int const                           inputDimWrt) override;

};
} // namespace Modelling
} // namespace muq

#endif // ifndef DIFFERENCEMODEL_H_
