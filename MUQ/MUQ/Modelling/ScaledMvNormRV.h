#ifndef SCALEDMVNORMRV_H_
#define SCALEDMVNORMRV_H_


#include <memory>

#include "MUQ/Modelling/RandVar.h"
#include "MUQ/Modelling/ConstantMVNormSpecification.h"

#include "MUQ/Modelling/MvNormRV.h"

namespace muq {
namespace Modelling {
class ScaledMvNormRV : public RandVar {
public:

  ScaledMvNormRV(std::shared_ptr<ConstantMVNormSpecification> specification) :
    RandVar(Eigen::VectorXi::Ones(1), specification->dim, false, false, false, false), specification(specification)
  {}

  template<typename ... Args>
  ScaledMvNormRV(Args ... args) : ScaledMvNormRV(std::make_shared<ConstantMVNormSpecification>(args ...)) {}

  std::shared_ptr<ConstantMVNormSpecification> const specification;

  virtual Eigen::MatrixXd Sample(std::vector<Eigen::VectorXd> const& input, int NumSamps = 1) override;

  virtual Eigen::VectorXd TransformStandardNormalSample(Eigen::VectorXd const& input, double const sigma);

private:

  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& input) override;
};
};
};

#endif // ifndef SCALEDMVNORMRV_H_
