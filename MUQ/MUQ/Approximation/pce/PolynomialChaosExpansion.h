#ifndef POLYNOMIALCHAOSEXPANSION_H_
#define POLYNOMIALCHAOSEXPANSION_H_

#include <vector>
#include <set>

#include <boost/serialization/access.hpp>
#include <boost/tuple/tuple.hpp>
#include <Eigen/Core>

#include "MUQ/Utilities/VariableCollection.h"
#include "MUQ/Approximation/Expansions/PolynomialExpansion.h"

#if MUQ_PYTHON == 1
#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"
#endif // if MUQ_PYTHON == 1


namespace muq {
namespace Approximation {

class GlobalPolyRegressor;

class SingleQuadraturePCEFactory;
class SmolyakPCEFactory;
class SmolyakTraditionalPCEFactory;

//class PolynomialChaosExpansion;

//std::ostream& operator<<(std::ostream& output, const PolynomialChaosExpansion& pce);
  
/**
 @class PolynomialChaosExpansion
 @ingroup PolynomialChaos
 @brief A class for representing and using expansions of orthogonal multivariate polynomials
 @details
 * A particular polynomial chaos expansion for a function from R^n->R^m. This class uses
 * some subclass of MulitIndexFamily to defines which PCE terms are in the expansion.
 * For each PCE term and output, there is a coefficient. The PCE is built from 1D polynomials
 * specified in a VariableCollection, so each dimension may have independent polynomial
 * choices.
 *
 * The only user function is Evaluate(), which evaluates the PCE at a point. There is no public
 * constructor, because there's no reasonable expectation that the user knows the
 * terms to keep and the coefficients necessary to represent any given function. Instead,
 * user code calls a factory that can create a PCE that approximates their input function.
 * These factories are given access to the private data structures by being friends of this
 * class.
@see PolynomialExpansion, SingleQuadraturePCEFactory, SmolyakPCEFactory
 */
class PolynomialChaosExpansion : public PolynomialExpansion {

  ///Allows polynomial chaos expansion to be printed.
//  friend std::ostream& operator<<(std::ostream& output, const PolynomialChaosExpansion& pce);

  ///Allows classes that construct PCEs access to the internals.
  friend class SingleQuadraturePCEFactory;
  friend class SmolyakPCEFactory;
  friend class SmolyakTraditionalPCEFactory;
  friend class GlobalPolyRegressor;

  ///Allow serialization access
  friend class boost::serialization::access;

public:
  PolynomialChaosExpansion(PolynomialExpansion const& expand) : PolynomialExpansion(expand){};
  
  PolynomialChaosExpansion(Eigen::MatrixXd const& coeffs,
                          std::shared_ptr<muq::Utilities::MultiIndexSet> multiSet,
                          std::shared_ptr<muq::Utilities::RecursivePolynomialFamily1D> poly);
  
  PolynomialChaosExpansion(Eigen::MatrixXd const& coeffs,
                           std::shared_ptr<muq::Utilities::MultiIndexSet> multiSet,
                           std::vector<std::shared_ptr<muq::Utilities::RecursivePolynomialFamily1D>> polys);
  
  PolynomialChaosExpansion(int outputDim,
                           std::shared_ptr<muq::Utilities::MultiIndexSet> multiSet,
                           std::shared_ptr<muq::Utilities::RecursivePolynomialFamily1D> poly);
      
  PolynomialChaosExpansion(int outputDim,
                           std::shared_ptr<muq::Utilities::MultiIndexSet> multiSet,
                           std::vector<std::shared_ptr<muq::Utilities::RecursivePolynomialFamily1D>> polys);
                          
  typedef std::shared_ptr<PolynomialChaosExpansion> Ptr;

  virtual ~PolynomialChaosExpansion() = default;


  ///Print the pce to basename+"_pce.dat" using the << operator.
  void  print(std::string basename);

  ///Print the pce with normalized coefficients to basename+"_pce.dat" using the << operator.
  void  printNormalized(std::string basename);

  // ///Return a pointer to a new PCE, a-b.
//   static PolynomialChaosExpansion::Ptr SubtractExpansions(PolynomialChaosExpansion::Ptr a,
//                                                           PolynomialChaosExpansion::Ptr b);

  /** This function returns the multiindices used in this expansion.
      Each row of the returned matrix defines a single term in the PCE.  The columns represent the input variables.
   */
  //Eigen::MatrixXu GetMultiIndices() const;
  
  /** This function returns the coefficients for each output dimension of the expansion.
      Each row of the coefficient matrix corresponds to an output of the expansion.  Thus, 
      the number of columns in the expansion is equal to the number of terms in the multiindex set
      returned by GetMultiIndices().
   */
  //Eigen::MatrixXd GetCoefficients() const;
  
  ///compute the variance of the current expansion
  Eigen::VectorXd ComputeVariance() const;
  Eigen::MatrixXd ComputeCovariance() const;
  Eigen::VectorXd ComputeMean() const;

  /// get the derivative of the variances wrt the polynomial coefficients -- useful in constrained regression
  Eigen::MatrixXd ComputeVarianceJacobian() const;

  /** Compute the gradient of the variance in dimension outInd to the polynomial coefficients. */
  Eigen::VectorXd ComputeVarianceGradient(int outInd) const;

  /** Compute the Hessian of the variance of a single output dimension with respect to the polynomial coefficients.
   *   Since the variance expression is qudratic, this Hessian is constant and thus does not depend on the polynomial
   *  coefficients.  For that reason there is no dimension input, even though the output matrix is the Hessian of a
   *  single output variance to the polynomial coefficients.
   */
  Eigen::MatrixXd ComputeVarianceHessian() const;


  ///Compute the L2 norm of each output.
  Eigen::VectorXd ComputeMagnitude() const;

  /**
   * Compute the weighted sum of polynomial expansions. Slow, because it assumes you haven't been
   * tracking which polynomials to keep, so it does that, then calls the private method. If you
   * know already, your class should be a friend and use the private method directly, as that
   * will be faster.
   * */
  static std::shared_ptr<PolynomialChaosExpansion> ComputeWeightedSum(std::vector<std::shared_ptr<PolynomialChaosExpansion>> expansions,
                                                                      Eigen::VectorXd                                 const& weights); 

  //unsigned int    NumberOfPolynomials() const;

  ///Load a PCE using boost::serialization and return the result
  //static Ptr      LoadFromFile(std::string fileName);

  ///Save a PCE using boost::serialization
  //static void     SaveToFile(PolynomialChaosExpansion::Ptr pce, std::string fileName);

  ///Compute the Sobol total sensitivity index for the input dimension, for each output dimension
  Eigen::VectorXd ComputeSobolTotalSensitivityIndex(unsigned const targetDim) const;

  ///Compute all Sobol total sensitivities. Rows are outputs, each column is an input.
  Eigen::MatrixXd ComputeAllSobolTotalSensitivityIndices() const;

  ///Compute the main sensitivity index for the input dimension, for each output dimension
  Eigen::VectorXd ComputeMainSensitivityIndex(unsigned const targetDim) const;

  ///Compute all the main sensitivities. Rows are outputs, each column is an input.
  Eigen::MatrixXd ComputeAllMainSensitivityIndices() const;

  ///Constructor for serialization
  PolynomialChaosExpansion(int const inputSize, int const outputSize);

  ///Load an expansion using boost::serialization and return the result
  static std::shared_ptr<PolynomialChaosExpansion> LoadFromFile(std::string fileName);

  ///Save an expansion to a file using boost::serialization
  static void SaveToFile(std::shared_ptr<PolynomialChaosExpansion> expansion, std::string fileName);

  
#if MUQ_PYTHON == 1
  
  boost::python::list PyComputeVariance() const;
  boost::python::list PyComputeCovariance() const;
  boost::python::list PyComputeMean() const;
  boost::python::list PyComputeVarianceJacobian() const;
  boost::python::list PyComputeVarianceGradient(int outInd) const;
  boost::python::list PyComputeVarianceHessian() const;
  boost::python::list PyComputeMagnitude() const;
  boost::python::list PyComputeSobolTotalSensitivityIndex(unsigned const targetDim) const;
  boost::python::list PyComputeAllSobolTotalSensitivityIndices() const;
  boost::python::list PyComputeMainSensitivityIndex(unsigned const targetDim) const;
  boost::python::list PyComputeAllMainSensitivityIndices() const;
  
#endif // if MUQ_PYTHON == 1
  
  
private:

  ///Extract the polynomial information from the variable collection because that is all the expansion really needs.
  static std::vector<std::shared_ptr<muq::Utilities::RecursivePolynomialFamily1D>> Variables2Polys(std::shared_ptr<muq::Utilities::VariableCollection> variables);
  
  /**
   * Constructor for a PCE, takes the specification of the variables. Since it
   * can't set up the multi-index family or set coefficients, it's not useful yet.
   * Hence, the constructor is private and not callable by users.
   * @param variables
   * @return
   */
  PolynomialChaosExpansion(muq::Utilities::VariableCollection::Ptr variables,
                           int outputSize);
  
  PolynomialChaosExpansion(std::vector<std::shared_ptr<muq::Utilities::RecursivePolynomialFamily1D>> polys,
                           int outputSize);
                          
// 
//   ///Implements evaluation of one vector
//   Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const& vals) override;
// 
//   ///Compute the Jacobian matrix at a point analytically, the matrix is output x inputs.
//   Eigen::MatrixXd JacobianImpl(Eigen::VectorXd const& point) override;

  ///The serialization operator.
  template<class Archive>
  void serialize(Archive& ar, const unsigned int version);


  /**
   * Evaluates one PCE basis function, specified by the input multiIndex and
   * the 1D polynomials given in the VariableCollection, at the input point.
   * NB: Does not apply any coefficients
   *
   * @param multiIndex The multi-index specifying the polynomial basis to evaluate.
   * @param vals The point to evaluate the polynomial basis function.
   * @return The PCE basis function's value.
   */
//  double EvaluateOneTerm(Eigen::RowVectorXu const& multiIndex, Eigen::VectorXd const& vals) const;

  ///Get the squared normalization of a PCE basis function.

  ///Return a vector with the normalization of all the terms.

  /**
   * This function returns the sqrt of the normalization, sqrt(<p*p>),
   * for each PCE basis function p.
   * NB: This function does not change depending on the coefficients.
   */
  Eigen::VectorXd                      GetNormalizationVec() const;
  
  /**
   * An internal function to compute the weighted sum of polynomial expansions if you already know which polynomials are
   * included, where polynomials
   * is a clean copy not used by other expansions. Actually does the addition.
   * */
  static std::shared_ptr<PolynomialChaosExpansion> ComputeWeightedSum(std::vector<std::shared_ptr<PolynomialChaosExpansion>>                    expansions,
                                                                      Eigen::VectorXd const&                                                    weights,
                                                                      unsigned int const                                                        outputDim,
                                                                      std::shared_ptr<muq::Utilities::MultiIndexSet> const&                     polynomials,
                                                                      std::map<unsigned int,std::set<unsigned int> > const&                     basisMultiIndexCache);


  ///Holds the specification of the variables
  //muq::Modelling::VariableCollection::Ptr variables;

  /**
   * This mutli-index family defines the terms in the expansion.
   */
  //std::shared_ptr<muq::Utilities::MultiIndexFamily> terms;
};

///Provide a way to directly print pointers to pces
std::ostream& operator<<(std::ostream& output, const std::shared_ptr<PolynomialChaosExpansion>& pce);
}
}

namespace boost { 
namespace serialization {

  template<class Archive>
  inline void save_construct_data(
    Archive & ar, const muq::Approximation::PolynomialChaosExpansion * t, const unsigned int file_version){
    ar << t->inputSizes(0);
    ar << t->outputSize;
  }

  template<class Archive>
  inline void load_construct_data(
    Archive & ar, muq::Approximation::PolynomialChaosExpansion * t, const unsigned int file_version){
    
    int inputSize,outputSize;
    
    ar >> inputSize;
    ar >> outputSize;
    ::new(t)muq::Approximation::PolynomialChaosExpansion(inputSize,outputSize);
  }

} // namespace serialization
} // namespace boost


// ///Define a hash function
// namespace boost {
// namespace tuples {
// std::size_t hash_value(const boost::tuples::tuple<unsigned int, unsigned int>& b);
// }
// }
// 
// struct evalPointEqStruct {
//   bool operator()(boost::tuple<unsigned int,
//                                unsigned int> const& a, boost::tuple<unsigned int, unsigned int> const& b) const;
// };


#endif /* POLYNOMIALCHAOSEXPANSION_H_ */
