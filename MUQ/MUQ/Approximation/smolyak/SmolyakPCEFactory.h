
#ifndef SMOLYAKPCEFACTORY_H_
#define SMOLYAKPCEFACTORY_H_

#include "MUQ/config.h"

#include <boost/property_tree/ptree.hpp>

#if MUQ_PYTHON == 1

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"
#endif // if MUQ_PYTHON == 1


#include <string.h>
#include <set>
#include <map>

#include <boost/function.hpp>

#include <boost/serialization/access.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/set.hpp>

#include "MUQ/Approximation/smolyak/SmolyakEstimate.h"
#include "MUQ/Approximation/pce/PolynomialChaosExpansion.h"

namespace muq {
namespace Modelling {
class ModPiece;
}
namespace Utilities{
  class VariableCollection;
}
}

namespace muq {
namespace Approximation {

/**
 @class SmolyakPCEFactory
 @ingroup PolynomialChaos
 @brief Smolyak consruction of Polynomial Chaos Expansions
 @details
 * An implementation of Smolyak estimates for PCEs. This version constructs the
 * sparse interpolant, summing the PCEs associated with full tensor estimates
 * of the PCE.
 *
 * Probably best used with exponential growth or nested quadrature rules. Note
 * that if you use single step growth, it may fail to notice that a function
 * is even or odd, and may stop prematurely. Avoid this by using a
 * LinearGrowthQuadratureFamily1D and set the spacing to 2.
 *
 * The VariableCollection specifies the 1D quadrature rules to use and the associated
 * orthogonal polynomial families,
 * and the FunctionWrapper specifies the function.
 @see SmolyakEstimate, PolynomialChaosExpansion
 */
class SmolyakPCEFactory : public SmolyakEstimate<PolynomialChaosExpansion::Ptr> {
public:

  typedef std::shared_ptr<SmolyakPCEFactory> Ptr;

  /**
   * Initialize, providing the variables and function to approximate.
   */
  SmolyakPCEFactory(boost::property_tree::ptree const& pt,
                    std::shared_ptr<muq::Modelling::ModPiece> inFn);
  
  SmolyakPCEFactory(std::shared_ptr<muq::Utilities::VariableCollection>       inVariables,
                    std::shared_ptr<muq::Modelling::ModPiece> inFn,
                    unsigned int                              simplexLimit = 0);

  SmolyakPCEFactory(std::shared_ptr<muq::Utilities::VariableCollection> inVariables,
                    std::shared_ptr<muq::Modelling::ModPiece>           inFn,
                    std::shared_ptr<muq::Utilities::MultiIndexSet>      limitingSet);


  /**
   * @brief This computes with a fixed set of polynomials, adding quadrature as required.
   *
   * @param polys A matrix, the rows are the orders of the polynomials
   * @return A pointer to the PCE
   **/
  PolynomialChaosExpansion::Ptr ComputeFixedPolynomials(Eigen::MatrixXu polys);


  virtual ~SmolyakPCEFactory();

  ///Load progress from a file, the data ends up in the results directory.

  /**
   * Because saving function pointers is hard, you must provide a FunctionWrapper
   * that is the same as the one used when this was saved.
   */

  //       static Ptr LoadProgress(std::string baseName, boost::function<Eigen::VectorXd (Eigen::VectorXd const&)> fn);

  ///Save the progress so far using boost::serialization. Writes to results/
  //       static void SaveProgress(SmolyakPCEFactory::Ptr pceFactory, std::string baseName);

  virtual unsigned int ComputeWorkDone() const;

  ///Asks the quadrature rules what their precise polynomial order is.
  Eigen::MatrixXu      GetEffectiveIncludedTerms();


  ///Return the function we're constructing
  //       std::shared_ptr<muq::Utilities::FunctionWrapper> GetFunction();

#if MUQ_PYTHON == 1
  PolynomialChaosExpansion::Ptr PyComputeFixedPolynomials(boost::python::list const& polys);
  boost::python::list PyGetEffectiveIncludedTerms();
  
#endif
private:

  SmolyakPCEFactory();

  //       friend class boost::serialization::access;
  //
  //       template<class Archive>
  //       void serialize(Archive & ar, const unsigned int version);


  /**
   * Implements the method for computing a single multi-index estimate.
   *
   * It computes the coefficients of a PCE with full tensor quadrature of order
   * of the multiIndex. The PCE includes all the terms whose order is less than half
   * the total GetPrecisePolyOrder of the quadrature rule.
   */
  PolynomialChaosExpansion::Ptr ComputeOneEstimate(Eigen::RowVectorXu const& multiIndex);

  double                        CostOfOneTerm(Eigen::RowVectorXu const& multiIndex);


  /**
   * Computes the weighted sum of polynomials. Grabs the terms from within the parent
   * and returns a pointer to the result.
   */
  PolynomialChaosExpansion::Ptr WeightedSumOfEstimates(Eigen::VectorXd const& weights) const;

  /**
   * Compute the magnitude of a PCE by summing the absolute value of the coefficients
   * times the magnitude of the polynomial. Used for error estimation.
   */
  double                        ComputeMagnitude(PolynomialChaosExpansion::Ptr estimate);


  /** @brief Defines the MultiIndexLimiter to use during adaptation.
      @details In addition to the user-specific limit, the quadrature rule can sometimes place constraints on the terms used in the expansion.
               For example, MUQ does not define Gauss-Patterson quadrature past level 8 (which is accurate for  383 degree polynomials).  This function returns a MultiIndexLimiter that prevents the adaptive factory from going beyond both the user-specific simplex and the quadrature constraints.
      @param[in] simplexLimit The user specified total degree for the multiindex
      @param[in] inVariables A shared ptr to a variable collection holding the quadrature rules.
      @return A shared_ptr to a MultiIndexLimiter that returns true when the simplex limit and quadrature limits are satisfied.
   */
  static std::shared_ptr<muq::Utilities::MultiIndexLimiter> GetLimiter(unsigned int simplexLimit,
                                                                       std::shared_ptr<muq::Utilities::VariableCollection> inVariables);
  
  /** @brief Defines the MultiIndexLimiter to use during adaptation.
      @details Similar to the GetLimiter function above, this function returns a MultiIndexLimiter return true for the intersection of the limitingSet and the feasible quadrature limits.
      @params[in] limitingSet A shared_ptr to a multiindex set defining the feasible terms to include in the PCE.
      @params[in] inVariables A shared ptr to a variable collection holding the quadrature rules.
      @return A shared_ptr to a MultiIndexLimiter that returns true when the quadrature limits are satisifed and the MultiIndex is in the limitng set.
   */
  static std::shared_ptr<muq::Utilities::MultiIndexLimiter> GetLimiter(std::shared_ptr<muq::Utilities::MultiIndexSet>      limitingSet,
                                                                       std::shared_ptr<muq::Utilities::VariableCollection> inVariables);
  
  std::shared_ptr<muq::Utilities::VariableCollection> variables;

  std::shared_ptr<muq::Modelling::ModPiece> fn;

  std::shared_ptr<muq::Utilities::MultiIndexSet> polynomialBasisTermsEstimated;

  //For a map that goes from the index of the polynomial term to the index of the Smolyak
  //indices that create them
  std::map<unsigned int, std::set<unsigned int> > basisMultiIndexCache;
};
}
}


#endif /* SMOLYAKPCEFACTORY_H_ */
