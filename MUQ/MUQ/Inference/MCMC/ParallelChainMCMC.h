
#ifndef PARALLELMCMC_H
#define PARALLELMCMC_H

#include <vector>

#include <Eigen/Core>

#include "MUQ/Inference/ProblemClasses/InferenceProblem.h"
#include "MUQ/Inference/MCMC/MCMCBase.h"

namespace muq {
namespace Modelling {
class EmpiricalRandVar;
}

namespace Inference {
class SingleChainMCMC;

///A basic parallel chain MCMC, and the base class for more complex ones.

/**
 * The basic implementation creates a family of chains that simulate from the same distribution
 * and returns the concatenation of their results. Hence, performs the chain length * number of chains
 * samples of MCMC. All the chains start at the same state.
 *
 * ProposeSwaps
 *
 * SwapChains
 *
 * The sub-chains are constructed using the parameters, except that MCMC.Sampler is modified to
 * the value MCMC.ParallelChain.ChildSampler. Hence, the parameters for those child samplers
 * are pulled from the top level of the parameter tree.
 **/
class ParallelChainMCMC : public MCMCBase {
public:

  ///Construct the parallel chains from the properties
  ParallelChainMCMC(std::shared_ptr<muq::Inference::AbstractSamplingProblem> inferenceProblem,
                    boost::property_tree::ptree                            & properties);

  ///Use the input set of child MCMC methods
  ParallelChainMCMC(std::vector < std::shared_ptr < SingleChainMCMC >>       childrenMCMC,
                    std::shared_ptr<muq::Inference::AbstractSamplingProblem> inferenceProblem,
                    boost::property_tree::ptree                            & properties);
  virtual ~ParallelChainMCMC();


  virtual std::shared_ptr<muq::Modelling::EmpiricalRandVar> GetResults() override;

  virtual void                                              SetupStartState(Eigen::VectorXd const& xStart);

  REGISTER_MCMC_CONSTRUCTOR(ParallelChainMCMC)

protected:

  ///Actually swap the state of the two chains input.
  virtual void SwapChains(unsigned i, unsigned j);

  std::vector < std::shared_ptr < SingleChainMCMC >> childrenMCMC;

private:

  REGISTER_MCMC_DEC_TYPE(ParallelChainMCMC) void SampleOnce() override;

  ///Try swapping the states of the sub-chains. Call SwapChains to perform accepted swaps.

  /**
   * Default implementation performs no swaps.
   **/
  virtual void ProposeSwaps();
};
}
}
#endif // PARALLELMCMC_H
