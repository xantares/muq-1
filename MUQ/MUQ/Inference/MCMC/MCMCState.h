#ifndef MCMCSTATE_H_
#define MCMCSTATE_H_

// standard library headers
#include <assert.h>

// external library includes
#include <Eigen/Core>

#include "MUQ/Inference/MCMC/MCMCBase.h"
#include "MUQ/Inference/ProblemClasses/SamplingProblem.h"


namespace muq {
namespace Inference {
class AbstractSamplingProblem;


/**
 * The MCMCState class provides an abstraction for holding the state of an MCMC chain and all relevant
 * data products.
 * 
 * The MCMC state holds the state, and provides methods for getting the density, gradient,
 * and the like. These quantities are computed by the SamplingProblem or its children. These fields are
 * computed at construction time, as needed, and are generally not changed. The idea is that
 * an MCMC chain's state is defined by one of these objects, and that we can perform Proposal and Kernel
 * computations using only the values stored within, without further reference to the original problem.
 * This also serves to cache those results - if a state is held for many iterations in a row, we don't
 * want to keep computing the posterior repeatedly.
 *
 * To accomodate a variety of MCMC methods, the state holds a variety of values that correspond
 * to the state, which may or may not be needed. The MCMC method can set flags against in
 * the SamplingProblem that determine which ones are set and used. The temperature may be ignored
 * unless it is specifically needed since it defaults to 1.0. As long as the flags are set correctly,
 * extra values are not computed.
 **/
class MCMCState {
public:

  MCMCState(Eigen::VectorXd const          & state,
            double                           logLikelihood,
            boost::optional<Eigen::VectorXd> likelihoodGrad,
            boost::optional<double>          logPrior,
            boost::optional<Eigen::VectorXd> priorGrad,
            boost::optional<Eigen::MatrixXd> metric,
            boost::optional<Eigen::VectorXd> forwardModel,
            double                           temperature = 1.0);
  virtual ~MCMCState() = default;

  ///The actual state vector.
  Eigen::VectorXd const state;


  double          GetDensity() const;


  double          GetLikelihood() const;


  double          GetLikelihoodOtherTemp(double const& otherTemp) const;

  Eigen::VectorXd GetDensityGrad() const;

  Eigen::VectorXd GetLikelihoodGrad() const;
  
  bool HasForwardModel() const;

  Eigen::VectorXd GetForwardModel() const;

  Eigen::MatrixXd GetMetric() const;

  void            SetTemperature(double const temp);

  void            CloneFrom(std::shared_ptr<MCMCState> const other);

  double          GetTemperature();

private:

  double logLikelihood = 0;         //always used, even if there's just a single density to sample from
  boost::optional<Eigen::VectorXd> likelihoodGrad;
  boost::optional<double> logPrior; //only used for inference problems
  boost::optional<Eigen::VectorXd> priorGrad;
  boost::optional<Eigen::MatrixXd> metric;
  boost::optional<Eigen::VectorXd> forwardModel;

  double temperature = 1.0;
};
}
}

#endif //MCMCSTATE_H_