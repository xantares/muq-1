
#ifndef _InferenceProblemBase_h
#define _InferenceProblemBase_h

#include <Eigen/Core>

namespace muq {
namespace Inference {
/** @class InferenceProblemBase
 *   @ingroup Inference
 *   @brief Base class for Inference problems
 *   @details The goal of this class is to provide a common interface for describing inference problems that the MCMC
 * and
 *      MAP solvers can understand.  By distinguishing this from the muq::Modelling::Density class, we can separate the
 *      Inference and Modelling modules.  Moreover, child instances of this class can provide a more structured
 *      description of the problem by explicitly including the prior, likelihood, and posterior of a Bayesian inference
 *      problem.
 */
class InferenceProblemBase {
public:

  /** Evaluate the log density at the point xc
   *  @param[in] xc The location to evaluate the density.
   *  @return The log-density evaluated at xc
   */
  virtual double LogEval(const Eigen::VectorXd& xc) = 0;

  /** Evaluate the gradient of the log density at xc
   *  @param[in] xc The location we want to evaluate the density.
   *  @param[out] grad The gradient of the log-density evaluated at xc
   *  @return The log-density evaluated at xc
   */
  virtual double GradLogEval(const Eigen::VectorXd& xc, Eigen::VectorXd& grad);

  /** Return the parameter dimension.
   *  @return An integer containing the number of parameters in this problem
   */
  virtual int    GetDim() = 0;
};

// class InferenceProblemBase
} // namespace Inference
} // namespace muq


#endif // ifndef _InferenceProblemBase_h
