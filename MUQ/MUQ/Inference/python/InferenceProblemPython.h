
#ifndef INFERENCEPROBLEMPYTHON_H_
#define INFERENCEPROBLEMPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Inference/ProblemClasses/InferenceProblem.h"
#include "MUQ/Inference/ProblemClasses/SamplingProblem.h"

namespace muq {
namespace Inference {



void ExportProblemClasses();
} // namespace Inference
} // namespace muq

#endif // ifndef INFERENCEPROBLEMPYTHON_H_
