#ifndef GENZMAIN_HPP_
#define GENZMAIN_HPP_

#include <MUQ/Modelling/ModPieceTemplates.h>
#include <MUQ/Approximation/pce/PolynomialChaosExpansion.h>

void RunAdaptiveGenzBenchmarking(unsigned int,
                                 unsigned int,
                                 unsigned int, 
                                 unsigned int,
                                 unsigned int);

Eigen::VectorXd EstimateApproximationError( std::shared_ptr<muq::Modelling::OneInputNoDerivModPiece>, 
                                            std::shared_ptr<muq::Approximation::PolynomialChaosExpansion>,
                                            unsigned int );

#endif
