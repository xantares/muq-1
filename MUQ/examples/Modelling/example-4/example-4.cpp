/** \example modelling-example-4.cpp
 *
 *  <h3> About this example </h3>
 *  <p>This example builds on example-2.  example-2 showed how to define and evaluate a ModPiece with multiple inputs.
 *  This example takes it a step further.  This example illustrates how Jacobian information is defined in a ModPiece
 *  and uses the Jacobian to find a fixed point of the RHS model with a simple Newton iteration.
 *  Thus, the goal of this example is to solve
 *  \f{eqnarray*}{
 *  0 & = & rP\left(1-\frac{P}{K}\right) - s\frac{PQ}{a+P}\\
 *  0 & = & u\frac{PQ}{a+P} - vQ
 *  \f}
 *  using Newton's method.
 *  </p>
 *
 *  <h3>Line by line explanation</h3>
 *  <p>See example-4.cpp for finely documented code.</p>
 *
 */
// Just as before, we will need Eigen and std::cout
#include <Eigen/Dense>
#include <iostream>

// In this example, we will work directly from the ModPiece base class instead of the single input templates.  This line
// includes the ModPiece base directly.
#include <MUQ/Modelling/ModPiece.h>

// as before, we'll work in the muq::Modelling and std namespaces
using namespace muq::Modelling;
using namespace std;


/*
 * As in example-1, this class describes the right hand side of our predator prey model.
 */
class PredPreyModel : public ModPiece {
public:

  /** The bulk of this class is identical to exmample-2 with the notable addition of a JacobianImpl function.  The JacobianImpl
   *  function is similar in purpose to the EvaluateImpl function, but for a Jacobian evaluation.  Notice that in the constructor,
   *  the efficient Jacobian flag is turned to true.
   */
  PredPreyModel() : ModPiece(Eigen::Vector2i{2, 6},        // input sizes (this examples uses the c++11 ability to construct vectors with a curly bracketed array)
                             2,        // the output dimension, still 2 as in example-1
                             false,    // does this class provide an efficient implementation of the GradientImpl function?
                             true,     // does this class provide an efficient implementation of the JacobianImpl function?
                             false,    // does this class provide an efficient implementation of the JacobianActionImpl function?
                             false,    // does this class provide an efficient implementation of the HessianImpl function?
                             false) {} // does this class return a random result?

  // virtual default constructor as before
  virtual ~PredPreyModel() = default;

private:

  /** The EvaluateImpl function is identical to example-2.
   */
  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs) override
  {
    // for clarity, we grab the prey and predator populations from the input vector
    double preyPop = inputs.at(0) (0); // notice the indexing here, the .at(0) extracts the first vector valued input and the .at(0)(0) grabs the first entry in the first input vector
    double predPop = inputs.at(0) (1);

    // extract the model parameters from the input vector.  Note this requires an extra copy of the model parameters, but results in more readable code
    double preyGrowth, preyCapacity, predationRate, predationHunger, predatorLoss, predatorGrowth;

    preyGrowth      = inputs.at(1) (0);
    preyCapacity    = inputs.at(1) (1);
    predationRate   = inputs.at(1) (2);
    predationHunger = inputs.at(1) (3);
    predatorLoss    = inputs.at(1) (4);
    predatorGrowth  = inputs.at(1) (5);

    // create a vector to hold the ModPiece output, this will hold the time derivatives of population
    Eigen::VectorXd output(2);

    // compute the growth rates as we did in example-1
    output(0) = preyPop * (preyGrowth * (1 - preyPop / preyCapacity) - predationRate * predPop / (predationHunger + preyPop));
    output(1) = predPop * (predatorGrowth * preyPop / (predationHunger + preyPop) - predatorLoss);

    // return the output
    return output;
  }

  /** Much like the EvaluateImpl function, the JacobianImpl takes a vector holding the model inputs.  However, because there
   *  are multiple inputs in general, the JacobianImpl function also needs to know what input to take derivatives with 
   *  respect to.   The output of the function is the Jacobian matrix evaluated at the input point with derivatives taken
   *  wrt to the inputDimWrt input.  For models with multiple inputs, there is usually a sequence of if statements to compute
   *  the correct derivatives.  Also, just like the EvaluateImpl function, users should call the Jacobian function, which
   *  provides size checking and one step caching.
  */
  virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& inputs, int inputDimWrt) override
  {
    // for clarity, we grab the prey and predator populations from the input vector
    double preyPop = inputs.at(0) (0);
    double predPop = inputs.at(0) (1);

    // extract the model parameters from the input vector.  Note this requires an extra copy of the model parameters,
    // but results in more readable code
    double preyGrowth, preyCapacity, predationRate, predationHunger, predatorLoss, predatorGrowth;

    preyGrowth      = inputs.at(1) (0);
    preyCapacity    = inputs.at(1) (1);
    predationRate   = inputs.at(1) (2);
    predationHunger = inputs.at(1) (3);
    predatorLoss    = inputs.at(1) (4);
    predatorGrowth  = inputs.at(1) (5);

    if (inputDimWrt == 0) {
		
      // create a vector to hold the ModPiece output, this will hold the time derivatives of population
      Eigen::MatrixXd output(2, 2);

      // compute the growth rates as we did in example-1
      output(0,0) = -1.0 * preyPop * (preyGrowth / preyCapacity - (predPop * predationRate) / pow(preyPop + predationHunger,2.0)) - preyGrowth * (preyPop / preyCapacity - 1) - (predPop * predationRate) / (preyPop + predationHunger);
      output(0, 1) = -1.0 * (preyPop * predationRate) / (preyPop + predationHunger);

      output(1, 0) = (predPop * predatorGrowth * predationHunger) / pow(preyPop + predationHunger, 2.0);
      output(1, 1) = (preyPop * predatorGrowth) / (preyPop + predationHunger) - predatorLoss;

      // return the output
      return output;
	  
    } else if (inputDimWrt == 1) {
		
      Eigen::MatrixXd output(2, 6);

      output(0, 0) = -preyPop * (preyPop / preyCapacity - 1);
      output(0, 1) = (pow(preyPop, 2.0) * preyGrowth) / pow(preyCapacity, 2.0);
      output(0, 2) = -(predPop * preyPop) / (preyPop + predationHunger);
      output(0, 3) = (predPop * preyPop * predationRate) / pow(preyPop + predationHunger, 2.0);
      output(0, 4) = 0.0;
      output(0, 5) = 0.0;

      output(1, 0) = 0.0;
      output(1, 1) = 0.0;
      output(1, 2) = 0.0;
      output(1, 3) = -(predPop * preyPop * predatorGrowth) / pow(preyPop + predationHunger, 2.0);
      output(1, 4) = -predPop;
      output(1, 5) = (predPop * preyPop) / (preyPop + predationHunger);

      return output;
	  
    } else {
		
      assert(inputDimWrt < inputSizes.size());
	  
      return Eigen::MatrixXd();
    }
  }
};


/** By providing Jacobian information in the the PredPreyModel class, much for involved analysis of the system can
 *  be performed.  Below, the Jacobian is used within a simple Newton iteration to compute an equilibrium point of the
 *  ODE model.
 */
int main()
{
  // create an instance of the modpiece defining the predator prey model
  auto predatorPreyModel = make_shared<PredPreyModel>();

  // set the population sizes
  Eigen::VectorXd populations(2);

  populations << 50, 5; // prey population, predator population

  // set the interaction parameters
  Eigen::VectorXd growthParams(6);
  growthParams << 0.8, 100, 4.0, 15, 0.6, 0.8; //preyGrowth, preyCapacity, predationRate, predationHunger, predatorLoss,
                                               // predatorGrowth;

  // Evaluate the predator prey growth rate ModPiece as we did before, but now include the second input
  Eigen::MatrixXd jac0 = predatorPreyModel->Jacobian(populations, growthParams, 0);
  Eigen::MatrixXd jac1 = predatorPreyModel->Jacobian(populations, growthParams, 1);

  cout << "\nJacobian with respect to growth parameters:\n";
  cout << jac1 << endl;

  cout << "\nJacobian with respect to populations:\n";
  cout << jac0 << endl << endl;

  // Use a simple Newton iteration to find the steady state attractor
  Eigen::VectorXd resid = predatorPreyModel->Evaluate(populations, growthParams);
  while (resid.norm() > 1e-10) {
	  
	// first, take the Newton step
    populations -= predatorPreyModel->Jacobian(populations, growthParams, 0).lu().solve(resid);
	
	// Now, update the residual, i.e. the rhs of the ODE model.
    resid        = predatorPreyModel->Evaluate(populations, growthParams);
  }

  cout << "A population equilibrium exists at:\n";
  cout << "  (prey , predator) = (" << populations(0) << " , " << populations(1) << ")\n\n";
}

