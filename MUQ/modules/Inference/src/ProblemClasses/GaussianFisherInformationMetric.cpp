
#include <boost/property_tree/ptree.hpp> // needed to avoid weird toupper bug on osx

#include "MUQ/Inference/ProblemClasses/GaussianFisherInformationMetric.h"

#include <iostream>

using namespace std;
using namespace Eigen;
using namespace muq::Inference;
using namespace muq::Modelling;


GaussianFisherInformationMetric::GaussianFisherInformationMetric(std::shared_ptr<muq::Modelling::GaussianDensity>      density,
                                                                 std::shared_ptr<muq::Modelling::ModPiece> const&      model,
																 std::shared_ptr<muq::Modelling::GaussianDensity>      prior)
   : model(model), errorDensity(density), priorDensity(prior)
{
	assert(density);
	assert(model);
}

Eigen::MatrixXd GaussianFisherInformationMetric::ComputeMetric(Eigen::VectorXd const& point)
{
  MatrixXd jacobian = model->Jacobian(point);

  // std::cout << "applied " << appliedInverse<< std::endl;
  MatrixXd metric;
  metric = jacobian.transpose()*errorDensity->specification->ApplyInverseCovariance(jacobian);
  
  if(priorDensity!=nullptr)
  {
    metric += priorDensity->specification->GetPrecisionMatrix();
  }

  //   std::cout << "metric " << metric<< std::endl;


  //apply a mask if there is one
  if (mask) {
    return metric.array() * mask->array();
  } else {
    return metric;
  }
}

void GaussianFisherInformationMetric::SetMask(Eigen::MatrixXd const& mask)
{
  this->mask = boost::optional<Eigen::MatrixXd>(mask);
}

