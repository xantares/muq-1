
#include "MUQ/Inference/ProblemClasses/DensityProblem.h"

using namespace muq::Modelling;
using namespace muq::Inference;
using namespace std;


/** Evaluate the log density at the point xc */
double DensityProblem::LogEval(const Eigen::VectorXd& xc)
{
  return Dens.LogEval(xc);
}

/** Evaluate the gradient of the log density at xc */
double DensityProblem::GradLogEval(const Eigen::VectorXd& xc, Eigen::VectorXd& grad)
{
  return Dens.GradLogEval(xc, grad);
}

int DensityProblem::GetDim()
{
  return Dens.GetInDim();
}

