#include "MUQ/Inference/python/BasisFactoryPython.h"

#include "MUQ/Inference/TransportMaps/PolynomialBasis.h"

#include "MUQ/Utilities/multiIndex/MultiIndexFactory.h"

using namespace std;
using namespace muq::Utilities;
using namespace muq::Inference;

template<typename PolyType>
shared_ptr<BasisSet> SingleTermPtr(int dim, int nonzeroDim, int order)
{
      assert(nonzeroDim < dim);

  Eigen::RowVectorXu multi = Eigen::RowVectorXu::Zero(dim);
      multi(nonzeroDim) = order;

  auto output = make_shared<BasisSet>();

  *output += make_shared<PolyType>(multi);

  return output;
}

shared_ptr<BasisSet> PyBasisFactory::PySingleTermHermite(int totalDim, int nonzeroDim, int order)
{
  return SingleTermPtr<HermitePolynomialBasis>(totalDim, nonzeroDim, order);
}

shared_ptr<BasisSet> PyBasisFactory::PySingleTermLegendre(int totalDim, int nonzeroDim, int order)
{
  return SingleTermPtr<LegendrePolynomialBasis>(totalDim, nonzeroDim, order);
}

shared_ptr<BasisSet> PyBasisFactory::PySingleTermMonomial(int totalDim, int nonzeroDim, int order)
{
  return SingleTermPtr<MonomialPolynomialBasis>(totalDim, nonzeroDim, order);
}

boost::python::list PyBasisFactory::PyFullLinear(int dim)
{
  boost::python::list output;

  for (unsigned int i = 0; i < dim; ++i) {
    auto basis = make_shared<BasisSet>();
    // add the constant
    *basis += make_shared<ConstantBasis>(i + 1);
    for (int d = 0; d <= i; ++d) {
      *basis += make_shared<LinearBasis>(i + 1, d);
    }

    output.append(basis);
  }

  return output;
}

template<typename PolyType>
boost::python::list PyTotalOrder(int dim, int order)
{
  boost::python::list output;

  for (int i = 0; i < dim; ++i) {
    auto basis = make_shared<BasisSet>();

    auto temp      = MultiIndexFactory::CreateTotalOrder(i + 1, order);
    auto allMultis = temp->GetAllMultiIndices();
    for (int j = 0; j < allMultis.rows(); ++j) {
      *basis += make_shared<PolyType>(allMultis.row(j));
    }

    output.append(basis);
  }

  return output;
}

boost::python::list PyBasisFactory::PyTotalOrderHermite(int dim, int order)
{
  return PyTotalOrder<HermitePolynomialBasis>(dim, order);
}

boost::python::list PyBasisFactory::PyTotalOrderLegendre(int dim, int order)
{
  return PyTotalOrder<LegendrePolynomialBasis>(dim, order);
}

boost::python::list PyBasisFactory::PyTotalOrderMonomial(int dim, int order)
{
  return PyTotalOrder<MonomialPolynomialBasis>(dim, order);
}

template<typename PolyType>
boost::python::list PyNoCross(int dim, int order)
{
  boost::python::list output;

  for (int i = 0; i < dim; ++i) {
    auto basis = make_shared<BasisSet>();

    Eigen::RowVectorXu multi = Eigen::RowVectorXu::Zero(i + 1);
    *basis += make_shared<HermitePolynomialBasis>(multi);
    for (int j = 1; j < order + 1; ++j) {
      multi(i) = j;
      *basis  += make_shared<PolyType>(multi);
    }
    output.append(basis);
  }

  return output;
}

boost::python::list PyBasisFactory::PyNoCrossHermite(int dim, int order)
{
  return PyNoCross<HermitePolynomialBasis>(dim, order);
}

boost::python::list PyBasisFactory::PyNoCrossLegendre(int dim, int order)
{
  return PyNoCross<LegendrePolynomialBasis>(dim, order);
}

boost::python::list PyBasisFactory::PyNoCrossMonomial(int dim, int order)
{
  return PyNoCross<MonomialPolynomialBasis>(dim, order);
}

void muq::Inference::ExportBasisSet()
{
  boost::python::class_<BasisSet, shared_ptr<BasisSet>, boost::noncopyable> exportBasisSet("BasisSet",
                                                                                           boost::python::init<>());

  exportBasisSet.def(boost::python::self += shared_ptr<TransportMapBasis>());
  exportBasisSet.def(boost::python::self += BasisSet());

  exportBasisSet.def("size", &BasisSet::size);
  exportBasisSet.def("at", &BasisSet::PyAt);
}

void muq::Inference::ExportPyBasisFactory()
{
  boost::python::class_<PyBasisFactory, shared_ptr<PyBasisFactory>, boost::noncopyable> exportPyBasisFac(
    "BasisFactory", boost::python::no_init);

  exportPyBasisFac.def("SingleTermHermite", &PyBasisFactory::PySingleTermHermite).staticmethod("SingleTermHermite");
  exportPyBasisFac.def("SingleTermLegendre", &PyBasisFactory::PySingleTermLegendre).staticmethod("SingleTermLegendre");
  exportPyBasisFac.def("SingleTermMonomial", &PyBasisFactory::PySingleTermMonomial).staticmethod("SingleTermMonomial");

  exportPyBasisFac.def("FullLinear", &PyBasisFactory::PyFullLinear).staticmethod("FullLinear");

  exportPyBasisFac.def("TotalOrderHermite", &PyBasisFactory::PyTotalOrderHermite).staticmethod("TotalOrderHermite");
  exportPyBasisFac.def("TotalOrderLegendre", &PyBasisFactory::PyTotalOrderLegendre).staticmethod("TotalOrderLegendre");
  exportPyBasisFac.def("TotalOrderMonomial", &PyBasisFactory::PyTotalOrderMonomial).staticmethod("TotalOrderMonomial");

  exportPyBasisFac.def("NoCrossHermite", &PyBasisFactory::PyNoCrossHermite).staticmethod("NoCrossHermite");
  exportPyBasisFac.def("NoCrossLegendre", &PyBasisFactory::PyNoCrossLegendre).staticmethod("NoCrossLegendre");
  exportPyBasisFac.def("NoCrossMonomial", &PyBasisFactory::PyNoCrossMonomial).staticmethod("NoCrossMonomial");
}

