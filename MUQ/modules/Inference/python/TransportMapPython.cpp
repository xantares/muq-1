#include "MUQ/Inference/TransportMaps/TransportMap.h"
#include "MUQ/Inference/TransportMaps/MapFactory.h"

#include "MUQ/Inference/TransportMaps/MapUpdateManager.h"

using namespace std;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Inference;

shared_ptr<TransportMap> TransportMap::PyBuildSampsToStdSerial0(boost::python::list const& pySamps,
                                                                boost::python::list const& pyBases,
                                                                boost::python::list        pyOptLevelsIn,
                                                                boost::python::list        pyCoeffGuess)
{
  Eigen::MatrixXd samps = GetEigenMatrix(pySamps);

  vector<BasisSet> bases = GetBasisSetVec(pyBases);

  const int dim = boost::python::len(pyBases);

  vector<Eigen::VectorXd> x0(dim);

  if (boost::python::len(pyCoeffGuess) > 0) {
    x0 = PythonListToVector(pyCoeffGuess);
  } else {
    for (unsigned int d = 0; d < dim; ++d) {
      x0[d] = GetIdentityCoeff(bases[d], d);
    }
  }

  auto map = make_shared<TransportMap>(bases, x0);

  MapUpdateManager manager(map, samps.cols());

  manager.UpdateMap(samps);

  return map;
}

shared_ptr<TransportMap> TransportMap::PyBuildSampsToStdSerial1(boost::python::list const& pySamps,
                                                                boost::python::list const& pyBases,
                                                                boost::python::list        pyOptLevelsIn)
{
  return PyBuildSampsToStdSerial0(pySamps, pyBases, pyOptLevelsIn, boost::python::list());
}

shared_ptr<TransportMap> TransportMap::PyBuildSampsToStdSerial2(boost::python::list const& pySamps,
                                                                boost::python::list const& pyBases)
{
  return PyBuildSampsToStdSerial0(pySamps, pyBases, boost::python::list(), boost::python::list());
}

shared_ptr<TransportMap> TransportMap::PyCreateIdentity(boost::python::list const& pyBases)
{
  vector<BasisSet> bases = GetBasisSetVec(pyBases);

  vector<Eigen::VectorXd> coeffs(bases.size());
  for (int i = 0; i < bases.size(); ++i) {
    coeffs.at(i) = GetIdentityCoeff(bases.at(i), i);
  }

  return make_shared<TransportMap>(bases, coeffs);
}

vector<BasisSet> TransportMap::GetBasisSetVec(boost::python::list const& pyBasis)
{
  const int num = boost::python::len(pyBasis);

  vector<BasisSet> basis(num);
  for (unsigned int i = 0; i < num; ++i) {
    shared_ptr<BasisSet> b = boost::python::extract<shared_ptr<BasisSet> >(pyBasis[i]);
    basis[i] = *b;
  }

  return basis;
}

boost::python::list TransportMap::PyJacobian(boost::python::list const& inputs)
{
  return GetPythonMatrix(Jacobian(GetEigenVector<Eigen::VectorXd>(inputs)));
}

boost::python::list TransportMap::PySplit(int splitDim) const
{
  pair<shared_ptr<TransportMap>, shared_ptr<TransportMap> > split = Split(splitDim);

  boost::python::list splitList;

  splitList.append(split.first);
  splitList.append(split.second);

  return splitList;
}

boost::python::list TransportMap::PyEvaluateInverse(boost::python::list const& input,
                                                    boost::python::list const& x0) const
{
  return GetPythonVector<Eigen::VectorXd>(EvaluateInverse(GetEigenVector<Eigen::VectorXd>(input),
                                                          GetEigenVector<Eigen::VectorXd>(x0)));
}

void muq::Inference::ExportTransportMap()
{
  boost::python::class_<TransportMap, std::shared_ptr<TransportMap>,
                        boost::python::bases<ModPiece, OneInputJacobianModPiece>,
                        boost::noncopyable>
  exportTransMap("TransportMap", boost::python::no_init);

  exportTransMap.def("__init__", boost::python::make_constructor(&TransportMap::PyBuildSampsToStdSerial0));
  exportTransMap.def("__init__", boost::python::make_constructor(&TransportMap::PyBuildSampsToStdSerial1));
  exportTransMap.def("__init__", boost::python::make_constructor(&TransportMap::PyBuildSampsToStdSerial2));

  exportTransMap.def("__init__", boost::python::make_constructor(&TransportMap::PyCreateIdentity));

  exportTransMap.def("Jacobian", &TransportMap::PyJacobian);
  exportTransMap.def("Split", &TransportMap::PySplit);
  exportTransMap.def("EvaluateInverse", &TransportMap::PyEvaluateInverse);

  boost::python::implicitly_convertible<shared_ptr<TransportMap>, shared_ptr<ModPiece> >();
  boost::python::implicitly_convertible<shared_ptr<TransportMap>,
                                        shared_ptr<OneInputJacobianModPiece> >();
}

