#include "MUQ/Approximation/Incremental/Approximator.h"

namespace py = boost::python;
using namespace muq::Approximation;
using namespace muq::Utilities;


py::list Approximator::PyGetCachePoints() const
{
  return GetPythonMatrix<double>(GetCachePoints().transpose());
}

void Approximator::PyAddToCache(py::list const& pts)
{
  // convert to an Eigen::Matrix
  const Eigen::MatrixXd ptsEigen = GetEigenMatrix(pts).transpose();

  // add the points to the cache 
  AddToCache(ptsEigen);
}

py::list Approximator::PyRefineNear(py::list const& point)
{
  // convert python list to Eigen::Vector
  Eigen::VectorXd eigenPoint = GetEigenVector<Eigen::VectorXd>(point);

  auto refineResult = RefineNear(eigenPoint);
  
  if(refineResult){
	  //if the result is present, unwrap the boost::optional
  return GetPythonVector<Eigen::VectorXd>(*refineResult);
  }
  else{
	  //can't return an optional, so return an empty list instead
  return GetPythonVector<Eigen::VectorXd>(Eigen::VectorXd());
  }
}
