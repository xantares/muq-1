
#include <iostream>

// include the google testing header
#include "gtest/gtest.h"

// include the Transleter header
#include "MUQ/Utilities/VectorTranslater.h"

using namespace muq::Utilities;
using namespace std;

// test the translation of a std::vector<double> to an Eigen::VectorXd
TEST(UtilitiesTranslater, StdToEigen)
{
  // create an stl vector filled with 0.1
  vector<double> vec1(10, 0.1);

  // create a pointer to an Eigen::VectorXd dynamic vector
  Eigen::VectorXd *vec2ptr;

  // create the translator
  Translater<vector<double>, Eigen::VectorXd> one2two(vec1);
  vec2ptr = one2two.GetPtr();

  for (int i = 0; i < 10; ++i) {
    EXPECT_EQ(vec1[i], (*vec2ptr)[i]);
  }
}


// test the translation of a std::vector<double> to an Eigen::VectorXd
TEST(UtilitiesTranslater, StdToEigenUpdate)
{
  // create an stl vector filled with 0.1
  vector<double> vec1(10, 0.1);

  // create a pointer to an Eigen::VectorXd dynamic vector
  Eigen::VectorXd *vec2ptr;

  // create a scope for the translater, so the Destructor will be called
  {
    // create the translator
    Translater<vector<double>, Eigen::VectorXd> one2two(vec1, true);

    vec2ptr = one2two.GetPtr();

    // add one to all of
    *vec2ptr += Eigen::VectorXd::Ones(10);
  }


  for (int i = 0; i < 10; ++i) {
    EXPECT_EQ(1.1, vec1[i]);
  }
}


// test the translation of an Eigen::VectorXd to a double pointer.
TEST(UtilitiesTranslater, EigenToDouble)
{
  // create an vector filled with 0.1
  Eigen::VectorXd vec1 = 0.1 * Eigen::VectorXd::Ones(10);

  // create a double pointer
  double *vec2ptr;

  // create the translator
  Translater<Eigen::VectorXd, double *> one2two(vec1);

  vec2ptr = *one2two.GetPtr();

  for (int i = 0; i < 10; ++i) {
    EXPECT_EQ(vec1[i], vec2ptr[i]);
  }
}

// test the translation of an Eigen::VectorXd to a double pointer.
TEST(UtilitiesTranslater, EigenToDoubleUpdate)
{
  // create an vector filled with 0.1
  Eigen::VectorXd vec1 = 0.1 * Eigen::VectorXd::Ones(10);

  // create a pointer to an Eigen::VectorXd dynamic vector
  double *vec2ptr;

  // create a scope for the translater, so the Destructor will be called
  {
    // create the translator
    Translater<Eigen::VectorXd, double *> one2two(vec1, true);

    vec2ptr = *one2two.GetPtr();

    // add one to all of the entries in the vector
    for (int i = 0; i < 10; ++i) {
      vec2ptr[i] += 1.0;
    }
  }


  for (int i = 0; i < 10; ++i) {
    EXPECT_EQ(1.1, vec1[i]);
  }
}


// test the translation of a double pointer to an Eigen vector.
TEST(UtilitiesTranslater, DoubleToEigen)
{
  // create an vector filled with 0.1
  double *vec1 = new double[10];

  for (int i = 0; i < 10; ++i) {
    vec1[i] = 0.1;
  }

  // create a pointer to an Eigen::VectorXd
  Eigen::VectorXd *vec2ptr;

  // create the translator
  Translater<double *, Eigen::VectorXd> one2two(vec1, 10);

  vec2ptr = one2two.GetPtr();

  for (int i = 0; i < 10; ++i) {
    EXPECT_EQ(vec1[i], (*vec2ptr)[i]);
  }

  delete[] vec1;
}


// test the translation of a double pointer to an Eigen vector with an update.
TEST(UtilitiesTranslater, DoubleToEigenUpdate)
{
  // create an vector filled with 0.1
  double *vec1 = new double[10];

  for (int i = 0; i < 10; ++i) {
    vec1[i] = 0.1;
  }

  // create a pointer to an Eigen::VectorXd
  Eigen::VectorXd *vec2ptr;

  {
    // create the translator
    Translater<double *, Eigen::VectorXd> one2two(vec1, 10, true);

    vec2ptr = one2two.GetPtr();

    *vec2ptr += Eigen::VectorXd::Ones(10);
  }


  for (int i = 0; i < 10; ++i) {
    EXPECT_EQ(1.1, vec1[i]);
  }

  delete[] vec1;
}

