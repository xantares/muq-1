
#include "gtest/gtest.h"

#include "MUQ/Utilities/mesh/Mesh.h"
#include "MUQ/Utilities/mesh/StructuredQuadMesh.h"
#include "MUQ/Utilities/mesh/UnstructuredSimplicialMesh.h"
#include "MUQ/Utilities/EigenTestUtils.h"


#include "MUQ/Utilities/Hmatrix/Hmatrix.h"
#include "MUQ/Utilities/LanczosEigenSolver.h"
#include "MUQ/Geostats/IsotropicCovKernel.h"
#include "MUQ/Geostats/PowerKernel.h"

#include <sys/time.h>
#include <algorithm>
#include <vector>
#include <Eigen/Eigenvalues>

using namespace muq::Utilities;
using namespace muq::geostats;


TEST(UtilitiesMesh, StructuredQuadMesh1d)
{
  // first define a fixed size matrix holding the lower and upper bounds
  Eigen::Matrix<double, 1, 1> lb(1, 1);

  lb[0] = 0;

  Eigen::Matrix<double, 1, 1> ub(1, 1);
  ub[0] = 1;

  Eigen::Matrix<unsigned int, 1, 1> N(1);
  N[0] = pow(2, 9);

  // create the mesh and store it in a smart pointer
  std::shared_ptr < Mesh < 1 >> MeshPtr(new StructuredQuadMesh<1>(lb, ub, N));
}

TEST(UtilitiesMesh, StructuredQuadMesh2d)
{
  // first define a fixed size matrix holding the lower and upper bounds
  Eigen::Matrix<double, 2, 1> lb(2, 1);

  lb[0] = 0;
  lb[1] = 0;

  Eigen::Matrix<double, 2, 1> ub(2, 1);
  ub[0] = 1;
  ub[1] = 1;

  Eigen::Matrix<unsigned int, 2, 1> N(2, 1);
  N[0] = pow(2, 4);
  N[1] = pow(2, 4);

  // create the mesh and store it in a smart pointer
  std::shared_ptr < Mesh < 2 >> MeshPtr(new StructuredQuadMesh<2>(lb, ub, N));
}

TEST(UtilitiesMesh, StructuredQuadMesh1d_Hmat)
{
  // first define a fixed size matrix holding the lower and upper bounds
  Eigen::Matrix<double, 1, 1> lb(1, 1);

  lb[0] = 0;

  Eigen::Matrix<double, 1, 1> ub(1, 1);
  ub[0] = 1;

  Eigen::Matrix<unsigned int, 1, 1> N(1);
  N[0] = pow(2, 9);

  // create the mesh and store it in a smart pointer
  std::shared_ptr < Mesh < 1 >> MeshPtr(new StructuredQuadMesh<1>(lb, ub, N));

  // now create the covariance kernel
  CovKernelPtr Kernel(new PowerKernel(0.1, 1.0, 1.0));

  // construct the hierarchical matrix
  HMatrix<1> Hmat(Kernel, MeshPtr);


  Eigen::VectorXd x      = Eigen::VectorXd::Ones(N[0]);
  Eigen::VectorXd b_hmat = Hmat * x;

  Eigen::MatrixXd FullCov(N[0], N[0]);
  FullCov = Kernel->BuildCov(MeshPtr);
  Eigen::VectorXd b_full = FullCov * x;


  for (int i = 0; i < b_full.size(); ++i) {
    EXPECT_NEAR(b_full[i], b_hmat[i], 0.01 * b_full[i]);
  }
}

TEST(UtilitiesMesh, StructuredQuadMesh2d_Hmat)
{
  // first define a fixed size matrix holding the lower and upper bounds
  Eigen::Matrix<double, 2, 1> lb(2, 1);

  lb[0] = 0;
  lb[1] = 0;

  Eigen::Matrix<double, 2, 1> ub(2, 1);
  ub[0] = 3;
  ub[1] = 3;

  Eigen::Matrix<unsigned int, 2, 1> N(2, 1);
  N[0] = 64; //pow(2,6);
  N[1] = 64; //pow(2,6);

  // create the mesh and store it in a smart pointer
  std::shared_ptr < Mesh < 2 >> MeshPtr(new StructuredQuadMesh<2>(lb, ub, N));

  // now create the covariance kernel
  CovKernelPtr Kernel(new PowerKernel(0.2, 1.0, 1.0));

  // construct the hierarchical matrix
  HMatrix<2> Hmat(Kernel, MeshPtr);

  Eigen::VectorXd x = Eigen::VectorXd::Ones(N[0] * N[1]);

  Eigen::VectorXd b_hmat = Hmat * x;

  Eigen::MatrixXd FullCov(N[0] * N[1], N[0] * N[1]);
  FullCov = Kernel->BuildCov(MeshPtr);

  Eigen::VectorXd b_full = FullCov * x;

  for (int i = 0; i < b_full.size(); ++i) {
    EXPECT_NEAR(b_full[i], b_hmat[i], 0.01 * b_full[i]);
  }
}


// test the lanczos solver on a general dense spd matrix
TEST(UtilitiesLanczos, BasicDenseTest)
{
  unsigned int dim  = 100;
  Eigen::MatrixXd A = Eigen::MatrixXd::Random(dim, dim);

  A = A * A.transpose();

  // use the lanczos solver to compute the eigenvalues and eigenvectors
  LanczosEigenSolver solver(dim);
  solver.compute(A);

  Eigen::VectorXd TestVals = solver.eigenvalues();
  Eigen::MatrixXd TestVecs = solver.eigenvectors();


  // use the Eigen built in dense eigenvalue solver for comparison
  Eigen::EigenSolver<Eigen::MatrixXd> truesolver;
  truesolver.compute(A);

  Eigen::VectorXd TrueVals = truesolver.eigenvalues().real();
  Eigen::MatrixXd TrueVecs = truesolver.eigenvectors().real();

  std::vector<unsigned int> idx(TrueVals.size());
  for (unsigned int i = 0; i != idx.size(); ++i) {
    idx[i] = i;
  }

  // sort the eigenvalues in increasing order
  std::sort(idx.begin(), idx.end(), [&TrueVals](unsigned int i1,
                                                unsigned int i2)                  { return TrueVals[i1] < TrueVals[i2];
            });

  Eigen::VectorXd OldVals = TrueVals;
  Eigen::MatrixXd OldVecs = TrueVecs;
  for (unsigned int i = 0; i < idx.size(); ++i) {
    TrueVals[i]     = OldVals[idx[i]];
    TrueVecs.col(i) = OldVecs.col(idx[i]);
  }


  // make sure everything matches up
  for (unsigned int i = 0; i < TestVals.size(); ++i) {
    EXPECT_NEAR(TrueVals[dim - i - 1], TestVals[i], 1e-10);
  }
}


TEST(UtilitiesLanczos, LargestEigsDenseTest)
{
  unsigned int dim  = 600;
  Eigen::MatrixXd A = Eigen::MatrixXd::Random(dim, dim);

  A = A * A.transpose();

  // use the lanczos solver to compute the eigenvalues and eigenvectors
  LanczosEigenSolver solver(5);
  solver.compute(A);

  Eigen::VectorXd TestVals = solver.eigenvalues();
  Eigen::MatrixXd TestVecs = solver.eigenvectors();


  // use the Eigen built in dense eigenvalue solver for comparison
  Eigen::EigenSolver<Eigen::MatrixXd> truesolver;
  truesolver.compute(A);

  Eigen::VectorXd TrueVals = truesolver.eigenvalues().real();
  Eigen::MatrixXd TrueVecs = truesolver.eigenvectors().real();

  // sort the eigenvalues in increasing order
  std::sort(TrueVals.data(), TrueVals.data() + TrueVals.size());

  // make sure everything matches up
  for (unsigned int i = 0; i < TestVals.size(); ++i) {
    EXPECT_NEAR(TrueVals[dim - i - 1], TestVals[i], 1e-5 * TrueVals[dim - i - 1]);
  }
}


TEST(UtilitiesMesh, StructuredQuadMesh2d_KL)
{
  // first define a fixed size matrix holding the lower and upper bounds
  Eigen::Matrix<double, 2, 1> lb(2, 1);

  lb[0] = 0;
  lb[1] = 0;

  Eigen::Matrix<double, 2, 1> ub(2, 1);
  ub[0] = 1;
  ub[1] = 1;

  Eigen::Matrix<unsigned int, 2, 1> N(2, 1);
  N[0] = 20; //pow(2,6);
  N[1] = 20; //pow(2,6);

  // create the mesh and store it in a smart pointer
  std::shared_ptr < Mesh < 2 >> MeshPtr(new StructuredQuadMesh<2>(lb, ub, N));

  // now create the covariance kernel
  CovKernelPtr Kernel(new PowerKernel(0.4, 2.0, 1.0));

  // construct the hierarchical matrix
  HMatrix<2> Hmat(Kernel, MeshPtr);

  Eigen::VectorXd x = Eigen::VectorXd::Ones(N[0] * N[1]);

  Eigen::VectorXd b_hmat = Hmat * x;

  LanczosEigenSolver solver(3);

  solver.compute(Hmat);

  //Eigen::VectorXd TestVals = solver.eigenvalues();
  Eigen::MatrixXd TestVecs = solver.eigenvectors();
}

TEST(UtilitiesMesh, UnstructuredSimplicial2d_Simple)
{
  // first define a fixed size matrix holding the lower and upper bounds
  Eigen::Matrix<double, 2, 1> lb(2, 1);

  lb[0] = 0;
  lb[1] = 0;

  Eigen::Matrix<double, 2, 1> ub(2, 1);
  ub[0] = 3;
  ub[1] = 3;

  Eigen::Matrix<unsigned int, 2, 1> N(2, 1);
  N[0] = 1; //pow(2,6);
  N[1] = 1; //pow(2,6);

  // create the mesh and store it in a smart pointer
  std::shared_ptr < Mesh < 2 >> MeshPtr(new UnstructuredSimplicialMesh<2>(lb, ub, N));

  Eigen::VectorXd vals = Eigen::VectorXd::Ones(MeshPtr->NumEles());

  MeshPtr->writeScalarVtk("results/tests/meshes/BasicConst.vtk", vals, "ConstantData");

  EXPECT_EQ(MeshPtr->NumEles(),  2);
  EXPECT_EQ(MeshPtr->NumNodes(), 4);
}


TEST(UtilitiesMesh, UnstructuredSimplicial2d)
{
  // create the mesh and store it in a smart pointer
  std::shared_ptr < Mesh < 2 >>
  MeshPtr(new UnstructuredSimplicialMesh<2>("data/tests/meshes/Superior.ele", "data/tests/meshes/Superior.node"));

  Eigen::VectorXd vals = Eigen::VectorXd::Ones(MeshPtr->NumEles());

  MeshPtr->writeScalarVtk("results/tests/meshes/SuperiorConst.vtk", vals, "TestData");
  EXPECT_EQ(MeshPtr->NumEles(),  4063);
  EXPECT_EQ(MeshPtr->NumNodes(), 2461);
}


TEST(UtilitiesMesh, UnstructuredSimplicial2d_KL)
{
  // create the mesh and store it in a smart pointer
  std::shared_ptr < Mesh < 2 >>
  MeshPtr(new UnstructuredSimplicialMesh<2>("data/tests/meshes/Superior.ele", "data/tests/meshes/Superior.node"));

  // make sure the mesh was read correctly
  EXPECT_EQ(MeshPtr->NumEles(),  4063);
  EXPECT_EQ(MeshPtr->NumNodes(), 2461);

  // create a covariance kernel
  CovKernelPtr Kernel(new PowerKernel(1, 2.0, 2.0));

  // compute the KL modes on the Lake Superior Mesh
  Eigen::MatrixXd Bases = Kernel->BuildKL(MeshPtr, 10);

  // write the first few nodes to vtk formatted files
  MeshPtr->writeScalarVtk("results/tests/meshes/SuperiorKLmodes.vtk", Bases, "KLModes");
}


TEST(UtilitiesMesh, Pos2EleTest1d)
{
  // first define a fixed size matrix holding the lower and upper bounds
  Eigen::Matrix<double, 1, 1> lb(1, 1);

  lb[0] = 0;

  Eigen::Matrix<double, 1, 1> ub(1, 1);
  ub[0] = 1;

  Eigen::Matrix<unsigned int, 1, 1> N(1);
  N[0] = 10;

  // create the mesh and store it in a smart pointer
  std::shared_ptr < Mesh < 1 >> MeshPtr(new StructuredQuadMesh<1>(lb, ub, N));

  // test to make sure we can find the correct element
  Eigen::Matrix<double, 1, 1> TestPos(1, 1);
  TestPos(0, 0) = 0.15;
  unsigned int element = MeshPtr->GetEle(TestPos);
  EXPECT_EQ(1, element);

  // test to make sure an exception is thrown
  TestPos(0, 0) = 1.15;
  bool has_failed = true;
  try {
    element = MeshPtr->GetEle(TestPos);
    EXPECT_EQ(has_failed, false);
  } catch (...) { //FindElementFailure fail){
    EXPECT_EQ(has_failed, true);
  }
}


TEST(UtilitiesMesh, Pos2EleTest2d_quad)
{
  // first define a fixed size matrix holding the lower and upper bounds
  Eigen::Matrix<double, 2, 1> lb(2, 1);

  lb << 0, 0;

  Eigen::Matrix<double, 2, 1> ub(2, 1);
  ub << 1, 1;

  Eigen::Matrix<unsigned int, 2, 1> N(2);
  N << 10, 10;

  // create the mesh and store it in a smart pointer
  std::shared_ptr < Mesh < 2 >> MeshPtr(new StructuredQuadMesh<2>(lb, ub, N));

  // test to make sure we can find the correct element
  Eigen::Matrix<double, 2, 1> TestPos(2, 1);
  TestPos << 0.15, 0.15;

  unsigned int element = MeshPtr->GetEle(TestPos);
    EXPECT_EQ(11, element);

  // test to make sure an exception is thrown
  TestPos << 1.15, 0.1;
  bool has_failed = true;
  try {
    element = MeshPtr->GetEle(TestPos);
    EXPECT_EQ(has_failed, false);
  } catch (...) { //FindElementFailure fail){
    EXPECT_EQ(has_failed, true);
  }
}


TEST(UtilitiesMesh, Pos2EleTest2d_simplicial)
{
  // create the mesh and store it in a smart pointer
  std::shared_ptr < Mesh < 2 >>
  MeshPtr(new UnstructuredSimplicialMesh<2>("data/tests/meshes/Superior.ele", "data/tests/meshes/Superior.node"));

  // test to make sure we can find the correct element
  Eigen::Matrix<double, 2, 1> TestPos = MeshPtr->GetElePos(100);

  unsigned int element = MeshPtr->GetEle(TestPos);

  EXPECT_EQ(100, element);
}


TEST(UtilitiesMesh, Pos2EleTest3d_quad)
{
  // first define a fixed size matrix holding the lower and upper bounds
  Eigen::Matrix<double, 3, 1> lb(3, 1);

  lb << 0, 0, 0;

  Eigen::Matrix<double, 3, 1> ub(3, 1);
  ub << 1, 1, 1;

  Eigen::Matrix<unsigned int, 3, 1> N(3);
  N << 10, 10, 10;

  // create the mesh and store it in a smart pointer
  std::shared_ptr < Mesh < 3 >> MeshPtr(new StructuredQuadMesh<3>(lb, ub, N));

  // test to make sure we can find the correct element
  Eigen::Matrix<double, 3, 1> TestPos(3, 1);
  TestPos << 0.15, 0.15, 0.15;
  unsigned int element = MeshPtr->GetEle(TestPos);
    EXPECT_EQ(111, element);

  // test to make sure an exception is thrown
  TestPos << 1.15, 0.1, 10.0;
  bool has_failed = true;
  try {
    element = MeshPtr->GetEle(TestPos);
    EXPECT_EQ(has_failed, false);
  } catch (...) { //FindElementFailure fail){
    EXPECT_EQ(has_failed, true);
  }
}

TEST(UtilitiesMesh, Pos2EleTest3d_simplicial)
{
  // first define a fixed size matrix holding the lower and upper bounds
  Eigen::Matrix<double, 3, 1> lb(3, 1);

  lb << 0, 0, 0;

  Eigen::Matrix<double, 3, 1> ub(3, 1);
  ub << 1, 1, 1;

  Eigen::Matrix<unsigned int, 3, 1> N(3);
  N << 10, 10, 10;

  // create the mesh and store it in a smart pointer
  std::shared_ptr < Mesh < 3 >> MeshPtr(new UnstructuredSimplicialMesh<3>(lb, ub, N));

  // test to make sure we can find the correct element
  Eigen::Matrix<double, 3, 1> TestPos(3, 1);
  TestPos << 0.15, 0.15, 0.125;
  unsigned int element = MeshPtr->GetEle(TestPos);
    EXPECT_EQ(668, element);

  // test to make sure an exception is thrown
  TestPos << 1.15, 0.1, 10.0;
  bool has_failed = false;
  try {
    element = MeshPtr->GetEle(TestPos);
    EXPECT_EQ(true, has_failed);
  } catch (...) { //FindElementFailure fail){
    has_failed = true;
    EXPECT_EQ(true, has_failed);
  }
}
