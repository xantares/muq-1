
#include "MUQ/Utilities/JobManager.h"

#ifdef MUQ_MPI
# include <boost/mpi/communicator.hpp>
# include <boost/mpi/status.hpp>
# include <boost/mpi/request.hpp>
# include <boost/mpi/nonblocking.hpp>
# include <boost/serialization/utility.hpp>
# include <boost/property_tree/ptree_serialization.hpp>
namespace mpi = boost::mpi;
#endif //MUQ_MPI

#include "MUQ/Utilities/LogConfig.h"
#include "MUQ/Utilities/HDF5Wrapper.h"

using namespace std;
using namespace boost::property_tree;
using namespace muq::Utilities;


#ifdef MUQ_MPI

struct ParallelTags {
  static const int requestWork   = 0;
  static const int issueWork     = 1;
  static const int returnResults = 2;
  static const int doneWithBatch = 3;
  static const int quitNow       = 4;
};


void SetupWorkers()
{
  unique_ptr<mpi::communicator> worldComm(new mpi::communicator);
      LOG(INFO) << "Initializing workers." << worldComm->rank();
  //don't want the base one to become a worker
  if (worldComm->rank() == 0) {
      LOG(INFO) << worldComm->rank() << "Primary process exiting.";
    return;
  }

      LOG(INFO) << worldComm->rank() << "Trapping workers.";
      

  boost::optional<mpi::status> probeReturn;

  //loop until you get a quit message
  while (!(probeReturn = worldComm->iprobe(0, ParallelTags::quitNow))) {
    //ask for new work
      LOG(INFO) << worldComm->rank() << "Asking for work.";
    worldComm->send(0, ParallelTags::requestWork, 0);

      LOG(INFO) << worldComm->rank() << "Done.";
    std::pair<string, ptree> data; //create someplace for the requested vector to go


      LOG(INFO) << worldComm->rank() << "Waiting";
    worldComm->recv(0, ParallelTags::issueWork, data);
    
    //check that it's not an empty job
    if (data.first.length() > 0) {
      LOG(INFO) << worldComm->rank() << "Running job named " << data.first;
      //Since the request was to do work, let's begin
      (JobManager::GetJobMap()->at(data.first).first)(data.second);

      LOG(INFO) << worldComm->rank() << "Done.";
    } else {
      LOG(INFO) << "Got empty job";
    }
  }
  
  // actually read the quit flag.  This removes the message from the buffer and allows jobManager to be run more than once in a single application
  int msg;
  worldComm->recv(0, ParallelTags::quitNow, msg);

      LOG(INFO) << worldComm->rank()  << "Finished being a worker.";
}

void CloseWorkers()
{
  unique_ptr<mpi::communicator> worldComm(new mpi::communicator);


  ptree  empty;
  string emptyStr    = "";
  int    trashBuffer = 0;


  for (int i = 1; i < worldComm->size(); ++i) {
      LOG(INFO) << worldComm->rank() << "Telling worker " << i << " to quit.";

    //make sure it loops and resends the work request in case the recieve was cancelled
    worldComm->send(i, ParallelTags::issueWork, std::make_pair(emptyStr, empty));
    //wait for it to request work
    worldComm->recv(i, ParallelTags::requestWork, trashBuffer);
    //queue up the quit message
    worldComm->send(i, ParallelTags::quitNow, 0);
    //and force it loop, which involves looking for a quit
    worldComm->send(i, ParallelTags::issueWork, make_pair(emptyStr, empty));
    LOG(INFO) << worldComm->rank() << "Quit received.";
  }
}

#endif //MUQ_MPI

void JobManager::RunJobs(boost::property_tree::ptree const& jobList)
{
#ifdef MUQ_MPI //parallel version
  unique_ptr<mpi::communicator> worldComm(new mpi::communicator);

      LOG(INFO) << "Running jobs in parallel, rank = " << worldComm->rank();
  SetupWorkers();


  //then actually assign jobs
  if (worldComm->rank() == 0) {
    typedef vector<mpi::request> RequestVector;
    //create a vector for all the remote processes
    RequestVector readyForWorkRequests(worldComm->size() - 1);

    int trashBuffer = 0;


    //create a request for workers that want work to do
    for (int i = 0; i < worldComm->size() - 1; ++i) {
      readyForWorkRequests.at(i) = worldComm->irecv(i + 1, ParallelTags::requestWork, trashBuffer);
    }

            LOG(INFO) << worldComm->rank() << "Work request polls created.";


    for (auto subtree : jobList) {
      if (subtree.first == "Job") {
        ptree dataTree = subtree.second.get_child("Data");

        for (int i = 0; i < subtree.second.get("NumberOfCopies", 1); ++i) {
          dataTree.put("CopyNumber", i);
          string fnName = subtree.second.get("Function", "");
          if (GetJobMap()->find(fnName) == GetJobMap()->end()) {
            LOG(INFO) << "Cannot find job function: " << fnName;
            cout << "Cannot find job function: " << fnName << endl;
            assert(false);
          }
          string datasetName = "";
          if (JobManager::GetJobMap()->at(fnName).second) {
            datasetName = (JobManager::GetJobMap()->at(fnName).second)(dataTree);
          }
          if (!JobManager::GetJobMap()->at(fnName).second || !HDF5Wrapper::DoesDataSetExist(datasetName)) {
            LOG(INFO) << "Running function " << fnName << " copy " << i << " to make " << datasetName;

            LOG(INFO) << worldComm->rank() << "Finding a worker.";

            //We need to find a worker to send it to.
            //Wait for one of the workers to be done
            std::pair<mpi::status, RequestVector::iterator> firstResponse = mpi::wait_any(
              readyForWorkRequests.begin(), readyForWorkRequests.end());

            //Make a copy into a colvec - helps resolve through all the templates
            pair<string, ptree> workToIssue = make_pair(fnName, dataTree);

            LOG(INFO) << worldComm->rank() << "Found worker " << firstResponse.first.source();

            //reply with this vector of work to do, don't need to wait for
            worldComm->isend(firstResponse.first.source(), ParallelTags::issueWork, workToIssue);

            //Also set up another request for when this worker is ready for more work
            readyForWorkRequests.at(firstResponse.first.source() - 1) = worldComm->irecv(
              firstResponse.first.source(), ParallelTags::requestWork, trashBuffer);
            LOG(INFO) << worldComm->rank() << "Work sent " << firstResponse.first.source();
            
          } else {
            LOG(INFO) << "Skipping dataset " << datasetName;
          }
        }
      }
    }

    //cancel all the waiting requests
    for (auto it : readyForWorkRequests) {
      it.cancel();
    }


    CloseWorkers();
  }


#else //serial version

          LOG(INFO) << "Running jobs in serial";


  for (auto subtree : jobList) {
    if (subtree.first == "Job") {
      ptree dataTree = subtree.second.get_child("Data");

      for (int i = 0; i < subtree.second.get("NumberOfCopies", 1); ++i) {
        dataTree.put("CopyNumber", i);
        string fnName = subtree.second.get("Function", "");
        if (GetJobMap()->find(fnName) == GetJobMap()->end()) {
          LOG(INFO) << "Cannot find job function: " << fnName;
          cout << "Cannot find job function: " << fnName << endl;
          assert(false);
        }
        string datasetName = "";
        if (JobManager::GetJobMap()->at(fnName).second) {
          datasetName = (JobManager::GetJobMap()->at(fnName).second)(dataTree);
        }
        if (!JobManager::GetJobMap()->at(fnName).second || !HDF5Wrapper::DoesDataSetExist(datasetName)) {
          LOG(INFO) << "Running function " << fnName << " copy " << i << " to make " << datasetName;
          (JobManager::GetJobMap()->at(fnName).first)(dataTree);
        } else {
          LOG(INFO) << "Skipping dataset " << datasetName;
        }
      }
    }
  }


#endif // ifdef MUQ_MPI
          LOG(INFO) << "Done with all jobs";
}

std::shared_ptr<JobManager::AvailableJobMap> JobManager::GetJobMap()
{
  static std::shared_ptr<JobManager::AvailableJobMap> map;

  if (!map) {
    map = make_shared<JobManager::AvailableJobMap>();
  }
  return map;
}
