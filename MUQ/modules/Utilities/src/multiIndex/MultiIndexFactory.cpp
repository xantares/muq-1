#include "MUQ/Utilities/multiIndex/MultiIndexFactory.h"


using namespace std;
using namespace muq::Utilities;

void muq::Utilities::MultiIndexFactory::RecursiveTotalOrderFill(unsigned int const maxOrder,
                                                                unsigned int const minOrder,
                                                                shared_ptr<MultiIndexSet> output,
                                                                unsigned int const currDim,
                                                                Eigen::RowVectorXu &base,
                                                                std::shared_ptr<MultiIndexLimiter> limiter)
{
  int currOrder = base.head(currDim+1).sum();
  const int length = base.size();
  
  if(currDim==length-1)
  {
    for(int i=max<int>(0,minOrder-currOrder); i<=maxOrder-currOrder; ++i)
    {
      base(length-1) = i;
      auto newTerm = make_shared<MultiIndex>(base);
      if(limiter->IsFeasible(newTerm))
        output->AddActive(newTerm);
    }
  }else{
    for(int i=0; i<=maxOrder-currOrder; ++i)
    {
      base.tail(length-currDim).setZero();
      base(currDim) = i;
      RecursiveTotalOrderFill(maxOrder,minOrder,output,currDim+1,base,limiter);
    }
  }
}

void muq::Utilities::MultiIndexFactory::RecursiveHyperbolicFill(const double maxNormPow,
                                                                shared_ptr<MultiIndexSet> output,
                                                                unsigned int const currDim,
                                                                Eigen::RowVectorXu &base,
                                                                const double q,
                                                                shared_ptr<MultiIndexLimiter> limiter)
{
  double currNorm = 0;
  for(int i=0; i<currDim; ++i)
    currNorm += pow(static_cast<double>(base(i)),q);
  
  const int length = base.size();
  
  if(currDim==length-1)
  {
    double newNorm = currNorm;
    base(length-1) = 0;
    while(newNorm<maxNormPow)
    {
      auto newTerm = make_shared<MultiIndex>(base);
      if(limiter->IsFeasible(newTerm))
        output->AddActive(newTerm);
      base(length-1)++;
      newNorm = currNorm + pow(static_cast<double>(base(length-1)),q);
    }
    
  }else{
    double newNorm = currNorm;
    base.tail(length-currDim).setZero();
    while(newNorm<maxNormPow)
    {
      RecursiveHyperbolicFill(maxNormPow,output,currDim+1,base,q,limiter);
      base(currDim)++;
      newNorm = currNorm + pow(static_cast<double>(base(currDim)),q);
    }
    
  }
}

                                                    
void muq::Utilities::MultiIndexFactory::RecursiveTensor(const Eigen::RowVectorXu& orders,
                                                        shared_ptr<MultiIndexSet> output,
                                                        unsigned int const currDim,
                                                        Eigen::RowVectorXu &base,
                                                        int &localInd,
                                                        int &activeInd,
                                                        bool allInactive)
{
  const unsigned length = base.size();
  if(currDim==length-1)
  {
    int globalInd;
    shared_ptr<MultiIndex> newMulti;
    // add all the active indices
    for(int i=0; i<=orders(length-1); ++i)
    {
      base(length-1) = i;
      newMulti = make_shared<MultiIndex>(base);
      globalInd = output->pool->AddMulti(newMulti);
      output->multi2local[newMulti] = localInd;
      output->local2global.push_back(globalInd);
      
      if(!allInactive){
        output->active2local.push_back(localInd);
        output->local2active.push_back(activeInd);
        activeInd++;
      }else{
        output->local2active.push_back(-1);
      }
      localInd++;
    }
    
    // add one inactive index at the end
    base(length-1) = orders(length-1)+1;
    newMulti =make_shared<MultiIndex>(base);
    globalInd = output->pool->AddMulti(newMulti);
    
    output->local2global.push_back(globalInd);
    output->multi2local[newMulti] = localInd;
    output->local2active.push_back(-1);
    localInd++;
      
  }else{
    
    // add all the active nodes
    for(int i=0; i<=orders(currDim); ++i)
    {
      base.tail(length-currDim).setZero();
      base(currDim) = i;
      RecursiveTensor(orders,output,currDim+1,base,localInd,activeInd,allInactive);
    }
    
    // add a bunch of inactive neighboring nodes
    base.tail(length-currDim).setZero();
    base(currDim) = orders(currDim)+1;
    RecursiveTensor(orders,output,currDim+1,base,localInd,activeInd,true);
    
  }
}


shared_ptr<MultiIndexSet> muq::Utilities::MultiIndexFactory::CreateTotalOrder(unsigned int const length,
                                                                              unsigned int const maxOrder,
                                                                              unsigned int const minOrder,
                                                                              std::shared_ptr<MultiIndexLimiter> limiter,
                                                                              std::shared_ptr<MultiIndexPool> pool)
{
  assert(maxOrder>=minOrder);
  assert(minOrder>=0);
  assert(length>0);
  
  // create an empy multiindex set
  shared_ptr<MultiIndexSet> output = make_shared<MultiIndexSet>(length,make_shared<NoLimiter>(),pool);
  
  // start with a vector of zeros
  Eigen::RowVectorXu base = Eigen::RowVectorXu::Zero(length);
  
  RecursiveTotalOrderFill(maxOrder,minOrder,output,0,base,limiter);

  return output;
}

std::vector<std::shared_ptr<MultiIndexSet>> muq::Utilities::MultiIndexFactory::CreateTriTotalOrder(unsigned int const length,
                                                                                                   unsigned int const maxOrder,
                                                                                                   unsigned int const minOrder,
                                                                                                   std::shared_ptr<MultiIndexLimiter> limiter)
{
  
  auto pool = make_shared<MultiIndexPool>();
  vector<shared_ptr<MultiIndexSet>> multis(length);
  for(int d=0; d<length; ++d){
    auto dimLimiter = make_shared<AndLimiter>(limiter,make_shared<DimensionLimiter>(0,d+1));
    multis.at(d) = MultiIndexFactory::CreateTotalOrder(length,maxOrder,minOrder,dimLimiter,pool);
  }
  return multis;
}

shared_ptr<MultiIndexSet> muq::Utilities::MultiIndexFactory::CreateHyperbolic(unsigned int const length,
                                                                              unsigned int const maxOrder,
                                                                              const double q,
                                                                              std::shared_ptr<MultiIndexLimiter> limiter,
                                                                              std::shared_ptr<MultiIndexPool> pool)
{
  assert(maxOrder>=0);
  assert(length>0);
  
  // create an empy multiindex set
  shared_ptr<MultiIndexSet> output = make_shared<MultiIndexSet>(length,make_shared<NoLimiter>(),pool);
  
  // start with a vector of zeros
  Eigen::RowVectorXu base = Eigen::RowVectorXu::Zero(length);
  
  const double nugget = 1e-5;// needed for maximum power to be inclusive
  RecursiveHyperbolicFill(pow(static_cast<double>(maxOrder),q)+nugget,output,0,base,q,limiter);
  
  return output;
}

std::vector<std::shared_ptr<MultiIndexSet>> muq::Utilities::MultiIndexFactory::CreateTriHyperbolic(unsigned int const length,
                                                                                                   unsigned int const maxOrder,
                                                                                                   const double q,
                                                                                                   std::shared_ptr<MultiIndexLimiter> limiter)
{
  
  auto pool = make_shared<MultiIndexPool>();
  vector<shared_ptr<MultiIndexSet>> multis(length);
  for(int d=0; d<length; ++d){
    auto dimLimiter = make_shared<AndLimiter>(limiter,make_shared<DimensionLimiter>(0,d+1));
    multis.at(d) = MultiIndexFactory::CreateHyperbolic(length,maxOrder,q,dimLimiter,pool);
  }
  return multis;
}

std::shared_ptr<MultiIndexSet> muq::Utilities::MultiIndexFactory::CreateFullTensor(unsigned int const length,
                                                                                   unsigned int const order,
                                                                                   std::shared_ptr<MultiIndexPool> pool)
{
  return muq::Utilities::MultiIndexFactory::CreateFullTensor(order*Eigen::RowVectorXu::Ones(length),pool);
}


std::shared_ptr<MultiIndexSet> muq::Utilities::MultiIndexFactory::CreateFullTensor(const Eigen::RowVectorXu& orders,
                                                                                   std::shared_ptr<MultiIndexPool> pool)
{
  assert(orders.minCoeff()>=0);
  assert(orders.size()>0);
  
  unsigned int length = orders.size();
  unsigned int numIndices = (orders.array()+2).prod(); // an overestimate of the total number of indices
  unsigned int numActiveIndices = (orders.array()+1).prod(); // the exact number of active indices
  
  // create an empy multiindex set
  shared_ptr<MultiIndexSet> output = make_shared<MultiIndexSet>(length,make_shared<NoLimiter>(),pool);
  output->local2active.reserve(numIndices);
  output->local2global.reserve(numIndices);
  output->active2local.reserve(numActiveIndices);
  
  // start with a vector of zeros
  Eigen::RowVectorXu base = Eigen::RowVectorXu::Zero(length);
  
  int localInd = 0;
  int activeInd = 0;
  RecursiveTensor(orders, output, 0, base, localInd, activeInd, false);
  
  
  // now fill in the input and output edges
  Eigen::RowVectorXi dimSizes(orders.size());
  dimSizes(0) = 1;
  for(int d=1; d<orders.size(); ++d)
    dimSizes(d) = static_cast<int>((dimSizes(d-1))*(orders(orders.size()-d)+2));
  
  output->outEdges.resize(output->local2active.size());
  output->inEdges.resize(output->local2active.size());
  
  for(int i=0; i<localInd; ++i){
    for(int d=0; d<orders.size(); ++d){
      if((i-dimSizes(d))>=0){
        output->inEdges.at(i).insert(i-dimSizes(d));
        output->outEdges.at(i-dimSizes(d)).insert(i);
      }
    }
  }
    
  // update other information in the output set
  output->maxOrders = orders.transpose();
  output->dim = length;
  
  return output;
}


std::shared_ptr<MultiIndex> MultiIndexFactory::CreateSingleTerm(int totalDim, int nonzeroDim, int order)
{
  shared_ptr<MultiIndex> output = make_shared<MultiIndex>(totalDim);
  output->SetValue(nonzeroDim,order);
  return output;
}