#include "MUQ/Utilities/Polynomials/HermitePolynomials1DRecursive.h"

#include <boost/math/constants/constants.hpp>
#include <boost/serialization/access.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/math/special_functions/factorials.hpp>

using namespace muq::Utilities;


double HermitePolynomials1DRecursive::alpha(double x, unsigned int k)
{
  assert(k < 250 && "Hermite are difficult to compute and are only tested to 200th order. May be unstable beyond this");
  return -2.0 * x;
}

double HermitePolynomials1DRecursive::beta(double x, unsigned int k)
{
  return 2.0 * k;
}

double HermitePolynomials1DRecursive::phi0(double x)
{
  return 1.0;
}

double HermitePolynomials1DRecursive::phi1(double x)
{
  return 2.0 * x;
}

double HermitePolynomials1DRecursive::GetNormalization_static(unsigned int n)
{
  return boost::math::constants::root_pi<double>() *
         pow(2.0, static_cast<double>(n)) * boost::math::factorial<double>(n);

  //	return 1.0;
}

double HermitePolynomials1DRecursive::gradient_static(unsigned int order, double x)
{
  //this definition provided by http://functions.wolfram.com/Polynomials/HermiteH/20/01/01/
  if (order == 0) {
    //order 0 is a constant
    return 0;
  } else {
    return 2 * static_cast<double>(order) * evaluate_static(order - 1, x);
  }
}

double HermitePolynomials1DRecursive::secondDerivative_static(unsigned int order, double x)
{
  //this definition provided by http://functions.wolfram.com/Polynomials/HermiteH/20/01/01/
  if (order < 2) {
    //order 0 is a constant
    return 0;
  } else {
    return 2 * static_cast<double>(order) * gradient_static(order - 1, x);
  }
}


Eigen::VectorXd HermitePolynomials1DRecursive::GetMonomialCoeffs(const int order){
  
  Eigen::VectorXd monoCoeffs = Eigen::VectorXd::Zero(order+1);
  
  if(order==0){
    monoCoeffs(0) = 1.0;
  }else if(order ==1){
    monoCoeffs(1) = 2.0;
  }else if(order==2){
    monoCoeffs(0) = -2.0;
    monoCoeffs(2) = 4.0;
  }else if(order==3){
    monoCoeffs(1) = -12.0;
    monoCoeffs(3) = 8.0;
  }else if(order==4){
    monoCoeffs(0) = 12.0;
    monoCoeffs(2) = -48.0;
    monoCoeffs(4) = 16.0;
  }else if(order==5){
    monoCoeffs(1) = 120.0;
    monoCoeffs(3) = -160.0;
    monoCoeffs(5) = 32.0;
  }else{
    Eigen::VectorXd oldOldCoeffs = Eigen::VectorXd::Zero(order+1);
    Eigen::VectorXd oldCoeffs = Eigen::VectorXd::Zero(order+1);
    oldOldCoeffs(0) = 1.0; // constant term -- same as monomial
    oldCoeffs(1) = 2.0;    // linear term -- same as monomial
    for(int i=2; i<=order; ++i){
      monoCoeffs = Eigen::VectorXd::Zero(order+1);
      monoCoeffs.tail(order) = 2*oldCoeffs.head(order);
      monoCoeffs -= 2*(i-1)*oldOldCoeffs;
      
      oldOldCoeffs = oldCoeffs;
      oldCoeffs = monoCoeffs;
    }
  }
  return monoCoeffs;
  
}

template<class Archive>
void HermitePolynomials1DRecursive::serialize(Archive& ar, const unsigned int version)
{
  //nothing to serialize except for the parent and the type of the object
  ar& boost::serialization::base_object<Static1DPolynomial<HermitePolynomials1DRecursive>>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(muq::Utilities::Static1DPolynomial<HermitePolynomials1DRecursive>)
BOOST_CLASS_EXPORT_IMPLEMENT(muq::Utilities::HermitePolynomials1DRecursive)

template void HermitePolynomials1DRecursive::serialize(boost::archive::text_oarchive& ar, unsigned int version);
template void HermitePolynomials1DRecursive::serialize(boost::archive::text_iarchive& ar, const unsigned int version);

template void Static1DPolynomial<HermitePolynomials1DRecursive>::serialize(boost::archive::text_oarchive& ar, const unsigned int version);
template void Static1DPolynomial<HermitePolynomials1DRecursive>::serialize(boost::archive::text_iarchive& ar, const unsigned int version);


