#include "MUQ/Utilities/python/MultiIndexPython.h"

#include "MUQ/Utilities/multiIndex/MultiIndexSet.h"
#include "MUQ/Utilities/multiIndex/MultiIndex.h"
#include "MUQ/Utilities/multiIndex/MultiIndexFactory.h"

using namespace std;
namespace py = boost::python;
using namespace muq::Utilities;


boost::python::list MultiIndex::PyGetMulti() const{
  return GetPythonVector(GetMulti());
}

std::shared_ptr<MultiIndex> MultiIndex::PyCreate(boost::python::list const& multiIn){
  return make_shared<MultiIndex>(GetEigenVector<Eigen::RowVectorXu>(multiIn));
}

boost::python::list MultiIndex::GetNz() const{
  
  Eigen::MatrixXi temp(2,nzInds.size());
  for(int i=0;i<nzInds.size(); ++i){
    temp(0,i) = nzInds.at(i).first;
    temp(1,i) = nzInds.at(i).second;
  }
  
  return GetPythonMatrix(temp);
}


boost::python::list MultiIndexSet::PyGetAllMultiIndices() const{
  return GetPythonMatrix(GetAllMultiIndices());
}
boost::python::list MultiIndexSet::PyIndexToMulti(unsigned int const activeIndex) const{
  return GetPythonVector(IndexToMulti(activeIndex));
}
int MultiIndexSet::PyMultiToIndex1(boost::python::list const& multiIn) const{
  return MultiToIndex(GetEigenVector<Eigen::RowVectorXu>(multiIn));
}
int MultiIndexSet::PyMultiToIndex2(std::shared_ptr<MultiIndex> multiIndex) const{
  return MultiToIndex(multiIndex);
}
boost::python::list MultiIndexSet::PyGetMaxOrders() const{
  return GetPythonVector(GetMaxOrders());
}
void MultiIndexSet::PyActivate1(boost::python::list const& multiIndex){
  Activate(GetEigenVector<Eigen::RowVectorXu>(multiIndex));
}
void MultiIndexSet::PyActivate2(std::shared_ptr<MultiIndex> multiIndex){
  Activate(multiIndex);
}
int MultiIndexSet::PyAddActive1(boost::python::list const& multiIndex){
  return AddActive(GetEigenVector<Eigen::RowVectorXu>(multiIndex));
}
int MultiIndexSet::PyAddActive2(std::shared_ptr<MultiIndex> multiIndex){
  return AddActive(multiIndex);
}
boost::python::list MultiIndexSet::PyExpand(unsigned int activeIndex){
  return GetPythonVector(Expand(activeIndex));
}
boost::python::list MultiIndexSet::PyForciblyExpand(unsigned int activeIndex){
  return GetPythonVector(ForciblyExpand(activeIndex));
}
boost::python::list MultiIndexSet::PyForciblyActivate1(std::shared_ptr<MultiIndex> multiIndex){
  return GetPythonVector(ForciblyActivate(multiIndex));
}
boost::python::list MultiIndexSet::PyForciblyActivate2(boost::python::list const& multiIndex){
  return GetPythonVector(ForciblyActivate(GetEigenVector<Eigen::RowVectorXu>(multiIndex)));
}
boost::python::list MultiIndexSet::PyGetAdmissibleForwardNeighbors(unsigned int activeIndex){
  return GetPythonMatrix(GetAdmissibleForwardNeighbors(activeIndex));
}
bool MultiIndexSet::PyIsAdmissible1(boost::python::list const& multiIndex) const{
  return IsAdmissible(GetEigenVector<Eigen::RowVectorXu>(multiIndex));
}
bool MultiIndexSet::PyIsAdmissible2(std::shared_ptr<MultiIndex> multiIndex) const{
  return IsAdmissible(multiIndex);
}
bool MultiIndexSet::PyIsActive1(boost::python::list const& multiIndex) const{
  return IsActive(GetEigenVector<Eigen::RowVectorXu>(multiIndex));
}
bool MultiIndexSet::PyIsActive2(std::shared_ptr<MultiIndex> multiIndex) const{
  return IsActive(multiIndex);
}

void muq::Utilities::ExportMultiIndex(){
  
  py::class_<MultiIndex, shared_ptr<MultiIndex>> exportMulti("MultiIndex", py::init<int>());
  
  exportMulti.def("__init__",py::make_constructor(MultiIndex::PyCreate));
  exportMulti.def("Clone", &MultiIndex::Clone).staticmethod("Clone");
  
  exportMulti.def("GetMulti", &MultiIndex::PyGetMulti);
  exportMulti.def("GetOrder", &MultiIndex::GetOrder);
  exportMulti.def("GetMax", &MultiIndex::GetMax);
  exportMulti.def("SetValue", &MultiIndex::SetValue);
  exportMulti.def("GetValue", &MultiIndex::GetValue);
  exportMulti.def("GetDimension", &MultiIndex::SetDimension);
  exportMulti.def("GetDimension", &MultiIndex::GetDimension);

}



void muq::Utilities::ExportMultiIndexSet(){
  py::class_<MultiIndexSet, shared_ptr<MultiIndexSet>> exportMulti("MultiIndexSet", py::init<int>());
  
  exportMulti.def("CloneExisting", &MultiIndexSet::CloneExisting).staticmethod("CloneExisting");
  exportMulti.def("GetAllMultiIndices", &MultiIndexSet::PyGetAllMultiIndices);
  exportMulti.def("GetNumberOfIndices", &MultiIndexSet::GetNumberOfIndices);
  
  exportMulti.def("IndexToMulti", &MultiIndexSet::PyIndexToMulti);
  exportMulti.def("IndexToMultiPtr", &MultiIndexSet::IndexToMultiPtr);
  exportMulti.def("MultiToIndex", &MultiIndexSet::PyMultiToIndex1);
  exportMulti.def("MultiToIndex", &MultiIndexSet::PyMultiToIndex2);
  exportMulti.def("GetMultiIndexLength", &MultiIndexSet::GetMultiIndexLength);
  //exportMulti.def("GetMaxOrders", &MultiIndexSet::PyGetMaxOrders);
  
  exportMulti.def("at", &MultiIndexSet::IndexToMultiPtr);
  exportMulti.def("size", &MultiIndexSet::size);
  exportMulti.def("Activate", &MultiIndexSet::PyActivate1);
  exportMulti.def("Activate", &MultiIndexSet::PyActivate2);
  exportMulti.def("AddActive", &MultiIndexSet::PyAddActive1);
  exportMulti.def("AddActive", &MultiIndexSet::PyAddActive2);
  exportMulti.def("Expand", &MultiIndexSet::PyExpand);
  exportMulti.def("ForciblyExpand", &MultiIndexSet::PyForciblyExpand);
  exportMulti.def("ForciblyActivate", &MultiIndexSet::PyForciblyActivate1);
  exportMulti.def("ForciblyActivate", &MultiIndexSet::PyForciblyActivate2);
  exportMulti.def("GetAdmissibleForwardNeighbors", &MultiIndexSet::PyGetAdmissibleForwardNeighbors);
  exportMulti.def("IsAdmissible", &MultiIndexSet::PyIsAdmissible1);
  exportMulti.def("IsAdmissible", &MultiIndexSet::PyIsAdmissible2);
  
  exportMulti.def("IsActive", &MultiIndexSet::PyIsActive1);
  exportMulti.def("IsActive", &MultiIndexSet::PyIsActive2);
}


void muq::Utilities::ExportMultiIndexFactory(){
  py::class_<MultiIndexFactory, shared_ptr<MultiIndexFactory>> exportMulti("MultiIndexFactory");
  
  exportMulti.def("CreateTotalOrder", &MultiIndexFactory::PyCreateTotalOrder1);
  exportMulti.def("CreateTotalOrder", &MultiIndexFactory::PyCreateTotalOrder2).staticmethod("CreateTotalOrder");
  exportMulti.def("CreateHyperbolic", &MultiIndexFactory::PyCreateHyperbolic1);
  exportMulti.def("CreateHyperbolic", &MultiIndexFactory::PyCreateHyperbolic2).staticmethod("CreateHyperbolic");
  exportMulti.def("CreateFullTensor", &MultiIndexFactory::PyCreateFullTensor1);
  exportMulti.def("CreateFullTensor", &MultiIndexFactory::PyCreateFullTensor2).staticmethod("CreateFullTensor");
  
  exportMulti.def("CreateSingleTerm", &MultiIndexFactory::CreateSingleTerm);
}
