
##########################################
#  Objects to build


# files containing google tests
set(Local_Test_Optimization_Sources

    Optimization/test/LineSearchTest.cpp
    Optimization/test/ConstraintTest.cpp
    Optimization/test/ConstrainedOptTest.cpp
    Optimization/test/StochasticOptTest.cpp
    Optimization/test/AssignmentTests.cpp
    Optimization/test/LinearLeastSquaresTest.cpp

)


# if NLOPT is available, add the NLOPT tests
if(MUQ_USE_NLOPT)

    list(APPEND Local_Test_Optimization_Sources
     Optimization/test/NloptTest.cpp
     Optimization/test/NloptConstrainedTest.cpp
      )

    # set in parent scope
    set(Test_Optimization_Sources "${Local_Test_Optimization_Sources}" PARENT_SCOPE)

endif(MUQ_USE_NLOPT)




