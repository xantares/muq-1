
#include "MUQ/Optimization/Constraints/LinearConstraint.h"


using namespace muq::Optimization;
using namespace std;


/** Construct the Linear constraint using the linear operator A and the vector b.  The constraint will return Ax-b */
LinearConstraint::LinearConstraint(const Eigen::MatrixXd& A, const Eigen::VectorXd& b) : ConstraintBase(
                                                                                           A.cols(), b.rows()), Astored(
                                                                                           A),
                                                                                         bstored(b)
{}


/** Evaluate the linear constraint. */
Eigen::VectorXd LinearConstraint::eval(const Eigen::VectorXd& xc)
{
  return Astored * xc - bstored;
}

/** Evaluate the action of the Jacobian of this constraint on a vector. */
Eigen::VectorXd LinearConstraint::ApplyJacTrans(const Eigen::VectorXd& xc, const Eigen::VectorXd& vecIn)
{
  return Astored.transpose() * vecIn;
}

Eigen::MatrixXd LinearConstraint::getHess(const Eigen::VectorXd& xc, const Eigen::VectorXd& sensitivity)
{
  return Eigen::MatrixXd::Zero(DimIn, DimIn);
}

