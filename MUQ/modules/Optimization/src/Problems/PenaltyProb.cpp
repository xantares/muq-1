
#include "MUQ/Optimization/Problems/PenaltyProb.h"

using namespace muq::Optimization;
using namespace std;


PenaltyProb::PenaltyProb(std::shared_ptr<OptProbBase> prob, double                       penaltyCoeffIn) : OptProbBase(
                                                                                                             prob->GetDim()),
                                                                                                           penaltyCoeff(
                                                                                                             penaltyCoeffIn),
                                                                                                           baseProb(prob)
{}


double PenaltyProb::eval(const Eigen::VectorXd& xc)
{
  // first, get the objective value
  double fval = baseProb->eval(xc);

  // now get the equality constraint values and add them to the objective
  if (baseProb->NumEqualities() > 0) {
    Eigen::VectorXd cVals = baseProb->equalityConsts.eval(xc);
    fval += penaltyCoeff * (cVals.squaredNorm());
  }

  // now get the inequality constraint values and add them to the objective
  if (baseProb->NumInequalities() > 0) {
    Eigen::VectorXd inVals = baseProb->inequalityConsts.eval(xc);
    fval += penaltyCoeff * (inVals.cwiseMax(Eigen::VectorXd::Zero(baseProb->NumInequalities()))).squaredNorm();
  }

  // return the augmented function value
  return fval;
}

/** Evaluate the gradient. */
double PenaltyProb::grad(const Eigen::VectorXd& xc, Eigen::VectorXd& gradient)
{
  // first, get the objective value and gradient of the unconstrained problem
  double fval = baseProb->grad(xc, gradient);

  // now, add the gradient of the equality constraint portion of the Augmented Lagrangian
  if (baseProb->NumEqualities() > 0) {
    // get value of constraints and add to objective
    Eigen::VectorXd cVals = baseProb->equalityConsts.eval(xc);
    fval += penaltyCoeff * (cVals.squaredNorm());

    // get sensitivity of penalized objective to output of constraint
    Eigen::VectorXd Sens = 2.0 * penaltyCoeff * cVals;

    // get sensitivity of penalized objective to input of constraint
    gradient += baseProb->equalityConsts.ApplyJacTrans(xc, Sens);
  }

  // now, add the gradient of the inequality constraint portion
  if (baseProb->NumInequalities() > 0) {
    // get the value of the constraints and add to objective
    Eigen::VectorXd inVals = baseProb->inequalityConsts.eval(xc);
    fval += penaltyCoeff * (inVals.cwiseMax(Eigen::VectorXd::Zero(baseProb->NumInequalities()))).squaredNorm();

    // get sensitivity of penalized objective to output of these inequality constraints
    Eigen::VectorXd Sens = 2.0 * penaltyCoeff * (inVals.cwiseMax(Eigen::VectorXd::Zero(baseProb->NumInequalities())));

    // propagate this sensitivity backward to get sensitivity to constraint input
    gradient += baseProb->inequalityConsts.ApplyJacTrans(xc, Sens);
  }

  // return the penalized function value
  return fval;
}

