//
//  SAA.cpp
//
//
//  Created by Matthew Parno on 5/16/13.
//  Copyright (c) 2013 MIT. All rights reserved.
//

// self include
#include "MUQ/Optimization/Algorithms/SAA.h"
#include "MUQ/Optimization/Problems/SAAProb.h"


using namespace muq::Optimization;
using namespace std;


REGISTER_OPT_DEF_TYPE(SAA)
/** Construct an Augmented Lagrangian based optimization solver from the settings in a ptree.  The ptree should use some
 * base optimization algorithm as Opt.Method and should have an Opt.ConstraintHandler set to AugLag. */
SAA::SAA(shared_ptr<OptProbBase> ProbPtr, boost::property_tree::ptree& properties) : OptAlgBase(ProbPtr,
                                                                                                properties,
                                                                                                true,
                                                                                                false,
                                                                                                false,
                                                                                                false,
                                                                                                false,
                                                                                                false,
                                                                                                false,
                                                                                                true), subProp(
                                                                                       properties)
{
  // read in the SAA method
  string method = properties.get("Opt.SAA.Method", "BFGS_Line");

  // get the number of optimization runs to perform
  NumRuns = properties.get("Opt.SAA.NumRuns", 5);

  // set the deterministic solver
  subProp.put("Opt.Method", method);
}

/** Solve the stochastic optimization problem with a deterministic solver by setting the random seed everytime the
 * objective is called, which approximates the true stochastic problem by a deterministic problem.  The optimization may
 * be repeated a few times with different seeds and the mean solution returned. */
Eigen::VectorXd SAA::solve(const Eigen::VectorXd& x0)
{
  // create a matrix to hold results from all of the optimization runs
  Eigen::MatrixXd allResults(OptProbPtr->GetDim(), NumRuns);
  Eigen::VectorXi allStats(NumRuns);

  for (int i = 0; i < NumRuns; ++i) {
    // create the deterministic optimization problem
    shared_ptr<OptProbBase> DetProbPtr = make_shared<SAAProb>(OptProbPtr, time(NULL));

    // create an instance of the deterministic solver
    shared_ptr<OptAlgBase> DetSolver = OptAlgBase::Create(DetProbPtr, subProp);

    // solve the deterministic subproblem with this seed
    allResults.col(i) = DetSolver->solve(x0);
    allStats[i]       = DetSolver->GetStatus();
  }

  status = allStats[0];

  return allResults.rowwise().sum() / NumRuns;
}
