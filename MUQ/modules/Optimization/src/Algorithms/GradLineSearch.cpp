//
//  GradLineSearch.cpp
//
//
//  Created by Matthew Parno on 5/13/13.
//  Copyright (c) 2013 MIT. All rights reserved.
//

//self include
#include "MUQ/Optimization/Algorithms/GradLineSearch.h"


using namespace muq::Optimization;
using namespace std;

/** Construct the line search method from a pointer to the optimization problem and a parameter list containing the
 * number of line search iterations, backtracing method, etc... */
GradLineSearch::GradLineSearch(shared_ptr<OptProbBase> ProbPtr, boost::property_tree::ptree& properties) : OptAlgBase(
                                                                                                             ProbPtr,
                                                                                                             properties,
                                                                                                             true,
                                                                                                             false,
                                                                                                             false,
                                                                                                             false,
                                                                                                             false,
                                                                                                             false,
                                                                                                             false,
                                                                                                             false)
{
  // get maximum number of line search iterations
  maxLineIts = properties.get("Opt.LineSearch.LineIts", 100);

  // get line search shrink parameters
  beta = properties.get("Opt.LineSearch.LineBeta", 0.5);
}

/** Unconstrained minimization routine that uses child class descent direction and a backtracing Armijo line-search
 */
Eigen::VectorXd GradLineSearch::solve(const Eigen::VectorXd& x0)
{
  // make sure the input vector is the correct size
  assert(x0.size() == OptProbPtr->GetDim());
  int dim = OptProbPtr->GetDim();

  // allocate space for a vector of descent direction
  Eigen::VectorXd delta = Eigen::VectorXd::Zero(dim);

  // set state to initially be the starting position
  xc = x0;

  // stopFlag=0 when we should continue, 1 to stop
  bool stopFlag = false;
  int  it       = 0; // number of iterations

  // optimization loop
  status = -1;
  while (!stopFlag) {
    // add to number of iterations
    ++it;

    // get a descent direction
    delta = step();

    // perform the line search -- this will change xc and fnew
    if (lineSearch(delta)) {
      status   = -1;
      stopFlag = true;
    }

    // check to see if we've converged
    if (fabs(fold - fnew) < ftol) {
      status   = 3;
      fold     = fnew;
      stopFlag = true;
    }

    // stop if the change is too small
    if ((lambda * delta).norm() < xtol) {
      status   = 4;
      stopFlag = true;
    }

    // stop if we've reached the maximum number of iterations
    if (it == maxIts) {
      status   = 5;
      stopFlag = true;
    }

    // if we want to be really verbose, print the iteration and function value
    if (verbose > 2) {
      cout << "Iteration: " << it << "   fval: " << fold << endl;
      if (stopFlag) {
        cout << "Terminating with status: " << status << std::endl;
      }
    }
  }

  return xc;
}

/** perform a line search, just updates the state given a descent direction.  This function updates xc with the new step
 * and fnew with the new function value.
 */
bool GradLineSearch::lineSearch(const Eigen::VectorXd& descDir)
{
  // where is the end of the line? (i.e. xc + lambda*descDir)
  Eigen::VectorXd newX;

  // some utility variables
  int  lineIt   = 0;
  bool failFlag = true;

  // start by assuming the proposed point has larger function value
  double fline = fold;

  // line search loop
  lambda = 1.0;

  // compute the projection of the descent direction onto the gradient
  double GradDotDesc = descDir.dot(gc);

  while (lineIt < maxLineIts) {
    ++lineIt;

    // add descent direction to the current position to get a proposed position
    newX = xc + lambda * descDir;

    // get function value at this new point
    fline = OptProbPtr->eval(newX);

    // check for sufficient decrease -- the Armijo condition
    if ((fline - fold) < c1 * lambda * GradDotDesc) {
      // we have sufficient decrease, so accept this point and return
      fnew     = fline;
      xc       = newX;
      failFlag = false;
      break;
    } else {
      // if we do not have sufficient decrease, shrink lambda and try again
      lambda *= beta;
    }
  } // line search

  // return success or failure
  return failFlag;
}

