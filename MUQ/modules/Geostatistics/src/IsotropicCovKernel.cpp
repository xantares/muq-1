
#include <assert.h>
#include <iostream>

#include <boost/property_tree/ptree.hpp>

#include "MUQ/Geostatistics/IsotropicCovKernel.h"
#include "MUQ/Utilities/mesh/Mesh.h"

using namespace muq::Geostatistics;
using namespace std;


/** Evaluate the base kernel by computing the distance between x and y and then calling DistKernel */
double IsotropicCovKernel::Kernel(const Eigen::Ref<const Eigen::VectorXd>& x,
                                  const Eigen::Ref<const Eigen::VectorXd>& y) const
{
  dimIn = x.rows();
  static Eigen::VectorXd diff;
  diff.resizeLike(x);
  for (unsigned i = 0; i < dimIn; ++i) {
    diff(i) = x(i) - y(i);
  }

  return DistKernel(diff.norm());
}

void IsotropicCovKernel::BuildGradMat(const Eigen::MatrixXd& xs, std::vector<Eigen::MatrixXd>& GradMat) const
{
  // make sure all the dimensions match
  int dim = xs.cols();

  //only wipe the gradmat if it's not the right size
  if (GradMat.size() != Nparams) {
    GradMat.resize(Nparams, Eigen::MatrixXd::Zero(dim, dim)); //just resize, no need to clear
  }

  //check the matrices are all the right size, on the off chance the dim has somehow changed
  for (unsigned i = 0; i < Nparams; ++i) {
    GradMat.at(i).resize(dim, dim);
  }

  Eigen::VectorXd grad(Nparams);

  // outer loop -- column index of covariance matrix
  for (int col = 0; col < dim; ++col) {
    // inner loop -- row index of covariance matrx
    for (int row = 0; row <= col; ++row) {
      GetGrad((xs.col(col) - xs.col(row)).norm(), grad);

      for (unsigned int i = 0; i < Nparams; ++i) {
        GradMat[i](row, col) = grad[i];
        GradMat[i](col, row) = grad[i];
      }
    }
  }
}

/** Return an approximate lengthscale */
double IsotropicCovKernel::EstimateLength() const
{
  // tolerance
  double tol = 1e-4;


  double lb = 0;
  double ub = 1;

  // get the variance
  double var = DistKernel(0);

  // find bounds on the value we want
  while (DistKernel(ub) >= tol * var) {
    lb  = ub;
    ub *= 2;
  }

  // use bisection to find our threshold level
  double x = 0;
  while ((ub - lb) > 1e-4) {
    x = lb + (ub - lb) / 2.0;

    if (DistKernel(x) > tol * var) {
      lb = x;
    } else {
      ub = x;
    }
  }

  return x;
}

Eigen::VectorXd IsotropicCovKernel::KernelSpatialDerivative(Eigen::VectorXd const& x, Eigen::VectorXd const& xWrt)
{
  dimIn = x.rows();
  Eigen::VectorXd rVec = xWrt - x;
  double r             = rVec.norm();
  return rVec / r * KernelRadialDerivative(r);
}

