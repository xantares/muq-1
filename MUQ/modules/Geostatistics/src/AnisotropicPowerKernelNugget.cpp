
#include "MUQ/Geostatistics/AnisotropicPowerKernelNugget.h"
#include "MUQ/Utilities/LogConfig.h"
#include "MUQ/Utilities/EigenUtils.h"

using namespace Eigen;
using namespace muq::Geostatistics;
using namespace muq::Utilities;

AnisotropicPowerKernelNugget::AnisotropicPowerKernelNugget(Eigen::VectorXd lengthScalesIn, double PowerIn,
                                               double nugget) : lengthScales(lengthScalesIn), Power(PowerIn), Nparams(1 +
                                                                                 lengthScales.rows()), nugget(nugget)
{}


double AnisotropicPowerKernelNugget::Kernel(const Eigen::Ref<const Eigen::VectorXd>& x,
                                      const Eigen::Ref<const Eigen::VectorXd>& y) const
{
  unsigned dimIn = x.rows();
  static Eigen::VectorXd diff;

  diff.resizeLike(x);
  for (unsigned i = 0; i < dimIn; ++i) {
    diff(i) = x(i) - y(i);
  }

//    LOG(INFO ) << "x " << x  << " y " << y;
//    LOG(INFO ) << "diff " <<diff  << " lengthScales " << lengthScales;
//    LOG(INFO ) << "one var " << Power << " b " << nugget ;

     double   result = exp(-1.0 * (diff.array() / lengthScales.array()).abs().pow(Power).sum()/Power);
  //double result = variance  * exp(-1.0 * (diff.array().square().matrix().dot(lengthScales)));
// 	 VectorXd temp = (diff.array() / lengthScales.array());
// 	 	 VectorXd temp2 = tedmp.pow(Power);
//  LOG(INFO) << " d " <<  temp  << " a " << temp2 << " c " << result;
  //if exactly equal, apply the nugget
  if(MatrixEqual(x,y)) {
    result += nugget;
  }
  
  assert(!std::isnan(result)); //can underflow during optimization
  // evaluate the kernel
  return result;
}

Eigen::MatrixXd AnisotropicPowerKernelNugget::BuildCov(Eigen::MatrixXd const& xs) const{
// 	LOG(INFO) << "fast build cov";
	assert(xs.rows() == lengthScales.rows());
	int N = xs.cols();
	MatrixXd cov = MatrixXd::Zero(N,N);
	
	if(Power == 2.0){
// 		LOG(INFO) << "really fast cov";
		auto squaredNorms = (xs.array() / lengthScales.replicate(1,N).array()).matrix().colwise().squaredNorm().replicate(N,1);
		cov = squaredNorms + squaredNorms.transpose() - 2.0* xs.transpose() * (1/lengthScales.array().square()).matrix().asDiagonal() * xs;
		cov *= -1.0/ Power;
	} else {
	
	for(int i=0; i<xs.rows(); ++i){
// 		LOG(INFO) << "diff mat " << (xs.row(i).replicate(N, 1) - xs.row(i).transpose().replicate(1, N)).array().abs();
// 		LOG(INFO) << "diff mat " << (xs.row(i).replicate(N, 1) - xs.row(i).transpose().replicate(1, N)).array().abs().pow(Power).matrix()*pow(lengthScales(i), -Power)/Power;
		cov -= (xs.row(i).replicate(N, 1) - xs.row(i).transpose().replicate(1, N)).array().abs().pow(Power).matrix()*pow(lengthScales(i), -Power)/Power;
	}
	}

	cov = cov.array().exp();
	cov += nugget*MatrixXd::Identity(N,N);
	return cov;
}

Eigen::VectorXd AnisotropicPowerKernelNugget::BuildCov(Eigen::MatrixXd const& xs, Eigen::VectorXd const& y) const{
// 	LOG(INFO) << "fast build cov";
	assert(xs.rows() == lengthScales.rows());
	int N = xs.cols();
	VectorXd cov = VectorXd::Zero(N);
	
	if(Power == 2.0){
// 		LOG(INFO) << "really fast cov";
		MatrixXd scaledDiff = (xs - y.replicate(1,N)).array()/lengthScales.replicate(1,N).array();
		cov = scaledDiff.colwise().squaredNorm().transpose();
		cov *= -1.0/ Power;
	} else {
		assert(false);
	}
	
	cov = cov.array().exp();

	for(int i=0; i<N; ++i){
		if( MatrixEqual(xs.col(i), y)){
			cov(i) += nugget;
		}
	}
	
	
	return cov;
}

void AnisotropicPowerKernelNugget::BuildGradMat(const Eigen::MatrixXd& xs, std::vector<Eigen::MatrixXd>& GradMat) const
{
// 	LOG(INFO) << "fast build cov grad";
	
  //NOTE: copy/paste from IsotropicCovKernel

  // make sure all the dimensions match
  int dim = xs.cols();
// 	LOG(INFO) << "fast build cov grad1";
  //only wipe the gradmat if it's not the right size
  if (static_cast<int>(GradMat.size()) != Nparams) {
    GradMat.resize(Nparams, Eigen::MatrixXd::Zero(dim, dim)); //just resize, no need to clear
  }
// 	LOG(INFO) << "fast build cov grad2";
  
	
//   	LOG(INFO) << "fast build cov grad3";
  //special case, the first one is just the identity
  GradMat.at(0) = MatrixXd::Identity(dim, dim);
  
  MatrixXd cov = BuildCov(xs);
//   	LOG(INFO) << "fast build cov grad4 " << cov.rows() << " " << cov.cols();
  for(int i=0; i<xs.rows(); ++i){
	  MatrixXd xs_i_diff;
	  
	  	if(Power == 2.0){
// 		LOG(INFO) << "really fast cov";
// 		auto squaredNorms = (xs.array() / lengthScales.replicate(1,dim).array()).matrix().colwise().squaredNorm().replicate(dim,1);
// 		xs_i_diff = -(squaredNorms + squaredNorms.transpose() - 2.0* xs.transpose() * (1/lengthScales.array().square()).matrix().asDiagonal() * xs);
		
			xs_i_diff =  (xs.row(i).replicate(dim, 1) - xs.row(i).transpose().replicate(1, dim)).array().abs().square();	
		  GradMat.at(i+1) = (cov.array() * xs_i_diff.array()).matrix()*pow(lengthScales(i), -Power-1.0);
		
// 			  GradMat.at(i+1) = (cov.array() * xs_i_diff.array()).matrix()*pow(lengthScales(i), -Power-1.0);

	} else {
	xs_i_diff =  (xs.row(i).replicate(dim, 1) - xs.row(i).transpose().replicate(1, dim)).array().abs();	
		  GradMat.at(i+1) = (cov.array() * xs_i_diff.array().pow(Power)).matrix()*pow(lengthScales(i), -Power-1.0);

	}
	  
	 
// 	  LOG(INFO) << "fast build cov grad6 " << xs_i_diff.rows() << " " << xs_i_diff.cols();
// 	  LOG(INFO) << "fast build cov grad7 " << cov.array() * xs_i_diff.array();
// 	  LOG(INFO) << "fast build cov grad8 " << (cov.array() * xs_i_diff.array().pow(Power)).matrix();
// 	  LOG(INFO) << "fast build cov grad9 " << (cov.array() * xs_i_diff.array().pow(Power)).matrix()*pow(lengthScales(i), -Power-1.0);
// 	  LOG(INFO) << "fast build cov grad9 " << GradMat.at(i+1) ;
  }
//   	LOG(INFO) << "fast build cov grad5";

}

void AnisotropicPowerKernelNugget::GetGrad(const Eigen::Ref<const Eigen::VectorXd>& x,
                                     const Eigen::Ref<const Eigen::VectorXd>& y,
                                     Eigen::VectorXd                        & grad) const
{
  VectorXd diff = x - y;

  grad                       = VectorXd::Zero(1 + lengthScales.rows());
  
  //first is the nugget, which has derivative 1 or 0
  if(MatrixEqual(x,y)){
  grad(0)                    = 1.0; //vary linearly with this term
  } else {
grad(0)                    = 0.0; //vary linearly with this term	  
  }
  
  grad.tail(lengthScales.rows()) =  Kernel(x, y) * diff.array().abs().pow(Power)*lengthScales.array().pow(-Power-1.0);
}

Eigen::VectorXd AnisotropicPowerKernelNugget::KernelSpatialDerivative(Eigen::VectorXd const& x, Eigen::VectorXd const& xWrt)
{
  VectorXd diff = x - xWrt;
  

  return Kernel(x, xWrt) * diff.array().abs().pow(Power-1.0)*lengthScales.array().pow(-Power);
}

void AnisotropicPowerKernelNugget::SetParms(const Eigen::VectorXd& theta)
{
  assert(theta.rows() == Nparams);
  
  nugget = theta(0);
  lengthScales = theta.tail(Nparams - 1);
}

Eigen::VectorXd AnisotropicPowerKernelNugget::GetParms()
{
  VectorXd result(Nparams);

  result << nugget, lengthScales;
  return result;
}

