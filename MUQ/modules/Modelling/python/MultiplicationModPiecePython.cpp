#include "MUQ/Modelling/python/MultiplicationModPiecePython.h"

using namespace std;
using namespace muq::Utilities;
using namespace muq::Modelling;

MultiplicationModPiecePython::MultiplicationModPiecePython(boost::python::list const& inputDims) :
  MultiplicationModPiece(GetEigenVector<Eigen::VectorXi>(inputDims)) {}

void muq::Modelling::ExportMultiplicationModPiece()
{
  boost::python::class_<MultiplicationModPiecePython, shared_ptr<MultiplicationModPiecePython>,
                        boost::python::bases<ModPiece>, boost::noncopyable> exportMult(
    "MultiplicationModPiece", boost::python::init<boost::python::list const&>());

  boost::python::implicitly_convertible<shared_ptr<MultiplicationModPiecePython>, shared_ptr<MultiplicationModPiece> >();
  boost::python::implicitly_convertible<shared_ptr<MultiplicationModPiecePython>, shared_ptr<ModPiece> >();
}

