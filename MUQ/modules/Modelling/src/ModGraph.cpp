
#include <boost/graph/graphviz.hpp> // This needs to be at the top of include list to avoid a weird boost-python on osx thing

#include "MUQ/Modelling/ModGraph.h"

// stadard template library includes
#include <algorithm>
#include <utility>
#include <fstream>
#include <iostream>
#include <typeinfo>
#include <cxxabi.h>

// boost graph library includes
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/depth_first_search.hpp>
#include <boost/graph/reverse_graph.hpp>
#include <boost/graph/iteration_macros.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/graph/copy.hpp>

// other boost includes
#include <boost/algorithm/string.hpp>

// other muq includes
#include "MUQ/Modelling/ModParameter.h"

#include "MUQ/Modelling/ModPiece.h"
#include "MUQ/Modelling/ModPieceDensity.h"

#include "MUQ/Modelling/ModGraphPiece.h"
#include <MUQ/Modelling/VectorPassthroughModel.h>

// namespace stuff
using namespace muq::Modelling;
using namespace std;


class nodeNameFinder {
public:

  nodeNameFinder(const std::string& nIn, const Graph *gIn) : myName(nIn), g(gIn) {}

  bool operator()(boost::graph_traits<Graph>::vertex_descriptor v)
  {
    return !myName.compare((*g)[v]->name);
  }

  std::string  myName;
  const Graph *g;
};

class nodePtrFinder {
public:

  nodePtrFinder(std::shared_ptr<ModPiece> ptrIn, const Graph *gIn) : ptr(ptrIn), g(gIn) {}

  bool operator()(boost::graph_traits<Graph>::vertex_descriptor v)
  {
    return ptr == ((*g)[v]->piece);
  }

  std::shared_ptr<ModPiece> ptr;
  const Graph *g;
};

ModGraph::ModGraph(const std::shared_ptr<ModPiece>& node)
{
  AddNode(node, node->GetName());
}

void ModGraph::print() const
{
  // first, list the vertices
  std::cout << "\nList of nodes:\n";
  boost::graph_traits<Graph>::vertex_iterator v, v_end;

  for (tie(v, v_end) = vertices(ModelGraph); v != v_end; v++) {
    std::cout << "\t" << ModelGraph[*v]->name << std::endl;
  }

  // now, list the edges
  std::cout << "\nList of edges:\n";
  boost::graph_traits<Graph>::edge_iterator e, e_end;
  for (tie(e, e_end) = edges(ModelGraph); e != e_end; e++) {
    std::cout << "\t" <<
      ModelGraph[source(*e,
                        ModelGraph)]->name << " -> " <<
      ModelGraph[target(*e, ModelGraph)]->name << "[" << ModelGraph[*e]->GetDim() << "]\n";
  }
  std::cout << "\n";
}

std::shared_ptr<ModPiece> ModGraph::GetNodeModel(const std::string& name)
{
  return ModelGraph[*GetNodeIterator(name)]->piece;
}

bool nodeInGraph(const std::string& name, const Graph& g)
{
  // try to find the node with this name
  boost::graph_traits<Graph>::vertex_iterator v, v_end;

  tie(v, v_end) = vertices(g);

  boost::graph_traits<Graph>::vertex_iterator iter = std::find_if(v, v_end, nodeNameFinder(name, &g));

  // make sure we actually found something
  return iter != vertices(g).second;
}

bool nodeInGraph(const std::string& name, const Graph& g, boost::graph_traits<Graph>::vertex_iterator& iter)
{
  // try to find the node with this name
  boost::graph_traits<Graph>::vertex_iterator v, v_end;

  tie(v, v_end) = vertices(g);

  iter = std::find_if(v, v_end, nodeNameFinder(name, &g));

  // make sure we actually found something
  return iter != vertices(g).second;
}

bool ModGraph::NodeExists(const std::string& name) const
{
  return nodeInGraph(name, ModelGraph);
}

void ModGraph::AddNode(std::shared_ptr<ModPiece> Input, const std::string& name)
{
  // check to see if any other nodes have this name
  assert(!NodeExists(name));

  // add a node to the graph
  auto x = add_vertex(ModelGraph);

  // set the node
  ModelGraph[x] = make_shared<ModGraphNode>(name, Input);
}

std::shared_ptr<ModGraph> ModGraph::DeepCopy()
{
  auto copy = make_shared<ModGraph>();
  boost::copy_graph(ModelGraph, copy->ModelGraph);

  return copy;
}

struct sameDim {
  sameDim(int dimIn, Graph *gIn) : dim(dimIn), g(gIn) {}

  bool operator()(boost::graph_traits<Graph>::edge_descriptor eIn)
  {
    return (*g)[eIn]->GetDim() == dim;
  }

  int    dim;
  Graph *g;
};

void ModGraph::RemoveNode(const std::string& nodeName)
{
  auto nodeDesc = GetNodeIterator(nodeName);

  clear_vertex(*nodeDesc, ModelGraph);  //remove edges to the node
  remove_vertex(*nodeDesc, ModelGraph); //then remove the node
}

void ModGraph::SwapNode(const std::string& currName, std::shared_ptr<ModPiece> Input, const std::string& newName)
{
  // first, get a descriptor to the current node
  auto nodeDesc = GetNodeIterator(currName);

  // check to make sure the new model is consistent with the old model
    assert(ModelGraph[*nodeDesc]->piece->outputSize == Input->outputSize);
    assert(ModelGraph[*nodeDesc]->piece->inputSizes.size() == Input->inputSizes.size());
  for (int i = 0; i < Input->inputSizes.size(); ++i) {
    assert(ModelGraph[*nodeDesc]->piece->inputSizes[i] == Input->inputSizes[i]);
  }

  // replace the old modpiece with the new modpiece
  ModelGraph[*nodeDesc]->piece = Input;

  // replace the name, if one was provided
  if (newName.length() > 0) {
    ModelGraph[*nodeDesc]->name = newName;
  }
}

void ModGraph::MoveInputEdges(const std::string& oldNode, const std::string& newNode)
{
    assert(NodeExists(oldNode));
    assert(NodeExists(newNode));

  // first, get a descriptor to the current node
  auto oldDesc = GetNodeIterator(oldNode);
  auto newDesc = GetNodeIterator(newNode);

  // check to make sure the new model is consistent with the old model
    assert(ModelGraph[*oldDesc]->piece->inputSizes.size() == ModelGraph[*newDesc]->piece->inputSizes.size());
  for (int i = 0; i < ModelGraph[*newDesc]->piece->inputSizes.size(); ++i) {
    assert(ModelGraph[*oldDesc]->piece->inputSizes[i] == ModelGraph[*newDesc]->piece->inputSizes[i]);
  }

  boost::graph_traits<Graph>::in_edge_iterator e, e_end;
  for (tie(e, e_end) = boost::in_edges(*oldDesc, ModelGraph); e != e_end; e++) {
    auto v = boost::source(*e, ModelGraph);
    AddEdge(ModelGraph[v]->name, newNode, ModelGraph[*e]->GetDim());
    RemoveEdge(ModelGraph[v]->name, oldNode, ModelGraph[*e]->GetDim());
  }
}

void ModGraph::MoveOutputEdges(const std::string& oldNode, const std::string& newNode)
{
  assert(NodeExists(oldNode));
  assert(NodeExists(newNode));
  auto oldDesc = GetNodeIterator(oldNode);
  auto newDesc = GetNodeIterator(newNode);

  assert(ModelGraph[*oldDesc]->piece->outputSize == ModelGraph[*newDesc]->piece->outputSize);

  boost::graph_traits<Graph>::out_edge_iterator e, e_end;
  for (tie(e, e_end) = boost::out_edges(*oldDesc, ModelGraph); e != e_end; e++) {
    auto v = boost::target(*e, ModelGraph);
    AddEdge(newNode, ModelGraph[v]->name, ModelGraph[*e]->GetDim());
    RemoveEdge(oldNode, ModelGraph[v]->name, ModelGraph[*e]->GetDim());
  }
}

void ModGraph::AddEdge(const std::string& nameFrom, const std::string& nameTo, int inputDim)
{
  auto fromDesc = GetNodeIterator(nameFrom);
  auto toDesc   = GetNodeIterator(nameTo);

  // make sure the input dimension is less than the number of inputs on the to node
  if(inputDim >= ModelGraph[*toDesc]->piece->inputSizes.size()){
	  std::cerr << "ERROR: Could not add edge from \"" << nameFrom << "\" to \"" << nameTo << "\" because the edge was assigned to input " << inputDim << " but only " << ModelGraph[*toDesc]->piece->inputSizes.size() << " inputs exist for the \"" << nameTo << "\" node.\n" << std::endl;
    assert(inputDim < ModelGraph[*toDesc]->piece->inputSizes.size());
  }
  
  if(ModelGraph[*fromDesc]->piece->outputSize != ModelGraph[*toDesc]->piece->inputSizes[inputDim]){
	  std::cerr << "ERROR: Could not add edge from \"" << nameFrom << "\" to \"" << nameTo << "\" because the output of \"" << nameFrom << "\" has " << ModelGraph[*fromDesc]->piece->outputSize << " components, but input " << inputDim << " of \"" << nameTo << "\" requires " << ModelGraph[*toDesc]->piece->inputSizes[inputDim] << " components.\n" << std::endl;
      assert(ModelGraph[*fromDesc]->piece->outputSize == ModelGraph[*toDesc]->piece->inputSizes[inputDim]);	
  }
  

  boost::graph_traits<Graph>::edge_iterator e, e_end;

  // remove any other edge going into dimension inputDim of the nameTo node
  boost::remove_in_edge_if(*toDesc, sameDim(inputDim, &ModelGraph), ModelGraph);

  // try to add the new edge, if an edge already exists, notFound will be false and we need to delete the current edge
  // first
  auto temp = boost::add_edge(*fromDesc, *toDesc, ModelGraph);

  // set the edge to have the current dimension
  ModelGraph[temp.first] = std::make_shared<ModGraphEdge>(inputDim);
}

class edgeMatcher {
public:

  edgeMatcher(const std::string& inName, int inputDim, Graph *gIn) : name(inName), dim(inputDim), g(gIn) {}

  bool operator()(boost::graph_traits<Graph>::edge_descriptor e)
  {
    return (!(*g)[boost::source(e, *g)]->name.compare(name)) && ((*g)[e]->GetDim() == dim);
  }

  string name;
  int    dim;
  Graph *g;
};

void ModGraph::RemoveEdge(const std::string& nameFrom, const std::string& nameTo, int inputDim)
{
  auto toDesc = GetNodeIterator(nameTo);

  boost::remove_in_edge_if(*toDesc, edgeMatcher(nameFrom, inputDim, &ModelGraph), ModelGraph);
}

boost::graph_traits<Graph>::vertex_iterator ModGraph::GetNodeIterator(const std::string& name) const
{
  // try to find the node with this name
  boost::graph_traits<Graph>::vertex_iterator v, v_end;

  tie(v, v_end) = vertices(ModelGraph);

  boost::graph_traits<Graph>::vertex_iterator iter = std::find_if(v, v_end, nodeNameFinder(name, &ModelGraph));

  // make sure we actually found something
  if (iter == vertices(ModelGraph).second) {
    std::cerr <<
      "ERROR: IN \"ModGraph::GetNodeIterator(const std::string &name) const\", could not find a node in the graph named "
              << name << "\n";
    assert(iter != vertices(ModelGraph).second);
  }
  return iter;
}

boost::graph_traits<Graph>::vertex_iterator ModGraph::GetNodeIterator(std::shared_ptr<ModPiece> ptr) const
{
  // try to find the node with this name
  boost::graph_traits<Graph>::vertex_iterator v, v_end;

  tie(v, v_end) = vertices(ModelGraph);

  boost::graph_traits<Graph>::vertex_iterator iter = std::find_if(v, v_end, nodePtrFinder(ptr, &ModelGraph));

  // make sure we actually found something
  if (iter == vertices(ModelGraph).second) {
    std::cerr <<
      "ERROR: IN \"ModGraph::GetNodeIterator(std::shared_ptr<ModPiece> ptr) const\", could not find a node in the graph using this pointer.\n";
    assert(iter != vertices(ModelGraph).second);
  }

  return iter;
}

struct TrueOp {
  template<typename T>
  bool operator()(const T& in)
  {
    return true;
  }
};

void ModGraph::BindNode(const std::string& nodeName, const Eigen::VectorXd& x)
{
  // find the node
  auto nodeDesc = GetNodeIterator(nodeName);

  // check that output dimensions agree
    assert(ModelGraph[*nodeDesc]->piece->outputSize == x.size());

  // next, delete all incoming edges
  boost::remove_in_edge_if(*nodeDesc, TrueOp(), ModelGraph);

  // finally, replace the ModPiece ptr

  ModelGraph[*nodeDesc]->piece = make_shared<ModParameter>(x);
}

void ModGraph::BindEdge(const std::string& nodeName, int inputDim, const Eigen::VectorXd& x)
{
  auto nodeDesc = GetNodeIterator(nodeName);

  // iterate through the input edges to determine if this edge already exists, remove it if it does
  boost::graph_traits<Graph>::in_edge_iterator e, e_end;

  for (tie(e, e_end) = boost::in_edges(*nodeDesc, ModelGraph); e != e_end; e++) {
    if (ModelGraph[*e]->GetDim() == inputDim) {
      boost::remove_edge(*e, ModelGraph);
      break;
    }
  }

  // add a node to the graph
  auto v = boost::add_vertex(ModelGraph);

  // set the node
  ModelGraph[v] =
    make_shared<ModGraphNode>(nodeName + "_FixedInput" + std::to_string(inputDim), make_shared<ModParameter>(x));

  // add a new edge
  auto temp = boost::add_edge(v, *nodeDesc, ModelGraph);
  ModelGraph[temp.first] = std::make_shared<ModGraphEdge>(inputDim);
}

std::shared_ptr<muq::Modelling::ModGraph> ModGraph::DependentCut(const std::string& nameOut) const
{
  // copy everything upstream of this node to a new graph and trim constant branches to a single ModParameter
  auto newGraph = std::make_shared<ModGraph>();

  auto oldV = GetNodeIterator(nameOut);

  // add a new node to the output graph to hold the output node
  auto newV = boost::add_vertex(newGraph->ModelGraph);

  // copy the output node
  newGraph->ModelGraph[newV] = ModelGraph[*oldV];

  // recurse through graph
  RecursiveCopyAndCut(*oldV, newV, newGraph);

  // return result
  return newGraph;
}

std::shared_ptr<muq::Modelling::ModGraph> ModGraph::ReplaceNodeWithInput(const std::string& inputName,
                                                                         const std::string& outputName)
{
  assert(NodeExists(inputName));
  assert(NodeExists(outputName));
  //start by making a copy for us to work on
  std::shared_ptr<muq::Modelling::ModGraph> tmpGraph = DependentCut(outputName);
  auto nodeToRemove                                  = tmpGraph->GetNodeIterator(inputName);

  int oldNodeSize = GetNodeModel(inputName)->outputSize;


  boost::graph_traits<Graph>::out_edge_iterator e, e_end;

  //a list of targets that recieve from the node we're removing
  std::list < std::tuple < std::string, int >> outEdges;
  for (tie(e, e_end) = boost::out_edges(*nodeToRemove, tmpGraph->ModelGraph); e != e_end; e++) {
    auto v = boost::target(*e, tmpGraph->ModelGraph);
    outEdges.push_back(std::tuple<std::string, int>(tmpGraph->ModelGraph[v]->name, tmpGraph->ModelGraph[*e]->GetDim()));
  }

  //cut out the node
  tmpGraph->RemoveNode(inputName);
  //add it's replacement
  tmpGraph->AddNode(make_shared<VectorPassthroughModel>(oldNodeSize), inputName);
  //replace all the edges with the input
  int dim;
  std::string targetName;
  for (auto anEdge : outEdges) {
    tie(targetName, dim) = anEdge;
    tmpGraph->AddEdge(inputName, targetName, dim);
  }

  //and trim away anything not used anymore
  return tmpGraph->DependentCut(outputName);
}
bool EdgeExists(const boost::graph_traits<Graph>::vertex_descriptor& vOld,
                                   const boost::graph_traits<Graph>::vertex_descriptor& vNew, int dim, Graph &graph)
{
boost::graph_traits<Graph>::out_edge_iterator ei, ei_end;
boost::tie(ei, ei_end) = boost::out_edges( vOld, graph );
int parallel_count = 0;
for( ; ei != ei_end; ++ei) {
  if( target(*ei, graph) == vNew &&  graph[*ei]->GetDim()  == dim) {
	  return true;
  }
}
return false;
}

void ModGraph::RecursiveCopyAndCut(const boost::graph_traits<Graph>::vertex_descriptor& vOld,
                                   const boost::graph_traits<Graph>::vertex_descriptor& vNew,
                                   std::shared_ptr<ModGraph>                            gNew) const
{
  // at this point, vOld is a node in ModelGraph that corresponds to the recently added vNew node in gNew

  // loop through the inputs to vOld
  boost::graph_traits<Graph>::in_edge_iterator e, e_end;

  for (tie(e, e_end) = in_edges(vOld, ModelGraph); e != e_end; e++) {
    auto v = boost::source(*e, ModelGraph);
    if (isConstant(v)) {
      // create a new constant ModParameter for this input
      auto nextV          = boost::add_vertex(gNew->ModelGraph);
      Eigen::VectorXd val = GetConstantVal(v);

      gNew->ModelGraph[nextV] =
        make_shared<ModGraphNode>(ModelGraph[v]->name + "_fixed", make_shared<ModParameter>(val));

      // add the edge from this node to the existing node, if it does not already exist
      if(!EdgeExists(nextV, vNew, ModelGraph[*e]->GetDim(), gNew->ModelGraph)){
        auto nextE = boost::add_edge(nextV, vNew, gNew->ModelGraph);
        gNew->ModelGraph[nextE.first] = make_shared<ModGraphEdge>(ModelGraph[*e]->GetDim());
	  }
    } else {
      boost::graph_traits<Graph>::vertex_descriptor nextV;
      boost::graph_traits<Graph>::vertex_iterator   ind;

      // check to see if the node already exists in the new graph
      if (nodeInGraph(ModelGraph[v]->name, gNew->ModelGraph, ind)) {
        nextV = *ind;

        // if not, add the node
      } else {
        // copy the source node over to the new graph and make an edge
        nextV                   = boost::add_vertex(gNew->ModelGraph);
        gNew->ModelGraph[nextV] = ModelGraph[v];
      }


      // add the edge from this node to the existing node, if the edge does not already exist
      if(!EdgeExists(nextV, vNew, ModelGraph[*e]->GetDim(), gNew->ModelGraph)){
        auto nextE = boost::add_edge(nextV, vNew, gNew->ModelGraph);
        gNew->ModelGraph[nextE.first] = make_shared<ModGraphEdge>(ModelGraph[*e]->GetDim());
	  }
	  
      // recurse down a step further
      RecursiveCopyAndCut(v, nextV, gNew);
    }
  }
}

bool ModGraph::isConstant(boost::graph_traits<Graph>::vertex_descriptor v) const
{
  // check to see if this is a pointer to a ModParameter
  if (dynamic_pointer_cast<ModParameter>(ModelGraph[v]->piece) != nullptr) {
    return true;

    // now check to see if this is a pointer to a Random Variable
  } else if (dynamic_pointer_cast<RandVar>(ModelGraph[v]->piece) != nullptr) {
    return false;

    // check to see if all the inputs to this piece have edges
  } else if (static_cast<int>(in_degree(v, ModelGraph)) != ModelGraph[v]->piece->inputSizes.size()) {
    return false;

    // if all the inputs are set, recursively check for constantness
  } else {
    // get iterators to the input edge list
    boost::graph_traits<Graph>::in_edge_iterator e, e_end;
    bool c = true;
    for (tie(e, e_end) = in_edges(v, ModelGraph); e != e_end; e++) {
      c = c && isConstant(boost::source(*e, ModelGraph));
    }
    return c;
  }
}

bool ModGraph::isConstant(const std::string& name)
{
  return isConstant(*GetNodeIterator(name));
}

Eigen::VectorXd ModGraph::GetConstantVal(const std::string& name) const
{
  return GetConstantVal(*GetNodeIterator(name));
}

Eigen::VectorXd ModGraph::GetConstantVal(boost::graph_traits<Graph>::vertex_descriptor v) const
{
  // first, make sure this is a constant node
  assert(isConstant(v));

  // check to see if this is a pointer to a ModParameter
  auto temp = dynamic_pointer_cast<ModParameter>(ModelGraph[v]->piece);
  if (temp != nullptr) {
    return temp->Evaluate();

    // if this is not a ModParameter, get the input values and evaluate the ModPiece
  } else {
    std::vector<Eigen::VectorXd> inputVecs(ModelGraph[v]->piece->inputSizes.size());

    // get iterators to the input edge list
    boost::graph_traits<Graph>::in_edge_iterator e, e_end;
    for (tie(e, e_end) = in_edges(v, ModelGraph); e != e_end; e++) {
      inputVecs[ModelGraph[*e]->GetDim()] = GetConstantVal(source(*e, ModelGraph));
    }

    return ModelGraph[v]->piece->Evaluate(inputVecs);
  }
}

int ModGraph::NumInputs() const
{
  // loop through the vertices and count the total number of inputs
  int allIns = 0;
  boost::graph_traits<Graph>::vertex_iterator v, v_end;

  for (tie(v, v_end) = vertices(ModelGraph); v != v_end; v++) {
    allIns += ModelGraph[*v]->piece->inputSizes.size();
  }

  // how many edges have been set?
  int setIns = boost::num_edges(ModelGraph);

  return allIns - setIns;
}

Eigen::VectorXi ModGraph::inputSizes() const
{
  // first, get a list of all the inputs
  auto inputVec = GraphInputs();

  // allocate a vector to hold the input sizes
  Eigen::VectorXi output(inputVec.size());

  // fill in the size vector with the correct input size
  for (unsigned int i = 0; i < inputVec.size(); ++i) {
    output[i] = ModelGraph[inputVec[i].first]->piece->inputSizes[inputVec[i].second];
  }

  return output;
}

void ModGraph::printInputs() const
{
  // first, get a list of all the inputs
  auto inputVec = GraphInputs();

  std::cout << "Hanging inputs:\n";

  // fill in the size vector with the correct input size
  for (unsigned int i = 0; i < inputVec.size(); ++i) {
    std::cout << "\t" << ModelGraph[inputVec[i].first]->name << "[" << inputVec[i].second << "] of size " <<
      ModelGraph[inputVec[i].first]->piece->inputSizes[inputVec[i].second] << "\n";
  }
}

std::vector<std::string> ModGraph::GetInputNames() const
{
  // first, get a list of all the inputs
  auto inputVec = GraphInputs();

  std::vector<std::string> result(inputVec.size());

  for (unsigned int i = 0; i < inputVec.size(); ++i) {
    result.at(i) =  ModelGraph[inputVec[i].first]->name;
  }

  return result;
}

std::vector < std::pair < boost::graph_traits<Graph>::vertex_descriptor, int >> ModGraph::GraphInputs() const {
  // create an empty vector to hold output
  std::vector < std::pair < boost::graph_traits<Graph>::vertex_descriptor, int >> output;

  // loop through the vertices
  boost::graph_traits<Graph>::vertex_iterator v, v_end;
  for (tie(v, v_end) = vertices(ModelGraph); v != v_end; v++) {
    // for each vertex, loop over the input nodes and figure out inputs are set
    std::vector<bool> isSet(ModelGraph[*v]->piece->inputSizes.size(), false);
    boost::graph_traits<Graph>::in_edge_iterator e, e_end;
    for (tie(e, e_end) = in_edges(*v, ModelGraph); e != e_end; e++) {
      isSet[ModelGraph[*e]->GetDim()] = true;
    }

    // if an input to this ModPiece is not set, it will be stored as an input to the Graph
    for (unsigned int i = 0; i < isSet.size(); ++i) {
      if (!isSet[i]) {
        output.push_back(std::make_pair(*v, i));
      }
    }
  }

  return output;
}

int ModGraph::NumOutputs() const
{
  // loop over all the vertices to see which ones have output edges
  int noOuts = 0;
  boost::graph_traits<Graph>::vertex_iterator v, v_end;

  for (tie(v, v_end) = vertices(ModelGraph); v != v_end; v++) {
    auto e = boost::out_edges(*v, ModelGraph);
    if (e.first == e.second) {
      noOuts++;
    }
  }
  return noOuts;
}

std::vector<std::string> ModGraph::GetOutputNames() const
{
  // loop over all the vertices to see which ones have output edges
  std::vector<std::string> result;

  boost::graph_traits<Graph>::vertex_iterator v, v_end;

  for (tie(v, v_end) = vertices(ModelGraph); v != v_end; v++) {
    auto e = boost::out_edges(*v, ModelGraph);
    if (e.first == e.second) {
      result.push_back(ModelGraph[*v]->name);
    }
  }
  return result;
}

std::string GetDerivColor(std::shared_ptr<ModPiece> modPtr){
	
	std::string color;
		
	const std::string baseScheme = "colorscheme=pastel16,";
	if(!modPtr->hasDirectGradient){
		color = baseScheme + "color=3";
	}else if(!modPtr->hasDirectJacobian){
		color = baseScheme + "color=4";
	}else if(!modPtr->hasDirectJacobianAction){
		color = baseScheme + "color=5";
	}else if(!modPtr->hasDirectHessian){
		color = baseScheme + "color=6";
	}else{
		color = baseScheme + "color=2";
	}

	
	return color;
}

std::shared_ptr<ModGraph> ModGraph::FormUnion(std::shared_ptr<ModGraph> a, std::shared_ptr<ModGraph> b)
{
  auto result = make_shared<ModGraph>(); //create the union graph

  //loop over a, make equivalent nodes
  BGL_FORALL_VERTICES(v, a->ModelGraph, Graph)
  {
    auto currentName = a->ModelGraph[v]->name;

    //check if the name exists in the merged graph
    if (!result->NodeExists(currentName)) {
      //we're safe, so copy the node
      result->AddNode(a->ModelGraph[v]->piece, a->ModelGraph[v]->name);
    } else {
      //not going to copy a duplicate, so make sure the existing copy is of the same thing
      assert(result->GetNodeModel(currentName) == a->ModelGraph[v]->piece);
    }
  }

  //loop over b, make equivalent nodes
  BGL_FORALL_VERTICES(v, b->ModelGraph, Graph)
  {
    auto currentName = b->ModelGraph[v]->name;

    //check if the name exists in the merged graph
    if (!result->NodeExists(currentName)) {
      //we're safe, so copy the node
      result->AddNode(b->ModelGraph[v]->piece, b->ModelGraph[v]->name);
    } else {
      //not going to copy a duplicate, so make sure the existing copy is of the same thing
      assert(result->GetNodeModel(currentName) == b->ModelGraph[v]->piece);
    }
  }

  //move a edges, we can move them without any further checks, since we know all the names are unique
  BGL_FORALL_EDGES(e, a->ModelGraph, Graph)
  {
    auto s = boost::source(e, a->ModelGraph);
    auto t = boost::target(e, a->ModelGraph);

    //no edge should displace another, so ensure that the input port this edge is going to use is open in the result
    // graph
    boost::graph_traits<Graph>::in_edge_iterator e_result, e_result_end;

    //loop over edges going into the target, assert there is no collision
    for (tie(e_result, e_result_end) =
           boost::in_edges(*result->GetNodeIterator(a->ModelGraph[t]->piece), result->ModelGraph);
         e_result != e_result_end; e_result++) {
      assert(result->ModelGraph[*e_result]->GetDim() != a->ModelGraph[e]->GetDim());
    }

    //go ahead and add the edge based on the names and dim
    result->AddEdge(a->ModelGraph[s]->name, a->ModelGraph[t]->name, a->ModelGraph[e]->GetDim());
  }

  //move a edges, we can move them without any further checks, since we know all the names are unique
  BGL_FORALL_EDGES(e, b->ModelGraph, Graph)
  {
    auto s = boost::source(e, b->ModelGraph);
    auto t = boost::target(e, b->ModelGraph);

    //no edge should displace another, so ensure that the input port this edge is going to use is open in the result
    // graph
    boost::graph_traits<Graph>::in_edge_iterator e_result, e_result_end;

    //loop over edges going into the target, assert there is no collision
    for (tie(e_result, e_result_end) =
           boost::in_edges(*result->GetNodeIterator(b->ModelGraph[t]->piece), result->ModelGraph);
         e_result != e_result_end; e_result++) {
      assert(result->ModelGraph[*e_result]->GetDim() != b->ModelGraph[e]->GetDim());
    }


    result->AddEdge(b->ModelGraph[s]->name, b->ModelGraph[t]->name, b->ModelGraph[e]->GetDim());
  }

  return result;
}

std::string GetRangeColor(double currVal, double minVal, double maxVal)
{
  if (minVal == maxVal) {
    return "colorscheme=ylorrd8,color=4";
  } else {
    assert(minVal < maxVal);

    double scaledVal  = (currVal - minVal) / (maxVal - minVal);
    std::string color = "colorscheme=ylorrd8,";
    if (scaledVal < 0.0) {
      color = "colorscheme=greys9,color=2";
    } else if (scaledVal < 1.0 / 8.0) {
      color += "color=1";
    } else if (scaledVal < 2.0 / 8.0) {
      color += "color=2";
    } else if (scaledVal < 3.0 / 8.0) {
      color += "color=3";
    } else if (scaledVal < 4.0 / 8.0) {
      color += "color=4";
    } else if (scaledVal < 5.0 / 8.0) {
      color += "color=5";
    } else if (scaledVal < 6.0 / 8.0) {
      color += "color=6";
    } else if (scaledVal < 7.0 / 8.0) {
      color += "color=7";
    } else {
      color += "color=8";
    }
    return color;
}
}

class MyVertexWriter {
public:

  MyVertexWriter(const Graph *gIn, ModGraph::colorOptionsType colorOptionIn = ModGraph::DEFAULT_COLOR, bool addDerivLabelsIn=false) : colorOption(colorOptionIn), addDerivLabels(addDerivLabelsIn), g(gIn)
  {
	  if(colorOption==ModGraph::CALLS_COLOR){
		  
		  // loop over the entire graph and store all the number of calls
		  int numNodes = boost::num_vertices(*gIn);
		  Eigen::Matrix<unsigned long int, Eigen::Dynamic,1> calls(numNodes);
		  int ind = 0;
		  BGL_FORALL_VERTICES(v, *gIn, Graph)
		  {
		    calls(ind) = (*g)[v]->piece->GetNumCalls("Evaluate");
			ind++;
		  }
		  

		  maxRange = static_cast<double>(calls.maxCoeff());
		  // find the minimum value greater than 0
		  minRange = maxRange;
		  for(int i=0; i<numNodes; ++i){
			  if((calls(i)!=0)&&(calls(i)<minRange))
				  minRange = calls(i);
			  
		  }
		  
	  }else if(colorOption==ModGraph::TIME_COLOR){
		  
		  // loop over the entire graph and store all the times
		  int numNodes = boost::num_vertices(*gIn);
		  Eigen::VectorXd times(numNodes);
		  int ind = 0;
		  BGL_FORALL_VERTICES(v, *gIn, Graph)
		  {
		    times(ind) = (*g)[v]->piece->GetRunTime("Evaluate");
			ind++;
		  }
		  
		  maxRange = times.maxCoeff();
		  minRange = maxRange;
		  for(int i=0; i<numNodes; ++i){
			  if((times(i)!=-1.0)&&(times(i)<minRange))
				  minRange = times(i);
			  
		  }
		  
	  }
  }

  void operator()(std::ostream& out, const boost::graph_traits<Graph>::vertex_descriptor& v) const
  {
    int   status;
	auto modPtr = (*g)[v]->piece;
	const string realString = modPtr->GetName();

    
	
    string style;
    if ((strcmp(realString.c_str(), "muq::Modelling::ModParameter") == 0) && (((*g)[v]->name).find("InputNode") == 0)) {
      style =  "shape=invhouse,colorscheme=pastel16,color=1, style=filled";
    } else {
		 if(colorOption==ModGraph::DERIV_COLOR){
 			style = GetDerivColor(modPtr) + ", style=filled";
 		}else if(colorOption==ModGraph::CALLS_COLOR){
 			style = GetRangeColor(modPtr->GetNumCalls("Evaluate"),minRange,maxRange) + ", style=filled";
	 	}else if(colorOption==ModGraph::TIME_COLOR){
	 		style = GetRangeColor(modPtr->GetRunTime("Evaluate"),minRange,maxRange) + ", style=filled";
 		}else{
			style = "colorscheme=pastel16,color=2, style=filled";
		}
		
		if(addDerivLabels){
		
		  string derivs = "0000";
		  if(modPtr->hasDirectGradient)
		    derivs[0] = '1';
		  if(modPtr->hasDirectJacobian)
		    derivs[1] = '1';
		  if(modPtr->hasDirectJacobianAction)
		    derivs[2] = '1';
		  if(modPtr->hasDirectHessian)
		    derivs[3] = '1';
		  
	      out << "[label=\"" << (*g)[v]->name << " : " << realString << " (" << derivs << ")\", " << style << "]";
		}else{
		  out << "[label=\"" << (*g)[v]->name << " : " << realString << "\", " << style << "]";
		}
    }

    
  }

private:
  double minRange, maxRange;
  
  ModGraph::colorOptionsType colorOption;
  bool addDerivLabels;
  const Graph *g;
};

class MyEdgeWriter {
public:

  MyEdgeWriter(const Graph *gIn) : g(gIn) {}

  void operator()(std::ostream& out, const boost::graph_traits<Graph>::edge_descriptor& e) const
  {
    int inputNum  = (*g)[e]->GetDim();
    int inputSize = (*g)[e.m_target]->piece->inputSizes(inputNum);

    // first, write the name as the label
    out << "[label=\" " << inputNum << "[" << inputSize << "]\"]";
  }

private:

  const Graph *g;
};

class MyGraphWriter {
public:

  MyGraphWriter(const Graph *gIn) : g(gIn) {}

  void operator()(std::ostream& out) const
  {
    out << "splines = true;" << endl;
  }

private:

  const Graph *g;
};

void ModGraph::writeGraphViz(const std::string& filename, colorOptionsType colorOption, bool addDerivLabel) const
{
  std::vector<std::string> strs;
  boost::split(strs, filename, boost::is_any_of("."));

  bool runningGraphviz = (strs.end() - 1)->compare("png") || (strs.end() - 1)->compare("jpg") ||
                         (strs.end() - 1)->compare("tif") || (strs.end() - 1)->compare("eps") ||
                         (strs.end() - 1)->compare("pdf") ||
                         (strs.end() - 1)->compare("svg");

  ofstream fout;

  if (runningGraphviz) {
    fout.open("temp.dot");
  } else {
    fout.open(filename.c_str());
  }

  typedef map<boost::graph_traits<Graph>::vertex_descriptor, size_t> IndexMap;
  IndexMap mapIndex;
  boost::associative_property_map<IndexMap> propmapIndex(mapIndex);


  int vertexNum = 0;
  BGL_FORALL_VERTICES(v, ModelGraph, Graph)
  {
    put(propmapIndex, v, vertexNum++);
  }


  boost::write_graphviz(fout, ModelGraph, MyVertexWriter(&ModelGraph,colorOption,addDerivLabel), MyEdgeWriter(&ModelGraph),
                        MyGraphWriter(&ModelGraph), propmapIndex);

  fout.seekp(-2, ios_base::cur); //back up so the rest is inside the brackets

  //loop over all the inputs and draw them
  std::vector < std::pair < boost::graph_traits<Graph>::vertex_descriptor, int >> graphInputs = GraphInputs();

  int i = 0;
  for (auto aPair : graphInputs) {
    vertexNum++;
    fout << vertexNum << "[label=\"Input #" << i << "\", shape=invhouse,colorscheme=pastel13,color=1, style=filled];" <<
      endl;
    fout << vertexNum << "->" << propmapIndex[aPair.first] << "[label=\" " << aPair.second << "[" <<
      ModelGraph[aPair.first]->piece->inputSizes(aPair.second) << "]\"];" << endl;
    i++;
  }


  //find all the nodes with no outputs
  BGL_FORALL_VERTICES(v, ModelGraph, Graph)
  {
    if (boost::out_degree(v, ModelGraph) == 0) {
      vertexNum++;
      fout << vertexNum << "[label=\"Output" <<   "\", shape=box,colorscheme=pastel16,color=1, style=filled];" << endl;
      fout << propmapIndex[v]  << "->" << vertexNum << "[label=\" [" << ModelGraph[v]->piece->outputSize << "]\"];" <<
        endl;
    }
  }


  fout << "}" << endl;

  // close the file stream
  fout.close();


  if (runningGraphviz) {
    // run dot
    assert(system(("dot -T" + *(strs.end() - 1) + " temp.dot -o " + filename).c_str()) >= 0);

    // remove the dot file
    //assert(system("rm temp.dot") >= 0);
  }
}

bool ModGraph::allDirectGradient() const
{
  boost::graph_traits<Graph>::vertex_iterator v, v_end;

  for (tie(v, v_end) = vertices(ModelGraph); v != v_end; v++) {
    if (!ModelGraph[*v]->piece->hasDirectGradient) {
      return false;
    }
  }

  return true;
}

bool ModGraph::allDirectJacobian() const
{
  boost::graph_traits<Graph>::vertex_iterator v, v_end;

  for (tie(v, v_end) = vertices(ModelGraph); v != v_end; v++) {
    if (!ModelGraph[*v]->piece->hasDirectJacobian) {
      return false;
    }
  }

  return true;
}

bool ModGraph::allDirectJacobianAction() const
{
  boost::graph_traits<Graph>::vertex_iterator v, v_end;

  for (tie(v, v_end) = vertices(ModelGraph); v != v_end; v++) {
    if (!ModelGraph[*v]->piece->hasDirectJacobianAction) {
      return false;
    }
  }

  return true;
}

bool ModGraph::allDirectHessian() const
{
  boost::graph_traits<Graph>::vertex_iterator v, v_end;

  for (tie(v, v_end) = vertices(ModelGraph); v != v_end; v++) {
    if (!ModelGraph[*v]->piece->hasDirectHessian) {
      return false;
    }
  }

  return true;
}

bool ModGraph::anyRandom() const
{
  boost::graph_traits<Graph>::vertex_iterator v, v_end;

  for (tie(v, v_end) = vertices(ModelGraph); v != v_end; v++) {
    if (ModelGraph[*v]->piece->isRandom) {
      return true;
    }
  }

  return false;
}

int ModGraph::outputSize(const std::string& nodeName) const
{
  if (nodeName.length() == 0) {
    // make sure there is only one output in this graph
    assert(NumOutputs() == 1);

    // now, find the output vertex
    // loop over all the vertices to see which ones have output edges
    boost::graph_traits<Graph>::vertex_iterator v, v_end;
    for (tie(v, v_end) = vertices(ModelGraph); v != v_end; v++) {
      auto e = boost::out_edges(*v, ModelGraph);
      if (e.first == e.second) {
        return outputSize(*v);
      }
    }

    return 0;
  } else {
    // return the output size for nodeName
    return outputSize(*GetNodeIterator(nodeName));
  }
}

int ModGraph::outputSize(const boost::graph_traits<Graph>::vertex_descriptor& v) const
{
  return ModelGraph[v]->piece->outputSize;
}

std::string ModGraph::GetOutputNodeName() const
{
  // make sure there is only one output in this graph
    assert(NumOutputs() == 1);

  // now, find the output vertex
  // loop over all the vertices to see which ones have output edges
  boost::graph_traits<Graph>::vertex_iterator v, v_end;
  for (tie(v, v_end) = vertices(ModelGraph); v != v_end; v++) {
    auto e = boost::out_edges(*v, ModelGraph);
    if (e.first == e.second) {
      return ModelGraph[*v]->name;
    }
  }

    assert(false && "No output found!");
}

void ModGraph::SetOutputNodeName(std::string const& newName)
{
  //this code is basically just GetOutputNodeName()

  // make sure there is only one output in this graph
    assert(NumOutputs() == 1);

  // now, find the output vertex
  // loop over all the vertices to see which ones have output edges
  boost::graph_traits<Graph>::vertex_iterator v, v_end;
  for (tie(v, v_end) = vertices(ModelGraph); v != v_end; v++) {
    auto e = boost::out_edges(*v, ModelGraph);
    if (e.first == e.second) {
      ModelGraph[*v]->name = newName;
      return;
    }
  }

    assert(false && "No output found!");
}

// are two nodes in the graph connected?  i.e. will information from v1 flow to v2?
bool ModGraph::NodesConnected(const std::string& name1, const std::string& name2) const
{
  return NodesConnected(*GetNodeIterator(name1), *GetNodeIterator(name2));
}

// are two nodes in the graph connected?  i.e. will information from v1 flow to v2?
bool ModGraph::NodesConnected(const boost::graph_traits<Graph>::vertex_descriptor& v1,
                              const boost::graph_traits<Graph>::vertex_descriptor& v2) const
{
  // if the nodes are the same, return true
  if (v1 == v2) {
    return true;

    // if the nodes are different check downstream
  } else {
    boost::graph_traits<Graph>::out_edge_iterator e, e_end;
    for (tie(e, e_end) = boost::out_edges(v1, ModelGraph); e != e_end; e++) {
      if (NodesConnected(target(*e, ModelGraph), v2)) {
        return true;
      }
    }
    return false;
  }
}

std::shared_ptr<ModGraphPiece> ModGraph::ConstructModPiece(std::shared_ptr<ModGraph> graph, const std::string& outNode)
{
  return ModGraphPiece::Create(graph, outNode);
}



std::shared_ptr<ModGraphPiece> ModGraph::ConstructModPiece(std::shared_ptr<ModGraph>       graph,
                                                           const std::string             & outNode,
                                                           const std::vector<std::string>& inputOrder)
{
  return ModGraphPiece::Create(graph, outNode, inputOrder);
}

std::shared_ptr<ModPieceDensity> ModGraph::ConstructDensity(std::shared_ptr<ModGraph> graph, const std::string& outNode)
{
  auto ptr = ModGraphPiece::Create(graph, outNode);

    assert(ptr);
  return std::make_shared<ModPieceDensity>(ptr);
}

std::shared_ptr<ModPieceDensity> ModGraph::ConstructDensity(std::shared_ptr<ModGraph>       graph,
                                                            const std::string             & outNode,
                                                            const std::vector<std::string>& inputOrder)
{
  auto ptr = ModGraphPiece::Create(graph, outNode, inputOrder);

    assert(ptr);
  return std::make_shared<ModPieceDensity>(ptr);
}

vector<string> ModGraph::InputNames(string const& outputName) const
{
  if (outputName.compare("") == 0) {
    vector<pair<boost::graph_traits<Graph>::vertex_descriptor, int> > inputPts = GraphInputs();

    // build the input node names
    vector<string> inNodeNames(inputPts.size());

    for (unsigned int i = 0; i < inputPts.size(); ++i) {
      stringstream temp;
      temp << ModelGraph[inputPts[i].first]->name;
      inNodeNames[i] = temp.str();
    }

    return inNodeNames;
  }

  return DependentCut(outputName)->InputNames();
}

