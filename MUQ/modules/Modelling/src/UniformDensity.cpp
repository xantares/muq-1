
#include "MUQ/Modelling/UniformDensity.h"

#include <iostream>
#include <limits>

using namespace muq::Modelling;
using namespace std;

double UniformDensity::LogDensityImpl(std::vector<Eigen::VectorXd> const& input)
{
  return specification->Density(input.at(0));
}
