
#include "MUQ/Modelling/UniformRandVar.h"


using namespace muq::Modelling;
using namespace std;

Eigen::VectorXd UniformRandVar::EvaluateImpl(std::vector<Eigen::VectorXd> const& input)
{
  return specification->Sample();
}

