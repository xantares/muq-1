import unittest
from numpy import * # math stuff in python

import libmuqModelling

class BasicComponentModel(libmuqModelling.ComponentwiseModel):
    def __init__(self, dim):
        libmuqModelling.ComponentwiseModel.__init__(self, dim)
        
    def BaseFunc(self, x):
        return pow(x, 2.0) + 2.0

    def BaseDeriv(self, x):
        return 2.0*x

    def BaseSecondDeriv(self, x):
        return 2.0

class ComponentwiseModelTest(unittest.TestCase):
    def testBasicTest(self):
        mod = BasicComponentModel(2)

        x = [1.0, 2.0]
        s = [1.0, 1.0]
        y    = mod.Evaluate(x)
        grad = mod.Gradient(x, s)
        J    = mod.Jacobian(x)
        Jact = mod.JacobianAction(x, s)
        hess = mod.Hessian(x, s)

        for i in range(2):
            self.assertEqual(y[i], pow(x[i], 2.0) + 2.0)
            self.assertEqual(grad[i], 2.0*x[i])
            self.assertEqual(J[i][i], 2.0*x[i])
            self.assertEqual(Jact[i], 2.0*x[i])
            self.assertEqual(hess[i][i], 2.0)
            for j in range(2):
                if not i==j:
                    self.assertEqual(J[i][j], 0.0)
                    self.assertEqual(hess[i][j], 0.0)

    def testExpModel(self):
        mod = libmuqModelling.ExpModel(2)

        x = [1.0, 2.0]
        y =  mod.Evaluate(x)
        
        for i in range(2):
            self.assertEqual(y[i], exp(x[i]))

    def testSinModel(self):
        mod = libmuqModelling.SinModel(2)

        x = [1.0, 2.0]
        y =  mod.Evaluate(x)

        for i in range(2):
            self.assertEqual(y[i], sin(x[i]))

    def testCosModel(self):
        mod = libmuqModelling.CosModel(2)

        x = [1.0, 2.0]
        y =  mod.Evaluate(x)

        for i in range(2):
            self.assertEqual(y[i], cos(x[i]))

    def testTanModel(self):
        mod = libmuqModelling.TanModel(2)

        x = [1.0, 2.0]
        y =  mod.Evaluate(x)

        for i in range(2):
            self.assertEqual(y[i], tan(x[i]))

    def testCscModel(self):
        mod = libmuqModelling.CscModel(2)

        x = [1.0, 2.0]
        y =  mod.Evaluate(x)

        for i in range(2):
            self.assertEqual(y[i], 1.0/sin(x[i]))

    def testSecModel(self):
        mod = libmuqModelling.SecModel(2)

        x = [1.0, 2.0]
        y =  mod.Evaluate(x)

        for i in range(2):
            self.assertEqual(y[i], 1.0/cos(x[i]))

    def testCotModel(self):
        mod = libmuqModelling.CotModel(2)

        x = [1.0, 2.0]
        y =  mod.Evaluate(x)

        for i in range(2):
            self.assertAlmostEqual(y[i], 1.0/tan(x[i]), 10)

    def testPow10Model(self):
        mod = libmuqModelling.Pow10Model(2)

        x = [1.0, 2.0]
        y =  mod.Evaluate(x)

        for i in range(2):
            self.assertEqual(y[i], pow(10.0, x[i]))

    def testPow2Model(self):
        mod = libmuqModelling.Pow2Model(2)

        x = [1.0, 2.0]
        y =  mod.Evaluate(x)

        for i in range(2):
            self.assertEqual(y[i], pow(2.0, x[i]))

    def testAbsModel(self):
        mod = libmuqModelling.AbsModel(2)

        x = [2.0, -2.0]
        y =  mod.Evaluate(x)

        for i in range(2):
            self.assertEqual(y[i], 2.0)

    def testLogModel(self):
        mod = libmuqModelling.LogModel(2)

        x = [1.0, 2.0]
        y =  mod.Evaluate(x)

        for i in range(2):
            self.assertEqual(y[i], log(x[i]))





        

