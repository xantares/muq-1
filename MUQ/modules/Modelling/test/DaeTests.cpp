// include the google testing header
#include "gtest/gtest.h"
#include <boost/property_tree/ptree.hpp>

// include the ODE ModPiece header
#include "MUQ/Modelling/DaeModPiece.h"

// include other MUQ models
#include "MUQ/Modelling/LinearModel.h"

using namespace std;
using namespace muq::Modelling;

class SimpleDaeResidual : public ModPiece{
public:
    SimpleDaeResidual() : ModPiece(Eigen::Vector3i{1,1,1},    // input sizes = [1; state dim; state dim, anything...]
  	                  1, // output dimensions is the dimension of the state
  	                  false,          // has direct gradient
  	                  true,          // has direct jacobian
  	                  false,          // has direct jacobian action
  	                  false,         // has direct hessian
  	                  false)
    {};
					  
  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs) override
  {
    return inputs.at(2) - Eigen::VectorXd::Ones(1);
  }

  Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& inputs, int const inputDimWrt)
  {
    if (inputDimWrt == 2) {    // derivative wrt derivative
      Eigen::MatrixXd jac(1,1);
	  jac << 1.0;
      return jac;

    } else {
      return Eigen::MatrixXd::Zero(outputSize, inputSizes(inputDimWrt));
    }
  }
};


class DaeResidual : public ModPiece {
public:

  // inputSizes must be 1, dim for time and state size
  DaeResidual() : ModPiece(Eigen::Vector3i{1,3,3},    // input sizes = [1; state dim; state dim, anything...]
	                  3, // output dimensions is the dimension of the state
	                  false,          // has direct gradient
	                  true,          // has direct jacobian
	                  false,          // has direct jacobian action
	                  false,         // has direct hessian
	                  false)
  {};

private:

  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs) override
  {
	  double time = inputs.at(0)(0);
	  Eigen::VectorXd state = inputs.at(1);
	  Eigen::VectorXd deriv = inputs.at(2);
	  
	  Eigen::VectorXd output(outputSize);
	  
	  output << deriv(0) + 0.04*state(0) - 1e4*state(1)*state(2),
	            deriv(1) - 0.04*state(0) + 1e4*state(1)*state(2) + 3e7*state(1)*state(1),
				state(0)+state(1)+state(2)-1;
				 
    return output;
  }

  Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& inputs, int const inputDimWrt)
  {
  	double time = inputs.at(0)(0);
  	Eigen::VectorXd state = inputs.at(1);
  	Eigen::VectorXd deriv = inputs.at(2);

    if (inputDimWrt == 1) { // derivative wrt state
      Eigen::MatrixXd jac(3, 3);
      jac <<  0.04, -1e4*state(2), -1e4*state(1),
	  -0.04, 1e4*state(2) + 6e7*state(1), 1e4*state(1),
	  1.0, 1.0, 1.0;
	   
      return jac;
    } else if (inputDimWrt == 2) {    // derivative wrt derivative
      Eigen::MatrixXd jac = Eigen::MatrixXd::Zero(3, 3);
	  jac(0,0) = 1.0;
	  jac(1,1) = 1.0;
      return jac;
	  
    } else {
      return Eigen::MatrixXd::Zero(outputSize, inputSizes(inputDimWrt));
    }
  }

};


class ModellingDae_SimpleTest : public::testing::Test {
protected:

  // set up the mesh that we will use, as well as an empty covariance matrix
  virtual void SetUp()
  {
    obsTimes.setLinSpaced(10,0.0,1.0); // the times we want to observe the output
    
    // define the ODE right hand side: du/dt = f(t,u)
    f = make_shared<SimpleDaeResidual>();

    // create a parameter list to hold the ODE solver parameters
    params.put("IDAS.ATOL", 1e-8);           // absolute tolerance
    params.get("IDAS.RTOL", 1e-4);           // relative tolerance
    //params.put("IDAS.MaxStepSize", 1e10);     // maximum step size, should be small for accurate discrete
 
    // set the initial condition
    y0.resize(1);
    y0 << 0; // the initial velocity (i.e. u0(1)) must be zero in this test
	
	dy0.resize(1);
	dy0 << 1;
  }

  Eigen::VectorXd y0, dy0, obsTimes;
  std::shared_ptr<ModPiece>   f;
  boost::property_tree::ptree params;
};



class ModellingDae_HardTest : public::testing::Test {
protected:

  // set up the mesh that we will use, as well as an empty covariance matrix
  virtual void SetUp()
  {
    obsTimes.resize(12); // the times we want to observe the output
    obsTimes(0) = 0.4;
	for(int i=1; i<obsTimes.size(); ++i)
		obsTimes(i) = 10*obsTimes(i-1);
	
    // define the ODE right hand side: du/dt = f(t,u)
    f = make_shared<DaeResidual>();

    // create a parameter list to hold the ODE solver parameters
    params.put("IDAS.ATOL", 1e-8);           // absolute tolerance
    params.get("IDAS.RTOL", 1e-4);           // relative tolerance
    //params.put("IDAS.MaxStepSize", 1e10);     // maximum step size, should be small for accurate discrete
 
    // set the initial condition
    y0.resize(3);
    y0 << 1, 0, 0; // the initial velocity (i.e. u0(1)) must be zero in this test
	
	dy0.resize(3);
	dy0 << -0.04, 0.04, 0.0;
  }

  Eigen::VectorXd y0, dy0, obsTimes;
  std::shared_ptr<ModPiece>   f;
  boost::property_tree::ptree params;
};



TEST_F(ModellingDae_SimpleTest, Dense)
{
  params.put("IDAS.LinearSolver", "Dense");
  auto u = make_shared<DaeModPiece>(f, obsTimes, params);

  // Integrate the ode
  Eigen::VectorXd output = u->Evaluate(y0,dy0);

  for(int i=0; i<obsTimes.size(); ++i)
	  EXPECT_NEAR(obsTimes(i),output(i),1e-8);
}

TEST_F(ModellingDae_HardTest, Dense)
{
  params.put("IDAS.LinearSolver", "Dense");
  auto u = make_shared<DaeModPiece>(f, obsTimes, params);

  // Integrate the ode
  Eigen::VectorXd output = u->Evaluate(y0,dy0);

  EXPECT_NEAR(1.0,output(12*3-1),1e-6);
  EXPECT_NEAR(0.0,output(12*3-2),1e-6);
  EXPECT_NEAR(0.0,output(12*3-3),1e-6);
}


