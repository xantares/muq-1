
// google testing library
#include "gtest/gtest.h"


#include "MUQ/Modelling/VectorPassthroughModel.h"
#include <MUQ/Modelling/ModGraphPiece.h>
#include <MUQ/Utilities/EigenTestUtils.h>
#include "MUQ/Modelling/ModGraphOperators.h"

using namespace std;
using namespace Eigen;
using namespace muq::Modelling;
using namespace muq::Utilities;


TEST(ModGraphOperations, ModGraphAdd)
{
  auto node1 = make_shared<VectorPassthroughModel>(2);
  auto node2 = make_shared<VectorPassthroughModel>(2);

  auto graph1 = make_shared<ModGraph>();

  graph1->AddNode(node1, node1->GetName());

  auto graph2 = make_shared<ModGraph>();
  graph2->AddNode(node2, node2->GetName());


  auto sum = graph1 + graph2;

  auto sum_piece = ModGraphPiece::Create(sum, sum->GetOutputNodeName());

  VectorXd x(2);
  x << 1, 2;
  VectorXd y(2);
  y << 3, 4;

  sum->writeGraphViz("results/tests/sumOperatorGraph.pdf");
  VectorXi sizes(2);
  sizes << 2, 2;
  EXPECT_PRED_FORMAT2(MatrixEqual, sizes, sum_piece->inputSizes);

  EXPECT_EQ(2, sum_piece->outputSize);
  EXPECT_PRED_FORMAT2(MatrixEqual, x + y, sum_piece->Evaluate(x, y));
}

TEST(ModGraphOperations, ModPieceAdd)
{
  auto node1 = make_shared<VectorPassthroughModel>(2);
  auto node2 = make_shared<VectorPassthroughModel>(2);

  auto sum = node1 + node2;

  auto sum_piece = ModGraphPiece::Create(sum);


  VectorXd x(2);

  x << 1, 2;
  VectorXd y(2);
  y << 3, 4;

  sum->writeGraphViz("results/tests/sumOperatorPiece.pdf");
  VectorXi sizes(2);
  sizes << 2, 2;
  EXPECT_PRED_FORMAT2(MatrixEqual, sizes, sum_piece->inputSizes);

  EXPECT_EQ(2, sum_piece->outputSize);
  EXPECT_PRED_FORMAT2(MatrixEqual, x + y, sum_piece->Evaluate(x, y));
}

TEST(ModGraphOperations, ModPieceAddSame)
{
  auto node1 = make_shared<VectorPassthroughModel>(2);

  auto sum = node1 + node1;

  auto sum_piece = ModGraphPiece::Create(sum);


  VectorXd x(2);

  x << 1, 2;

  sum->writeGraphViz("results/tests/sumOperatorPieceSame.pdf");
  VectorXi sizes(1);
  sizes << 2;
  EXPECT_PRED_FORMAT2(MatrixEqual, sizes, sum_piece->inputSizes);

  EXPECT_EQ(2, sum_piece->outputSize);
  EXPECT_PRED_FORMAT2(MatrixEqual, x + x, sum_piece->Evaluate(x));
}

TEST(ModGraphOperations, ModPieceAddMixed1)
{
  auto node1 = make_shared<VectorPassthroughModel>(2);
  auto node2 = make_shared<VectorPassthroughModel>(2);

  auto graph1 = make_shared<ModGraph>();

  graph1->AddNode(node1, node1->GetName());


  auto sum = graph1 + node2;

  auto sum_piece = ModGraphPiece::Create(sum);


  VectorXd x(2);
  x << 1, 2;
  VectorXd y(2);
  y << 3, 4;

  VectorXi sizes(2);
  sizes << 2, 2;
  EXPECT_PRED_FORMAT2(MatrixEqual, sizes, sum_piece->inputSizes);

  EXPECT_EQ(2, sum_piece->outputSize);
  EXPECT_PRED_FORMAT2(MatrixEqual, x + y, sum_piece->Evaluate(x, y));
}
TEST(ModGraphOperations, ModPieceAddMixed2)
{
  auto node1 = make_shared<VectorPassthroughModel>(2);
  auto node2 = make_shared<VectorPassthroughModel>(2);

  auto graph1 = make_shared<ModGraph>();

  graph1->AddNode(node1, node1->GetName());


  auto sum = node2 + graph1;

  auto sum_piece = ModGraphPiece::Create(sum);


  VectorXd x(2);
  x << 1, 2;
  VectorXd y(2);
  y << 3, 4;

  VectorXi sizes(2);
  sizes << 2, 2;
  EXPECT_PRED_FORMAT2(MatrixEqual, sizes, sum_piece->inputSizes);

  EXPECT_EQ(2, sum_piece->outputSize);
  EXPECT_PRED_FORMAT2(MatrixEqual, x + y, sum_piece->Evaluate(x, y));
}


TEST(ModGraphOperations, ModGraphDiff)
{
  auto node1 = make_shared<VectorPassthroughModel>(2);
  auto node2 = make_shared<VectorPassthroughModel>(2);

  auto graph1 = make_shared<ModGraph>();

  graph1->AddNode(node1, node1->GetName());

  auto graph2 = make_shared<ModGraph>();
  graph2->AddNode(node2, node2->GetName());


  auto sum = graph1 - graph2;

  auto sum_piece = ModGraphPiece::Create(sum, sum->GetOutputNodeName());

  VectorXd x(2);
  x << 1, 2;
  VectorXd y(2);
  y << 3, 4;

  VectorXi sizes(2);
  sizes << 2, 2;
  EXPECT_PRED_FORMAT2(MatrixEqual, sizes, sum_piece->inputSizes);

  EXPECT_EQ(2, sum_piece->outputSize);
  EXPECT_PRED_FORMAT2(MatrixEqual, x - y, sum_piece->Evaluate(x, y));
}

TEST(ModGraphOperations, ModPieceDiff)
{
  auto node1 = make_shared<VectorPassthroughModel>(2);
  auto node2 = make_shared<VectorPassthroughModel>(2);

  auto sum = node1 - node2;

  auto sum_piece = ModGraphPiece::Create(sum);


  VectorXd x(2);

  x << 1, 2;
  VectorXd y(2);
  y << 3, 4;

  VectorXi sizes(2);
  sizes << 2, 2;
  EXPECT_PRED_FORMAT2(MatrixEqual, sizes, sum_piece->inputSizes);

  EXPECT_EQ(2, sum_piece->outputSize);
  EXPECT_PRED_FORMAT2(MatrixEqual, x - y, sum_piece->Evaluate(x, y));
}


TEST(ModGraphOperations, ModPieceDiffMixed1)
{
  auto node1 = make_shared<VectorPassthroughModel>(2);
  auto node2 = make_shared<VectorPassthroughModel>(2);

  auto graph1 = make_shared<ModGraph>();

  graph1->AddNode(node1, node1->GetName());


  auto sum = graph1 - node2;

  auto sum_piece = ModGraphPiece::Create(sum);


  VectorXd x(2);
  x << 1, 2;
  VectorXd y(2);
  y << 3, 4;

  VectorXi sizes(2);
  sizes << 2, 2;
  EXPECT_PRED_FORMAT2(MatrixEqual, sizes, sum_piece->inputSizes);

  EXPECT_EQ(2, sum_piece->outputSize);
  EXPECT_PRED_FORMAT2(MatrixEqual, x - y, sum_piece->Evaluate(x, y));
}
TEST(ModGraphOperations, ModPieceDiffMixed2)
{
  auto node1 = make_shared<VectorPassthroughModel>(2);

  node1->SetName("y");
  auto node2 = make_shared<VectorPassthroughModel>(2);
  node2->SetName("x");
  auto graph1 = make_shared<ModGraph>();
  graph1->AddNode(node1, node1->GetName());


  auto sum = node2 - graph1;


  vector<string> inputOrder = { "x", "y" };
  auto sum_piece            = ModGraphPiece::Create(sum, "", inputOrder);


  VectorXd x(2);
  x << 1, 2;
  VectorXd y(2);
  y << 3, 4;

  VectorXi sizes(2);
  sizes << 2, 2;
  EXPECT_PRED_FORMAT2(MatrixEqual, sizes, sum_piece->inputSizes);

  EXPECT_EQ(2, sum_piece->outputSize);
  EXPECT_PRED_FORMAT2(MatrixEqual, x - y, sum_piece->Evaluate(x, y));
}


TEST(ModGraphOperations, ModGraphProduct)
{
  auto node1 = make_shared<VectorPassthroughModel>(2);
  auto node2 = make_shared<VectorPassthroughModel>(2);

  auto graph1 = make_shared<ModGraph>();

  graph1->AddNode(node1, node1->GetName());

  auto graph2 = make_shared<ModGraph>();
  graph2->AddNode(node2, node2->GetName());


  auto sum = graph1 * graph2;

  auto sum_piece = ModGraphPiece::Create(sum, sum->GetOutputNodeName());

  VectorXd x(2);
  x << 1, 2;
  VectorXd y(2);
  y << 3, 4;

  VectorXi sizes(2);
  sizes << 2, 2;
  EXPECT_PRED_FORMAT2(MatrixEqual, sizes, sum_piece->inputSizes);

  EXPECT_EQ(2, sum_piece->outputSize);
  EXPECT_PRED_FORMAT2(MatrixEqual, (x.array() * y.array()).matrix(), sum_piece->Evaluate(x, y));
}

TEST(ModGraphOperations, ModPieceProduct)
{
  auto node1 = make_shared<VectorPassthroughModel>(2);
  auto node2 = make_shared<VectorPassthroughModel>(2);

  auto sum = node1 * node2;

  auto sum_piece = ModGraphPiece::Create(sum);


  VectorXd x(2);

  x << 1, 2;
  VectorXd y(2);
  y << 3, 4;

  VectorXi sizes(2);
  sizes << 2, 2;
  EXPECT_PRED_FORMAT2(MatrixEqual, sizes, sum_piece->inputSizes);

  EXPECT_EQ(2, sum_piece->outputSize);
  EXPECT_PRED_FORMAT2(MatrixEqual, (x.array() * y.array()).matrix(), sum_piece->Evaluate(x, y));
}


TEST(ModGraphOperations, ModPieceProductMixed1)
{
  auto node1 = make_shared<VectorPassthroughModel>(2);
  auto node2 = make_shared<VectorPassthroughModel>(2);

  auto graph1 = make_shared<ModGraph>();

  graph1->AddNode(node1, node1->GetName());


  auto sum = graph1 * node2;

  auto sum_piece = ModGraphPiece::Create(sum);


  VectorXd x(2);
  x << 1, 2;
  VectorXd y(2);
  y << 3, 4;

  VectorXi sizes(2);
  sizes << 2, 2;
  EXPECT_PRED_FORMAT2(MatrixEqual, sizes, sum_piece->inputSizes);

  EXPECT_EQ(2, sum_piece->outputSize);
  EXPECT_PRED_FORMAT2(MatrixEqual, (x.array() * y.array()).matrix(), sum_piece->Evaluate(x, y));
}
TEST(ModGraphOperations, ModPieceProductMixed2)
{
  auto node1 = make_shared<VectorPassthroughModel>(2);
  auto node2 = make_shared<VectorPassthroughModel>(2);

  auto graph1 = make_shared<ModGraph>();

  graph1->AddNode(node1, node1->GetName());


  auto sum = node2 * graph1;

  auto sum_piece = ModGraphPiece::Create(sum);


  VectorXd x(2);
  x << 1, 2;
  VectorXd y(2);
  y << 3, 4;

  VectorXi sizes(2);
  sizes << 2, 2;
  EXPECT_PRED_FORMAT2(MatrixEqual, sizes, sum_piece->inputSizes);

  EXPECT_EQ(2, sum_piece->outputSize);
  EXPECT_PRED_FORMAT2(MatrixEqual, (x.array() * y.array()).matrix(), sum_piece->Evaluate(x, y));
}


TEST(ModGraphOperations, ScalarOps)
{
  auto node1 = make_shared<VectorPassthroughModel>(2);

  auto sum = (-(4 * (1 + node1 + 2) * 2) - 1.0) / .2;

  auto sum_piece = ModGraphPiece::Create(sum);


  VectorXd x(2);

  x << 1, 2;

  VectorXi sizes(1);
  sizes << 2;
  EXPECT_PRED_FORMAT2(MatrixEqual, sizes, sum_piece->inputSizes);

  EXPECT_EQ(2, sum_piece->outputSize);

  EXPECT_PRED_FORMAT2(MatrixEqual,
                      (-((8. * (1. + x.array() + 2.).matrix()).array()) - 1.0).matrix() / .2, sum_piece->Evaluate(x));
}

class BarMod : public ModPiece {
public:

  /** Constructor taking vector dimensions */
  BarMod(int dimIn) : ModPiece(Eigen::VectorXi::Ones(
                                 dimIn), dimIn, false, false, false, false, false, "RandomModPieceName") {}

  virtual ~BarMod() = default;

private:

  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& input) override
  {
    Eigen::VectorXd shifted(inputSizes.rows());

    for (int i = 0; i < inputSizes.rows(); ++i) {
      shifted(i) = input.at (i)(0);
    }
    return shifted;
  }
};

TEST(ModGraphOperations, ComposeModPiece)
{
  auto node1 = make_shared<VectorPassthroughModel>(1);
  auto node2 = make_shared<VectorPassthroughModel>(1);

  auto barNode = make_shared<BarMod>(3);
  auto graph   = GraphCompose(barNode, node1, node1, node2);


  auto graph_piece = ModGraphPiece::Create(graph);

  graph->writeGraphViz("results/tests/composeModPiece.pdf");
  VectorXd x(1); x << 1;
  VectorXd y(1); y << 2;

  VectorXi sizes(2);
  sizes << 1, 1;

  EXPECT_PRED_FORMAT2(MatrixEqual, sizes, graph_piece->inputSizes);

  EXPECT_EQ(3, graph->outputSize());

  VectorXd storedResult(3);
  storedResult << 1, 1, 2;
  EXPECT_PRED_FORMAT2(MatrixEqual, storedResult, graph_piece->Evaluate(x, y));
}

TEST(ModGraphOperations, ComposeModGraph)
{
  auto node1 = make_shared<VectorPassthroughModel>(1);

  node1->SetName("x");
  auto node2 = make_shared<VectorPassthroughModel>(1);
  node2->SetName("y");

  auto barNode  = make_shared<BarMod>(3);
  auto barGraph = GraphCompose(barNode, node1, node1, node2);

  auto node3 = make_shared<VectorPassthroughModel>(1);
  node3->SetName("a");
  auto node4 = make_shared<VectorPassthroughModel>(1);
  node4->SetName("b");


  auto graph = GraphCompose(barGraph, node3, node4, vector<string> { "x", "y" });
  graph->writeGraphViz("results/tests/composeModGraph.pdf");


  auto graph_piece = ModGraphPiece::Create(graph);

  VectorXd x(1); x << 1;
  VectorXd y(1); y << 2;

  VectorXi sizes(2);
  sizes << 1, 1;

  EXPECT_PRED_FORMAT2(MatrixEqual, sizes, graph_piece->inputSizes);

  EXPECT_EQ(3, graph->outputSize());

  VectorXd storedResult(3);
  storedResult << 1, 1, 2;
  EXPECT_PRED_FORMAT2(MatrixEqual, storedResult, graph_piece->Evaluate(x, y));
}