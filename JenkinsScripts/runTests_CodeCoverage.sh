#!/bin/bash
set +e
cd $WORKSPACE/MUQ
build/modules/RunAllTests --gtest_output=xml:coverage.junit.xml

$WORKSPACE/JenkinsScripts/gcovr -x --gcov-executable=gcov-4.8  -f ".*MUQ\/modules.*" -f ".*MUQ\/MUQ.*" -e ".*\/test\/.*" > coverage.xml

exit 0
